import errno
import glob
import os


class OutputDirectoryTreeCreator(object):
    def __init__(self, samples_details, output_dir, lane, force):
        """
        Create directory tree for the output files. The format is identical to CASAVA.
        Raises OSError if force is False and one of the folders already exist.
        If force is True, use existing directories if there are such.
        """
        self.samples_details = samples_details
        self.output_dir = output_dir
        self.lane = lane
        self.force = force

    @staticmethod
    def _create_dir_if_not_exist(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            # ignore error if someone already created the path, for example: running spid in parallel and writing
            # to the same output directory can cause this
            if e.errno != errno.EEXIST:
                raise

    def _remove_old_files_if_force(self, base_directory):
        old_files = glob.glob(os.path.join(base_directory, '*L00'+str(self.lane)+'*fastq*'))
        if self.force:
            [os.remove(filename) for filename in old_files]
        elif old_files:
            raise IOError("Files of lane %d already exist: %s" % (self.lane, old_files))

    def _create_sample_directory(self, one_sample_details):
        """
        Create sub-folders for each sample.
        'Undetermined_indices_InFirstOnly', 'Undetermined_indices_InSecondOnly' if there is more than one index
        """
        sample_name = one_sample_details.sample_name
        project_name = one_sample_details.project_name
        master_sample_name = one_sample_details.master_sample_name
        if sample_name == 'Undetermined':
            base_directory = os.path.join(self.output_dir, 'Undetermined_indices', 'Sample_lane' + str(self.lane))
        elif master_sample_name is None:
            base_directory = os.path.join(self.output_dir, 'Project_' + project_name, 'Sample_' + sample_name)
        else:
            base_directory = os.path.join(self.output_dir, 'Project_' + project_name,
                                          'Sample_' + master_sample_name, sample_name)
        self._remove_old_files_if_force(base_directory)
        self._create_dir_if_not_exist(base_directory)

    def create_directory_tree(self):
        """
        Create folders according to samples information.
        """
        self._create_dir_if_not_exist(self.output_dir)
        for one_sample_details in self.samples_details.values():
            self._create_sample_directory(one_sample_details)
