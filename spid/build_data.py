import csv
import logging
import re

from spid.sample_info import SampleInfo

MISSING_VALUE = '-'
SAMPLE_NAME_PATTERN = re.compile('^[A-Za-z_0-9\-]+$')

logger = logging.getLogger(__name__)


# some helper functions
def my_str(s):
    if not s:
        raise ValueError('Empty value')
    elif s == MISSING_VALUE:
        return None
    else:
        return s

my_int = lambda s: int(s) if s and s.isdigit() else s
my_int_minus_one = lambda s: int(s)-1 if s and s.isdigit() else s
yes_no_to_bin = lambda s: s == 'yes'


class SampleReader(object):
    def __init__(self, barcode_sheet, lanes, reads_num):
        """
        Reads the file with samples details (a.k.a. barcode sheet)
        """
        self.reads_num = reads_num
        self.barcode_sheet = barcode_sheet
        self.lanes = lanes
        self.samples_details = self._create_samples_details()

    @classmethod
    def _validate_sample_name(cls, sample_name):
        if sample_name and not SAMPLE_NAME_PATTERN.match(sample_name):
            raise ValueError('Sample name [%s] contains invalid characters' % sample_name)

    @classmethod
    def _get_sample_names(cls, master_sample_name, raw_sample_name):
        """
        Construct master sample name and sample name from the sample names in the input barcode sheet
        """
        cls._validate_sample_name(master_sample_name)
        cls._validate_sample_name(raw_sample_name)
        if raw_sample_name is None:
            return None, master_sample_name
        else:
            return master_sample_name, master_sample_name + '_' + raw_sample_name

    @classmethod
    def from_input_line(cls, input_line, reads_num):
        try:
            input_line = [my_str(s.strip()) for s in input_line]
        except ValueError:
            raise ValueError('empty value in line %s' % input_line)
        lane = my_int(input_line[0])
        project_name = input_line[1]
        master_sample_name, sample_name = cls._get_sample_names(input_line[2], input_line[3])
        tags = input_line[4:7]
        tags_names = input_line[7:10]
        master_tag = my_int_minus_one(input_line[10])
        cut_tags = [yes_no_to_bin(input_line[11]), yes_no_to_bin(input_line[12]), yes_no_to_bin(input_line[13])]
        max_mismatch = [my_int(input_line[14]), my_int(input_line[15]), my_int(input_line[16])]
        max_offset = [input_line[17], input_line[18], input_line[19]]
        reversed_tagged_reads = [my_int_minus_one(input_line[20]), my_int_minus_one(input_line[21])]
        reads_description = input_line[22:22+reads_num]
        return SampleInfo(lane, reads_num, project_name, master_sample_name, sample_name,
                          tags, tags_names, master_tag, cut_tags, max_mismatch,
                          max_offset, reversed_tagged_reads, reads_description)

    def _create_samples_details(self):
        """
        Reads the files of samples details and stores the values in a dictionary of three dimensions:
        first - keys are lane numbers, second - keys are sample names, third - keys are details of
        sample. 
        'Undetermined' and 'Undetermined_<sample>' are added as independent samples.
        """
        samples_fh = open(self.barcode_sheet, 'r')
        samples_lines = list(csv.reader(samples_fh, delimiter=','))
        samples_details = {}
        for lane in self.lanes:
            samples_details[lane] = {}
            logger.debug('Adding Undetermined to lane %s' % lane)
            samples_details[lane]['Undetermined'] = SampleInfo.undetermined_sample(lane, self.reads_num)
        for input_line in samples_lines:
            if not input_line or input_line[0][0] == '#':#empty line or comment line
                continue
            lane = int(input_line[0])
            if lane not in self.lanes:
                continue
            current_sample_details = self.from_input_line(input_line, self.reads_num)
            logger.debug('Adding sample %s to lane %s' % (current_sample_details.sample_name, lane))
            samples_details[lane][current_sample_details.sample_name] = current_sample_details
            if current_sample_details.sample_name != current_sample_details.master_sample_name and \
               current_sample_details.sample_name is not None and \
               current_sample_details.master_tag is not None:
                internal_undetermined = 'Undetermined_' + current_sample_details.master_sample_name
                if internal_undetermined not in samples_details[lane]:
                    samples_details[lane][internal_undetermined] = \
                        SampleInfo.undetermined_subsample(lane, self.reads_num, current_sample_details,
                                                          internal_undetermined)
        return samples_details
