from bs4 import BeautifulSoup
import glob
import logging
import os
import re


CASAVA_STATS_BASE_FILENAME = 'Demultiplex_Stats.htm'

logger = logging.getLogger(__name__)


def create_statistics(samples_details, total_num_sequences_per_sample, output_dir, lane):
    fhw_html = open(os.path.join(output_dir, 'Demultiplex_Stats_lane' + str(lane) + '.htm'), 'w')
    fhw_csv = open(os.path.join(output_dir, 'Demultiplex_Stats_lane' + str(lane) + '.csv'), 'w')
    # calculate total number of sequences in this lane
    total_num_sequences_in_lane = sum(total_num_sequences_per_sample.values())
    for sample_name, sample_details in samples_details.items():
        sequences_num = total_num_sequences_per_sample[sample_name]
        project_name = sample_details.project_name if sample_details.project_name else ''
        sequences_percent = 0.0
        if total_num_sequences_in_lane != 0:
            sequences_percent = float(sequences_num)/total_num_sequences_in_lane * 100
        barcodes = ''
        if not re.match('Undetermined.*', sample_details.sample_name):
            barcodes = '_'.join([name for name in sample_details.tags_names if name is not None])
        row_html = '<tr>\n' + '<td>' + str(lane) + '</td>\n' + '<td>' + sample_name + '</td>\n' + '<td>' + barcodes + \
                   '</td>\n' + '<td>' + re.sub("(\d)(?=(\d{3})+(?!\d))", r"\1,", "%d" % sequences_num) + '</td>\n' + \
                   '<td>' + '{0:.2f}'.format(sequences_percent) + '</td>\n' + '<td>' + project_name + '</td>\n' + \
                   '</tr>\n'
        row_csv = str(lane) + ',' + sample_name + ',' + barcodes + ',' + str(sequences_num) + \
                              ',' + '{0:.2f}'.format(sequences_percent) + ',' + project_name + '\n'
        fhw_html.write(row_html)
        fhw_csv.write(row_csv)
    fhw_html.close()
    fhw_csv.close()


def parse_casava_stat_file(file_handler, lane):
    soup = BeautifulSoup(file_handler)
    table = soup.findAll('table')[1]
    lane_rows = table.findAll('tr')
    for lane_row in lane_rows:
        lane_columns = lane_row.findAll('td')
        if int(lane_columns[0].text) == lane:
            return int(lane_columns[9].text.replace(',', ''))
    raise IOError('Reads number from CASAVA not found in file %s' % (file_handler.name),)


def get_num_sequences_in_lane_from_casava_output(demultiplexing_parameters, lane):
    try:
        casava_stat_file = os.path.join(demultiplexing_parameters.casava_output_dir, 'Basecall_Stats_*',
                                        CASAVA_STATS_BASE_FILENAME)
        casava_stat_file = glob.glob(casava_stat_file)[0]
        casava_stat_fh = open(casava_stat_file)
    except (IndexError, IOError) as e:  # IndexError: when glob found None, IOError: when the file cannot be opened
        logger.error('File %s was not found or could not be opened, check if Casava was run or ended : %s'
                     % (CASAVA_STATS_BASE_FILENAME, e,))
        logger.exception(e)
        raise e
    num_sequences_in_lane = parse_casava_stat_file(casava_stat_fh, lane)
    num_sequences_in_lane /= demultiplexing_parameters.reads_num
    return num_sequences_in_lane
