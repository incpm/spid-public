from abc import ABCMeta, abstractmethod
from collections import namedtuple
import logging
import re

WILDCARD = '.'
LETTERS = ['A', 'G', 'C', 'T', 'N']

TREE_ALGORITHM_MIDDLE_WILDCARDS = 'middle-wildcards'
TREE_ALGORITHM_START_WILDCARDS = 'start-wildcards'

logger = logging.getLogger(__name__)

# tag_number: 0 or 1 (first or second tag)
# tag_length: tag length - excluding the dots which represent offsets and dots before the location of tag in read
# tag_start: start location of tag in the read
Tag = namedtuple("Tag", ["sample_name", "tag_number", "tag_length", "tag_start",
                         "tree_num", "master_sample_name", "tag"])


class TagsTree(object):
    __metaclass__ = ABCMeta

    def __init__(self, letter=None):
        self.p_next = {}  # key is one character of the tag, value is a TagsTree object
        self.tag_details = {}  # the key is sample name, the value is a Tag object
        self.letter = letter

    @abstractmethod
    def merge(self, s, new_tag_details):
        pass

    @abstractmethod
    def lookup(self, sequence, last_appear):
        """
        Search in the tree for the tag matching the given sequence
        """
        pass

    def build(self, tags):
        for tag_details in tags:
            self.add(tag_details.tag, tag_details)
        for tag_details in tags:
            self.merge(tag_details.tag, tag_details)

    def add(self, s, new_tag_details):
        """
        Add given sequence 's' to the tree
        """
        assert s, 'Empty sequence'
        key_letter, tail = s[0], s[1:]
        if key_letter not in self.p_next:
            self.p_next[key_letter] = self.__class__(letter=key_letter)
        next_node = self.p_next[key_letter]
        if not tail:  # this is a leaf
            next_node.tag_details[new_tag_details.sample_name] = new_tag_details
            return
        next_node.add(tail, new_tag_details)

    def _node_repr(self):
        # return 'letter=[%s],next=%s,tag=%s' % (self.letter, self.p_next.keys(), self.tag_details)
        return '%s,%s,%s' % (self.letter, ''.join(sorted(self.p_next.keys())), self.tag_details)

    def __str__(self, level=0):
        ret = "  " * level + self._node_repr() + "\n"
        for (_, child) in sorted(self.p_next.items()):
            ret += child.__str__(level+1)
        return ret

    @property
    def next_wildcard(self):
        return self.p_next.get(WILDCARD, None)

    def is_split_point(self):
        return len(self.p_next) > 1

    def is_leaf(self):
        return bool(self.tag_details)


# Note: TagTreeStartWithWildcards is actually a DAG (directed acyclic graph) rather than a tree
class TagTreeStartWithWildcards(TagsTree):
    def __init__(self, *args, **kwargs):
        super(TagTreeStartWithWildcards, self).__init__(*args, **kwargs)
        self.is_ready = False

    def build(self, tags):
        super(TagTreeStartWithWildcards, self).build(tags)
        self._rename_new_edges_and_wildcard_keys()

    def merge(self, s, new_tag_details):
        self._merge_tags_with_wildcards(s, new_tag_details, self)

    def __str__(self, level=0, visited_edges=set()):
        # use DFS (depth first search) to print all edges
        ret = "  " * level + self._node_repr() + "\n"
        visited_edges.add(self)
        for (_, child) in sorted(self.p_next.items()):
            if child not in visited_edges:
                ret += child.__str__(level+1)
        return ret

    # tag_tree_with_wildcards is always the tree root in the first call to this method
    def _merge_tags_with_wildcards(self, s, new_tag_details, tag_tree_with_wildcards):
        """
        Merge tag with wildcards with other tags in the tree.
        Wildcard in new tag is merged with any letter in the tree.
        Letter in new tag is merged with the same letter in the tree.
        """
        tag_tree_with_wildcards.tag_details.update(self.tag_details)  # TODO: TAKE ONLY OLD, NO WITH "_"
        if not s:  # reached the end of the original branch of the tag with wildcards
            for key_letter in list(self.p_next.keys()):
                if key_letter[0] != '_' and \
                   key_letter not in tag_tree_with_wildcards.p_next and \
                   '_' + key_letter not in tag_tree_with_wildcards.p_next:
                    tag_tree_with_wildcards.p_next['_'+key_letter] = self.p_next[key_letter]
            return  # there is another tag that ended in this leaf
        letter, tail = s[0], s[1:]
        next_node_with_wildcards = tag_tree_with_wildcards.p_next[letter]
        for key_letter in sorted(list(self.p_next.keys()), reverse=True):
            if key_letter[0] != '_' and \
               letter != WILDCARD and \
               key_letter not in tag_tree_with_wildcards.p_next and \
               '_' + key_letter not in tag_tree_with_wildcards.p_next:
                tag_tree_with_wildcards.p_next['_'+key_letter] = self.p_next[key_letter]
            # merge
            if key_letter == letter or key_letter == WILDCARD:
                # noinspection PyProtectedMember
                self.p_next[key_letter]._merge_tags_with_wildcards(tail, new_tag_details, next_node_with_wildcards)

    def _rename_new_edges_and_wildcard_keys(self):
        if self.is_ready:
            return
        for key_letter in list(self.p_next.keys()):
            new_key_letter = key_letter
            if key_letter[0] == '_':  # replace new edges
                new_key_letter = key_letter[1]
                self.p_next[new_key_letter] = self.p_next[key_letter]
                del self.p_next[key_letter]
            # noinspection PyProtectedMember
            self.p_next[new_key_letter]._rename_new_edges_and_wildcard_keys()
            self.p_next[new_key_letter].is_ready = True
        if WILDCARD in self.p_next:  # replace wildcard keys
            for letter in LETTERS:
                if letter not in self.p_next:
                    self.p_next[letter] = self.p_next[WILDCARD]
            del self.p_next[WILDCARD]

    # assume that tree root is empty
    def lookup(self, sequence, last_appear, tag_details_memory=None):
        tag_details_memory = tag_details_memory or {}
        key_letter, tail = sequence[0], sequence[1:]
        next_node = self.p_next.get(key_letter, None)
        if next_node:
            if next_node.tag_details:  # match is found
                tag_details_memory = next_node.tag_details
                if not last_appear:
                    return tag_details_memory
            if tail:
                return next_node.lookup(tail, last_appear, tag_details_memory)
        # no match found until any leaf of the tree
        return tag_details_memory


class TagTreeWithWildcardsInMiddle(TagsTree):
    """
    An implementation of TagsTree for representing tags containing sequences of wildcards in an efficient way.
    For example, a tag 'AGCCG.....TAGCA' can be represented as:
    A -> G -> C -> C -> G -> (5 wildcards) -> T -> A -> G -> C -> A
    """
    def __init__(self, *args, **kwargs):
        super(TagTreeWithWildcardsInMiddle, self).__init__(*args, **kwargs)
        self.num_removed_wildcards = 0

    def _node_repr(self):
        return '%s,%s,%s' % (self.letter, self.num_removed_wildcards, self.tag_details)

    def build(self, tags):
        super(TagTreeWithWildcardsInMiddle, self).build(tags)
        self.zip_tree()

    def zip_tree(self):
        if not self.is_split_point() and not self.is_leaf():
            next_wildcard = self.next_wildcard
            if next_wildcard and next_wildcard.next_wildcard and \
                    not next_wildcard.is_split_point() and \
                    not next_wildcard.is_leaf():
                self.p_next[WILDCARD] = next_wildcard.next_wildcard
                self.num_removed_wildcards += 1
        # do this recursively for each one of the sub-trees
        for key_letter in self.p_next:
            self.p_next[key_letter].zip_tree()

    # s is a sequence with one dot at least
    def merge(self, s, new_tag_details):
        """
        Merging tag with wildcards with other tags in the tree.
        Dot in new tag is merged with any letter in the tree.
        Letter in new tag is merged with the same letter in the tree.
        """
        if len(s) == 0 and self.tag_details:  # the new tag and the branch end together
            if new_tag_details.sample_name in self.tag_details.keys():  # the new tag does not need to merge with itself
                return
            dict_tag_details = dict([(new_tag_details.sample_name, new_tag_details)])
            self.tag_details.update(dict_tag_details)
            return  # there is another tag that ended in this leaf
        elif len(s) == 0 and not self.tag_details:  # no branch ended with the new tag - no need to merge
            return
        letter, tail = s[0], s[1:]
        for key_letter in self.p_next.keys():
            if key_letter == letter or letter == WILDCARD:
                next_node = self.p_next[key_letter]
                next_node.merge(tail, new_tag_details)

    # assume that tree root is empty
    def lookup(self, sequence, last_appear, tag_details_memory=None, wildcard_stack=None):
        tag_details_memory = tag_details_memory or {}
        wildcard_stack = wildcard_stack or []
        key_letter, tail = sequence[0], sequence[1:]
        next_node = self._choose_and_save_sons(wildcard_stack, tag_details_memory, key_letter, tail)
        if next_node:  # the next son match to key_letter of the sequence
            if next_node.tag_details:  # match is found
                tag_details_memory = next_node.tag_details
                if not last_appear:
                    return tag_details_memory
            if tail:
                # i.e. the next_node is a wildcard which includes some zipped wildcards
                if self.num_removed_wildcards > 0 and self.p_next[WILDCARD] == next_node:
                    tail = tail[self.num_removed_wildcards:]
                return next_node.lookup(tail, last_appear, tag_details_memory, wildcard_stack)
            else:  # no match found until end of the sequence - search again from the split point
                return self._lookup_sequence_from_stack(wildcard_stack, tag_details_memory, last_appear)
        # no match found until any leaf of the tree - search again from the split point
        return self._lookup_sequence_from_stack(wildcard_stack, tag_details_memory, last_appear)

    def _choose_and_save_sons(self, wildcard_stack, tag_details_memory, key_letter, tail):
        if WILDCARD in self.p_next or key_letter in self.p_next:
            if key_letter in self.p_next:
                if WILDCARD in self.p_next:
                    if self.num_removed_wildcards > 0:
                        # next_node is wildcards that ends some zipped wildcards
                        tail = tail[self.num_removed_wildcards:]
                    wildcard_stack.append((self.p_next[WILDCARD], tag_details_memory, tail))
                    return self.p_next[key_letter]
                else:
                    return self.p_next[key_letter]
            else:
                return self.p_next[WILDCARD]
        return None

    @staticmethod
    def _lookup_sequence_from_stack(wildcard_stack, tag_details_memory, last_appear):
        while wildcard_stack:
            (saved_node, saved_tag_details_memory, saved_tail) = wildcard_stack.pop()
            # check if match found and the match is after split point
            # if the last match is equal to saved match in stack, the last match is found after the split point, 
            # else, the last match found before split and we need to search again from the split. 
            if tag_details_memory and tag_details_memory != saved_tag_details_memory: 
                continue
            else:  # match was found before split - continue to search from split point
                return saved_node.lookup(saved_tail, last_appear, saved_tag_details_memory, wildcard_stack)
        return tag_details_memory  # the stack is empty



class TagsTreeBuilder(object):
    def __init__(self, samples_details, reads_num, read_lengths):
        """
        Create all possible tags (with possible mismatches and offsets) and search trees.
        """
        self.samples_details = samples_details
        self.reads_num = reads_num
        self.read_lengths = read_lengths
        self.all_tags = self._prepare_tags_list()

    @staticmethod
    def _remove_wildcard_before_mutations_insertion(tag):
        wildcard_start = tag.find(WILDCARD)
        wildcard_end = None
        removed_wildcard_tag = tag
        # if there is wildcard, we cut the wildcards and create mismatches only in other letters
        if wildcard_start != -1:
            wildcard_end = tag.rfind(WILDCARD)
            removed_wildcard_tag = tag[:wildcard_start] + tag[wildcard_end+1:]
        return removed_wildcard_tag, wildcard_start, wildcard_end

    @staticmethod
    def _return_wildcard_to_tag(start, end, variation_tag):
        """
        Return wildcards to tag from which the wildcards were removed before the mismatches insertion
        """
        if start == -1:
            return variation_tag
        else:
            return variation_tag[:start] + (end - start + 1) * WILDCARD + variation_tag[start:]

    @staticmethod
    def _create_tag_offsets_and_inits(tag, init_tag_indices, max_offset, read_length):
        """
        Functionality:
        Add wildcards before the tag in cases: 1) the initial of tag in read is not start in first of the read 2) for
        each of the offsets toward the 3'. the function also cut the tag if the offset cause to tag exceed from
        the edges of the read.
        Input:
        Tag, the location of the tag in read, the maximum offset to 5' and 3'. 
        Output:
        list of tags with dots in 5' edge of the tag, according to each of the possible offsets. For each tag in list
        the function also return the offset. for offset toward 5' (left) the function return negative number.
        """
        m = re.match(r'([0-9]+)l([0-9]+)r', max_offset)
        max_offset_left = int(m.group(1))
        max_offset_right = int(m.group(2))
        tag_offsets = []
        for offset in range(0, max_offset_left+1):
            left_offset = init_tag_indices - offset
            if left_offset >= 0:
                tag_offsets.append((left_offset * WILDCARD + tag, left_offset))
            elif offset < init_tag_indices + len(tag):
                # offset is partially behind the left edge of the sequence
                tag_offsets.append((tag[offset-init_tag_indices:], left_offset))
        for offset in range(1, max_offset_right+1):
            right_offset = init_tag_indices + offset
            if right_offset > read_length-1:  # if the start of tag moved behind the 3' - it is illegal
                continue
            else:
                # if the end of tag exceeded the 3' edge, we remove the bases which exceeded
                tag_offsets.append((right_offset * WILDCARD + tag[:read_length-right_offset], right_offset))
        return tag_offsets

    def _dots_within_tags(self, tree_num):
        match_letters = '['+''.join(LETTERS)+']+\.'
        for tag in self.all_tags[tree_num]:
            if re.match(match_letters, tag.tag):
                return True
        return False

    def _node_for_tags_type(self, tree_num, tree_algorithm):
        if tree_algorithm == TREE_ALGORITHM_MIDDLE_WILDCARDS or self._dots_within_tags(tree_num):
            return TagTreeWithWildcardsInMiddle()
        elif tree_algorithm == TREE_ALGORITHM_START_WILDCARDS:
            return TagTreeStartWithWildcards()
        else:
            raise IOError('Unknown tree algorithm [%s]' % tree_algorithm)

    def _send_tags_to_trees(self, tags_trees, tree_algorithm):
        for i in range(self.reads_num):
            if self.all_tags[i]:
                tags_trees[i] = self._node_for_tags_type(i, tree_algorithm)
                tags_trees[i].build(self.all_tags[i])

    # noinspection PyTypeChecker
    def _prepare_tags_list(self):
        all_tags = [[] for _ in range(self.reads_num)]
        for sample_details in self.samples_details.values():
            if sample_details.is_undetermined():
                continue
            for i in range(sample_details.tags_num):
                (tag, init_tag_indices, max_offset, tagged_read_num) = \
                    (sample_details.tags[i], sample_details.init_tag_indices[i],
                     sample_details.max_offset[i], sample_details.tagged_reads[i])
                (tagged_read_length, tag_length) = (self.read_lengths[tagged_read_num], len(tag))
                (removed_wildcard_tag, wildcard_start, wildcard_end) = self._remove_wildcard_before_mutations_insertion(tag)
                variations_tag = TagVariationsGenerator().create_tag_variations(removed_wildcard_tag,
                                                                                sample_details.max_mismatch[i])
                for variation_tag in variations_tag:
                    variation_tag = self._return_wildcard_to_tag(wildcard_start, wildcard_end, variation_tag)
                    offsets_tag = self._create_tag_offsets_and_inits(variation_tag, init_tag_indices, max_offset, tagged_read_length)
                    for offset_tag in offsets_tag:
                        new_tag = Tag(sample_name=sample_details.sample_name,
                                      tag_number=i, tag_length=tag_length,
                                      tag_start=offset_tag[1], tree_num=tagged_read_num,
                                      master_sample_name=sample_details.master_sample_name,
                                      tag=offset_tag[0])
                        all_tags[tagged_read_num].append(new_tag)
                        if sample_details.tagged_reads[i] in sample_details.reversed_tagged_reads:
                            # in reversed read - its tag is inserted to two trees
                            other_tagged_read = sample_details.get_other_rev_tagged_read_number(sample_details.tagged_reads[i])
                            new_tag = Tag(sample_details.sample_name, i, tag_length, offset_tag[1],
                                          other_tagged_read, sample_details.master_sample_name, offset_tag[0])
                            all_tags[other_tagged_read].append(new_tag)
        return all_tags

    def build(self, tree_algorithm=TREE_ALGORITHM_START_WILDCARDS):
        """
        Build the tags trees.
        Number of trees is as the number of reads. If a read does not contain tag in any sample - the tree will be empty
        """
        tags_trees = [[] for _ in range(self.reads_num)]
        self._send_tags_to_trees(tags_trees, tree_algorithm)
        return tags_trees

    def build_tags_collision_tree(self):
        tags_trees = [[] for _ in range(self.reads_num)]
        tags_collisions = []
        for i in range(self.reads_num):
            if self.all_tags[i]:
                tags_trees[i] = TagsTreeCollisions()
                tags_trees[i].build(self.all_tags[i])
                tags_collisions.append(tags_trees[i].collisions)
        return tags_collisions


class TagVariationsGenerator(object):
    def _get_selections(self, tag, num_mismatches, selected, selections):
        """
        Choose all combinations of letters that need to undergo changes (for mismatches).
        """
        if num_mismatches == len(selected):
            selections.append(selected)
            return
        for i in range(len(tag)):
            selected_copy = selected[:]
            selected_copy.append(tag[i])
            self._get_selections(tag[i+1:], num_mismatches, selected_copy, selections)

    def _get_all_variations_in_selection(self, tag, selection, variations):
        """
        Compute all possible changes for one of combination of letters. This is done recursively.
        """
        if not selection:
            variations.append(tag)
            return
        for i in LETTERS:
            next_selection = int(selection[0])
            tag = tag[:next_selection] + i + tag[next_selection+1:]
            self._get_all_variations_in_selection(tag, selection[1:], variations)

    def create_tag_variations(self, tag, max_mismatches):
        """
        Create a list of all possible tags derived from the given tag with the given maximum number of mismatches
        """
        if max_mismatches == 0:
            return [tag]
        selections = []
        variations = []
        for mismatches_num in range(1, max_mismatches+1):
            self._get_selections(range(len(tag)), mismatches_num, [], selections)
            for selection in selections:
                self._get_all_variations_in_selection(tag, selection, variations)
        variations = list(set(variations))  # make the list unique
        return variations
