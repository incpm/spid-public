import glob
import logging
import os
import multiprocessing
import six
import timeit
import zlib

from collections import namedtuple

from spid.settings import READER_CHUNK_SIZE, READER_LINES_BUFFER, BYTES_TO_SEEK_HEADER

logger = logging.getLogger(__name__)

FASTQ_FILENAME_TEMPLATE_CASAVA_V1 = 'lane%(lane)d_NoIndex_L%(lane)03d_R%(read_num)d_%(file_number)03d.fastq'
FASTQ_FILENAME_TEMPLATE_CASAVA_V2 = 'Undetermined_S0_L%(lane)03d_R%(read_num)d_%(file_number)03d.fastq'
FASTQ_INDEX_FILENAME_TEMPLATE_CASAVA_V2 = 'Undetermined_S0_L%(lane)03d_I%(read_num)d_%(file_number)03d.fastq'
NUM_LINES_PER_FRAGMENT = 4


class InputFilesReader(object):
    def __init__(self, lane, casava_output_dir, casava_folders_structure):
        self.lane = lane
        self.casava_output_dir = casava_output_dir
        self.input_fastq_files_path = self._init_input_fastq_files_path(casava_folders_structure)
        self.gzipped_input_files = self._is_gzipped_input_files()
        self.file_name_template = FASTQ_FILENAME_TEMPLATE_CASAVA_V1 if self._is_casava_v1_file_name_template() else FASTQ_FILENAME_TEMPLATE_CASAVA_V2
        self.file_name_template = self.file_name_template+".gz" if self.gzipped_input_files else self.file_name_template
        self.num_reads_files = self._find_reads_num(self.file_name_template)
        self.num_idnexes_files = self._find_reads_num(FASTQ_INDEX_FILENAME_TEMPLATE_CASAVA_V2)
        self.read_lengths = self._get_read_lengths()

    def __iter__(self):
        return self

    def _is_casava_v1_dir_structure(self):
        return glob.glob(os.path.join(self.casava_output_dir, 'Project_*', 'Sample_*'))

    def _is_casava_v1_file_name_template(self):
        if glob.glob(os.path.join(self.input_fastq_files_path, 'lane*')):
            return True
        elif glob.glob(os.path.join(self.input_fastq_files_path, 'Undetermined_S0*')):
            return False
        else:
            raise IOError("Unknown input files names format in input folder %s.\n"
                          "Replace the input files names to regular format of CASAVA: %s.\n"
                          "For example: Undetermined_S0_L002_R1_001.fastq" %(self.input_fastq_files_path,FASTQ_FILENAME_TEMPLATE_CASAVA_V2))

    def _is_gzipped_input_files(self):
        if glob.glob(os.path.join(self.input_fastq_files_path, '*fastq.gz')) and glob.glob(os.path.join(self.input_fastq_files_path, '*fastq')):
            raise IOError("In input folder %s: part of the files are gzipped and other are not gzipped. Use with one type of files" %(self.input_fastq_files_path))
        if glob.glob(os.path.join(self.input_fastq_files_path, '*fastq.gz')):
            return True
        elif glob.glob(os.path.join(self.input_fastq_files_path, '*fastq')):
            return False
        else:
            raise IOError("The extention of input files must be fastq or fastq.gz in input folder %s." %(self.input_fastq_files_path))

    def _init_input_fastq_files_path(self, casava_dir_structure):
        if casava_dir_structure and self._is_casava_v1_dir_structure():
            logger.info("Input directories of the fastq files is structured in original format of CASAVA v1")
            project_dirs = glob.glob(os.path.join(self.casava_output_dir, 'Project_*'))
            if not project_dirs:
                raise IOError("Project_* directories do not exist in %s, please check if CASAVA ran successfully"
                              % self.casava_output_dir)
            if len(project_dirs) > 1:
                raise IOError("Multiple project directories found in %s, SPID can handle only a single project directory "
                              "in each lane. Project directories: %s"
                              % (self.casava_output_dir, project_dirs))
            return os.path.join(project_dirs[0], 'Sample_lane' + str(self.lane))
        else:
            return self.casava_output_dir

    def _find_reads_num(self, template_file_name):
        reads_num = 1
        while True:
            new_filename = os.path.join(self.input_fastq_files_path, template_file_name %
                                        {'lane': self.lane, 'read_num': reads_num, 'file_number': 1})
            if not os.path.isfile(new_filename):
                return reads_num-1
            reads_num +=1

    def _get_read_lengths(self):
        read_lengths = []
        for file in self.get_fastq_filenames()[0]:
            with open(file) as f:
                f.readline()#skip on the header line
                read_lengths.append(len(f.readline()))
        return read_lengths

    #return only the read lines (without quality etc.)
    def _get_reads_sample(self, reads_num):
        fragments = []
        fhs = [open(file) for file in self.get_fastq_filenames()[0]]
        for i in range(reads_num):
            fragment = []
            for fh in fhs:
                fh.readline()
                fragment.append(fh.readline().strip())
                fh.readline()
                fh.readline()
            fragments.append(fragment)
        [fh.close() for fh in fhs]
        return fragments

    def _get_file_name(self, template_file_name, read_num, file_number):
        new_filename = os.path.join(self.input_fastq_files_path, template_file_name %
                                    {'lane': self.lane, 'read_num': read_num, 'file_number': file_number})
        if not os.path.isfile(new_filename):
            raise IOError('File %s is missing' % new_filename)
        return new_filename

    def get_fastq_filenames(self):
        """
        Assuming all input files already exist (usually zipped FASTQ files generated by CASAVA's bcl2fastq script),
        return a list containing all these file names. For example:
        [[lane1_NoIndex_L001_R1_001.fastq, lane1_NoIndex_L001_R2_001.fastq],
         [lane1_NoIndex_L001_R1_002.fastq, lane1_NoIndex_L001_R2_002.fastq],
         ...
         [lane1_NoIndex_L001_R1_022.fastq, lane1_NoIndex_L001_R2_022.fastq]]

        In version 2 of bcl2fastq script:
        Can be one file for each read: Undetermined_S0_L001_R1_001.fastq,
        and can be appear files of indexes: Undetermined_S0_L001_I1_001.fastq. For example:
        [[Undetermined_S0_L001_R1_001.fastq, Undetermined_S0_L001_I1_001.fastq, \
        Undetermined_S0_L001_I2_001.fastq, Undetermined_S0_L001_R2_001.fastq]]
        """
        input_fastq_filenames = []
        file_number = 1
        while True:
            first_read_filename = os.path.join(self.input_fastq_files_path, self.file_name_template %
                                               {'lane': self.lane, 'read_num': 1, 'file_number': file_number})
            logger.info('Looking for file %s' % first_read_filename)
            if not os.path.exists(first_read_filename):
                break
            new_filenames = [first_read_filename]
            for index_num in range(1, self.num_idnexes_files+1):#Search for index files (I1,I2 etc.)
                new_filenames.append(self._get_file_name(FASTQ_INDEX_FILENAME_TEMPLATE_CASAVA_V2, index_num, file_number))
            for read_num in range(2, self.num_reads_files+1):#Search for read files (R1,R2 etc.)
                new_filenames.append(self._get_file_name(self.file_name_template, read_num, file_number))
            input_fastq_filenames.append(new_filenames)
            file_number += 1
        logger.info('Lane %d was divided by CASAVA into %d segments' % (self.lane, len(input_fastq_filenames)))
        if not input_fastq_filenames:
            raise IOError('There is no input files. Verify that the names of the input files are in CASAVA format.')
        return input_fastq_filenames


def is_gz_format(file_name):
    extension = os.path.splitext(file_name)[1]
    return True if extension == '.gz' else False



class SingleFastqFileReader(object):
    def __init__(self, fastq_filename_with_range, file_fastq_lines, lines_buffer=READER_LINES_BUFFER, reader_chunk_size=READER_CHUNK_SIZE):
        self.file_fastq_lines = file_fastq_lines
        self.fastq_filename = fastq_filename_with_range.file_name
        self.lines_buffer = lines_buffer
        self.fh = open(self.fastq_filename, 'r')
        self.reader_chunk_size_in_megabytes = reader_chunk_size*1024*1024
        self.start_file_read = fastq_filename_with_range.start
        self.end_file_read = fastq_filename_with_range.end
        self.gz_format = is_gz_format(self.fastq_filename)

    def end_of_partition(self):
        return self.fh.tell() == self.end_file_read+1

    def read_chunk(self):
        if self.fh.closed:
            yield
        start_time = timeit.default_timer()
        fastq_file_basename = os.path.basename(self.fh.name)  # so that the log message will be a bit shorter :)
        logger.info('Opening input file %s from byte %i until byte %i for reading'
                    %(fastq_file_basename, self.start_file_read, self.end_file_read))
        elapsed_time_in_milliseconds = (timeit.default_timer() - start_time) * 1000
        decompressor = zlib.decompressobj(16+zlib.MAX_WBITS)
        half_line  = ""
        line_num = 0
        self.fh.seek(self.start_file_read)
        while not self.end_of_partition():
            buffer = self.fh.read(min(self.reader_chunk_size_in_megabytes, self.end_file_read-self.fh.tell())+1)
            if self.gz_format:
                buffer = decompressor.decompress(buffer)
            for line in buffer.splitlines(True):
                if len(line) == 0:#there is more one end of line in consequently
                    continue
                if line[-1] == '\n' or self.end_of_partition():
                    line_num += 1
                    line = half_line + line
                    self.file_fastq_lines.append(line)
                    half_line = ""
                else:
                    half_line += line
                if line_num == self.lines_buffer:
                    line_num = 0
                    yield
            if self.end_of_partition():
                yield
        if len(self.file_fastq_lines)%NUM_LINES_PER_FRAGMENT:
            raise IOError('File %s - number of lines is not divisible by %s, file might be corrupt or trimmed'
                          % (os.path.basename(self.fh.name), NUM_LINES_PER_FRAGMENT))
        logger.info('Read FASTQ records of file %s from byte %i until byte %i successfully (%.2f ms.)' %
                    (fastq_file_basename, self.start_file_read, self.end_file_read, elapsed_time_in_milliseconds))
        self.fh.close()
        yield


class SingleFastqFileSetReader(object):
    """
    Reads sequences from a single set of FASTQ files.

    An example for such a set in case of paired-end run for example, can be:
    lane3_NoIndex_L002_R1_001.fastq, lane2_NoIndex_L002_R2_001.fastq, lane2_NoIndex_L002_R3_001.fastq
    In this case, a sequence (or fragment) is constructed of 3 reads and therefore should be read from all 3 files
    """
    def __init__(self, input_fastq_set_with_ranges, reader_chunk_size=READER_CHUNK_SIZE, lines_buffer=READER_LINES_BUFFER):
        assert input_fastq_set_with_ranges, 'No filenames given'
        self.input_fastq_set_with_ranges = input_fastq_set_with_ranges
        self.sequence_index = -1
        self.num_lines = None
        self.fastq_lines = [[] for _ in self.input_fastq_set_with_ranges]
        self.single_file_readers = [SingleFastqFileReader(input_file_with_ranges, file_fastq_lines, lines_buffer, reader_chunk_size).read_chunk() for (input_file_with_ranges, file_fastq_lines) in
                                   zip(self.input_fastq_set_with_ranges, self.fastq_lines)]


    def _read_input_files(self):
        for item in self.fastq_lines:
            del item[:]
        processes = []
        for reader in self.single_file_readers:
            p = multiprocessing.Process(target=six.next(reader))#Support in next function of python2 and python3
            p.start()
            processes.append(p)
        [p.join() for p in processes]

    def _load_input_files(self):
        self._read_input_files()
        self.num_lines = len(self.fastq_lines[0])
        for file_ranges, fastq_lines in zip(self.input_fastq_set_with_ranges[1:], self.fastq_lines[1:]):
            if self.num_lines != len(fastq_lines):
                del fastq_lines[self.num_lines:]
                if file_ranges.end == os.path.getsize(file_ranges.file_name):
                    raise IOError('Files %s and %s do not contain the same number of sequences'
                                  % (self.input_fastq_set_with_ranges[0].file_name, file_ranges.file_name))

    def close(self):
        del self.fastq_lines

    def next(self):
        """
        Return a single fragment. Raises StopIteration if done
        """
        self.sequence_index += 1
        line_index = NUM_LINES_PER_FRAGMENT * self.sequence_index
        if self.sequence_index == 0 or line_index > self.num_lines-1:  # first time
            self.sequence_index = 0
            line_index = 0
            self._load_input_files()
        if self.fastq_lines == [[] for _ in self.input_fastq_set_with_ranges]:
            if self.num_lines is None:
                logger.warn("No sequences found in %s" % self.input_fastq_set_with_ranges)
            raise StopIteration
        fastq_fragment = [[line.rstrip() for line in fastq_lines_per_read[line_index:line_index+NUM_LINES_PER_FRAGMENT]]
                          for fastq_lines_per_read in self.fastq_lines]
        return fastq_fragment


class FileReadingRanges(object):
    def __init__(self, file_name, start, end):
        self.file_name = file_name
        self.start = start
        self.end = end


class FilesReadingRanges(object):
    def __init__(self, fastq_filenames, gzipped_input_files, bytes_to_seek_header=BYTES_TO_SEEK_HEADER):
        self.fastq_filenames = fastq_filenames
        self.bytes_to_seek_header = bytes_to_seek_header
        self.files_and_ranges = []
        self.gzipped_input_files = gzipped_input_files
    
    def readLineLocation(self, fh):
        return fh.tell(), fh.readline()
        
    def _correct_start_first_file_and_header_line(self, first_file_raw_range):
        fh_first_file = open(first_file_raw_range.file_name, 'r')
        if first_file_raw_range.start == 0:
            return fh_first_file.readline().split(' ')[0]#header line
        fh_first_file.seek(first_file_raw_range.start)
        while True:
            start, line = self.readLineLocation(fh_first_file)
            if line == '+\n':
                fh_first_file.readline()
                location, line = self.readLineLocation(fh_first_file)
                first_file_raw_range.start = location
                return line.split(' ')[0] #header line

    def _correct_end_first_file(self, first_file_raw_range):
        fh_first_file = open(first_file_raw_range.file_name, 'r')
        if first_file_raw_range.end == os.path.getsize(first_file_raw_range.file_name)-1:
            return
        fh_first_file.seek(first_file_raw_range.end)
        while True:
            line = fh_first_file.readline()
            if line == '+\n':
                fh_first_file.readline()
                first_file_raw_range.end = fh_first_file.tell()-1
                return

    def _correcte_start_other_files(self, other_files_raw_range, header_line_of_first_file):
        for file_raw_range in other_files_raw_range:
            fh = open(file_raw_range.file_name, 'r')
            fh.seek(max(file_raw_range.start-self.bytes_to_seek_header, 0))
            while True:
                location, line = self.readLineLocation(fh)
                if line.split(' ')[0] == header_line_of_first_file:
                    file_raw_range.start = location
                    break

    def _set_all_file_ranges(self):
        for files_set in self.fastq_filenames:
            files_and_ranges_set = []
            for file in files_set:
                files_and_ranges_set.append(FileReadingRanges(file, 0, os.path.getsize(file)-1))
            self.files_and_ranges.append(files_and_ranges_set)

    def _set_raw_ranges_within_files_set(self, read_processes_num):
            file_names_set = self.fastq_filenames[0]#suppose that there is only one set (bcl2fastq version 2)
            for i in range(read_processes_num):
                files_and_range_set = []
                file_count=0
                for file in file_names_set:
                    file_size = os.path.getsize(file)-1
                    part_size = int(file_size)/read_processes_num
                    end = file_size if file_count > 0 or i == read_processes_num-1 else (i+1)*part_size-1
                    files_and_range_set.append(FileReadingRanges(file, i*part_size, end))
                    file_count+=1
                self.files_and_ranges.append(files_and_range_set)
            return self.files_and_ranges

    def get_reading_range_within_file(self, read_processes_num):
        if len(self.fastq_filenames) > 1 or (len(self.fastq_filenames)==1 and self.gzipped_input_files):
            self._set_all_file_ranges()
        else:#One not compressed file.
            self._set_raw_ranges_within_files_set(read_processes_num)
            for raw_ranges_set in self.files_and_ranges:
                first_header_line = self._correct_start_first_file_and_header_line(raw_ranges_set[0])
                self._correct_end_first_file(raw_ranges_set[0])
                self._correcte_start_other_files(raw_ranges_set[1:], first_header_line)
        return self.files_and_ranges
