import gzip
import logging
import os
import threading
import timeit

from spid.settings import OUTPUT_FILE_FORMAT_UNCOMPRESSED, OUTPUT_FILE_FORMAT_GZIPPED

logger = logging.getLogger(__name__)


class OutputFilesManager(object):
    def __init__(self, output_dir, lane, samples_details, output_format, fastq_cluster_count):
        self.output_dir = output_dir
        self.lane = lane
        self.samples_details = samples_details
        self.base_output_paths = self._construct_output_paths()
        self.output_format = output_format
        self.fastq_cluster_count = fastq_cluster_count

    def _construct_output_paths(self):
        return dict([(sample_details.sample_name, self._construct_output_path(sample_details))
                     for sample_details in self.samples_details.values()])

    def _construct_output_path(self, sample_details):
        if sample_details.sample_name == 'Undetermined':
            return os.path.join(self.output_dir, 'Undetermined_indices', 'Sample_lane' + str(sample_details.lane))
        elif sample_details.master_sample_name is None:
            return os.path.join(self.output_dir, 'Project_' + sample_details.project_name,
                                'Sample_' + sample_details.sample_name)
        else:
            return os.path.join(self.output_dir, 'Project_' + sample_details.project_name,
                                'Sample_' + sample_details.master_sample_name, sample_details.sample_name)

    def get_output_filenames(self, sample_name, total_num_sequences):
        file_number = (total_num_sequences / self.fastq_cluster_count) + 1
        output_filenames = self._construct_fastq_output_filenames(sample_name, file_number=file_number)
        num_sequences_in_output_files = total_num_sequences % self.fastq_cluster_count
        return output_filenames, num_sequences_in_output_files

    def _construct_fastq_output_filenames(self, sample_name, file_number):
        """
        Construct new output file names for each of the non-empty reads (where the index does not cover all the bases)
        in a specific sample
        
        The returned filenames format is identical to CASAVA. For example:
        'lane1_Undetermined_L001_R1_001.fastq.gz' or
        'TAU_Dana_TTAGGC_L001_R1_026.fastq.gz'
        """
        sample_details = self.samples_details[sample_name]
        if sample_details.is_undetermined_sample():
            filename_prefix = 'lane%(lane)s_Undetermined' % {'lane': self.lane}
            reads = range(sample_details.reads_num)
        elif sample_details.is_undetermined_subsample():
            concatenated_tags_names = '_'.join(filter(lambda x: x is not None, sample_details.tags_names))
            filename_prefix = '%s_%s' % (sample_name, concatenated_tags_names)
            reads = range(sample_details.reads_num)
        else:
            concatenated_tags_names = '_'.join(filter(lambda x: x is not None, sample_details.tags_names))
            filename_prefix = '%s_%s' % (sample_name, concatenated_tags_names)
            reads = sample_details.not_empty_reads_num
        logger.debug('filename_prefix=%s reads=%s' % (filename_prefix, reads))
        full_path_fq_file = self.base_output_paths[sample_name]
        file_extension = 'fastq.gz'
        if self.output_format == OUTPUT_FILE_FORMAT_UNCOMPRESSED:
            file_extension = 'fastq'
        output_filenames = []
        for read_num in reads:
            new_filename = '%(prefix)s_L%(lane)03d_R%(read_num)d_%(file_number)03d.%(file_exten)s' \
                           % dict(prefix=filename_prefix, lane=self.lane, read_num=read_num + 1,
                                  file_number=file_number, file_exten=file_extension)
            output_filenames.append(os.path.join(full_path_fq_file, new_filename))
        logger.debug('Created a new set of output filenames for %s : %s' % (sample_name, output_filenames))
        return output_filenames


class SingleFastqFileWriter(threading.Thread):
    def __init__(self, output_filename, output_format, file_fastq_lines):
        super(SingleFastqFileWriter, self).__init__()
        self.output_filename = output_filename
        self.output_format = output_format
        self.file_fastq_lines = file_fastq_lines

    def run(self):
        start_time = timeit.default_timer()
        fastq_file_basename = os.path.basename(self.output_filename)  # so that the log message will be a bit shorter...
        logger.debug('Started writing output to file %s' % fastq_file_basename)
        fh = None
        try:
            if self.output_format == OUTPUT_FILE_FORMAT_GZIPPED:
                fh = gzip.open(self.output_filename, 'ab', compresslevel=1)
            else:
                fh = open(self.output_filename, 'ab')
            linesToPrint='\n'.join(self.file_fastq_lines)+'\n'
            fh.write(linesToPrint.encode('utf-8'))
            elapsed_time_in_milliseconds = (timeit.default_timer() - start_time) * 1000
            logger.debug('Wrote %s lines to file %s successfully (%.2f ms.)'
                         % (len(self.file_fastq_lines), fastq_file_basename, elapsed_time_in_milliseconds))
        except Exception as e:
            logger.exception(e)
            raise
        finally:
            if fh:
                fh.close()


class BulkWriter(object):
    def __init__(self, output_filenames, sample_name, not_empty_reads_num, output_format):
        assert output_filenames, 'No output filenames for sample %s' % (sample_name,)
        logger.debug('Initializing new BulkWriter to files %s' % (output_filenames,))
        self.output_filenames = output_filenames
        self.sample_name = sample_name
        self.not_empty_reads_num = not_empty_reads_num
        self.output_format = output_format

    def flush(self, sequences):
        assert sequences, 'No sequences'
        logger.debug('Going to flush %s sequences' % (len(sequences),))
        fastq_lines = []
        # re-order sequences
        for (read_index, read_number) in enumerate(self.not_empty_reads_num):
            fastq_lines.append([])
            for sequence in sequences:
                fastq_lines[read_index].extend(sequence[read_number])
        flush_threads = [SingleFastqFileWriter(self.output_filenames[read_index],
                                               self.output_format, fastq_lines[read_index])
                         for read_index in range(len(self.not_empty_reads_num))]
        [t.start() for t in flush_threads]
        [t.join() for t in flush_threads]
        return len(sequences)
