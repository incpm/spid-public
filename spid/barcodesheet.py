import ahocorasick
from collections import namedtuple
import csv
import logging
import random
import re
from math import floor
import string
import sys
# from scipy import stats

logger = logging.getLogger(__name__)

from spid.settings import MIN_REMAINS_PER_MIS, MIN_UMI_ENRICHMENT, MAX_P_VALUE_ENRICHMENT, MAX_DIFF_BARCODES_FREQ_WITHIN_SAMPLE, MAX_DIFF_BARCODES_FREQ_BETWEEN_SAMPLES

PotentialIntersection = namedtuple("PotentialIntersection", ["read", "min_pos", "max_pos", "o_min_pos", "o_max_pos"])

class BarcodeLocationError(Exception):
    pass

class BarcodeLocations(object):
    def __init__(self, barcode, read_1=None, read_2=None, min_pos_1=float('inf'), max_pos_1=-float('inf'),
                 min_pos_2=float('inf'), max_pos_2=-float('inf'), umi_code1='', umi_code2='', loc1_freq=0, loc2_freq=0,
                 sample_name=None, lane=None):
        self.barcode = barcode
        self.read_1 = read_1
        self.read_2 = read_2
        self.min_pos_1 = min_pos_1
        self.max_pos_1 = max_pos_1
        self.min_pos_2 = min_pos_2
        self.max_pos_2 = max_pos_2
        self.umi_code1 = umi_code1
        self.umi_code2 = umi_code2
        self.loc1_freq = loc1_freq
        self.loc2_freq = loc2_freq
        self.mismatch = len(barcode)
        self.sample_name = sample_name
        self.lane = lane


    def is_shift_1(self):
        return self.min_pos_1 != self.max_pos_1

    def is_shift_2(self):
        return self.min_pos_2 != self.max_pos_2

    def is_replaced(self):
        return self.read_2 is not None

    def reads_equal(self, o_barcode_location):
        return self.read_1 == o_barcode_location.read_1 and self.read_2 == o_barcode_location.read_2

    def _is_intersect(self, min1, max1, min2, max2):
        return (min1 >= min2 and min1 <= max2) or (max1 >= min2 and max1 <= max2)

    def _set_min_max(self, min_num, max_num, position):
        return min(min_num, position), max(max_num, position)

    def _swap(self, first, second):
        return second, first

    def _set_small_read_in_first(self):
        if self.read_2 is not None and self.read_1 > self.read_2:
            self.read_1, self.read_2 = self._swap(self.read_1, self.read_2)
            self.min_pos_1, self.min_pos_2 = self._swap(self.min_pos_1, self.min_pos_2)
            self.max_pos_1, self.max_pos_2 = self._swap(self.max_pos_1, self.max_pos_2)
            self.umi_code1, self.umi_code2 = self._swap(self.umi_code1, self.umi_code2)
            self.loc1_freq, self.loc2_freq = self._swap(self.loc1_freq, self.loc2_freq)

    def pos_equal(self, o_barcode_location):
        return self._is_intersect(self.min_pos_1, self.max_pos_1, o_barcode_location.min_pos_1, o_barcode_location.max_pos_1) \
            and self._is_intersect(self.min_pos_2, self.max_pos_2, o_barcode_location.min_pos_2, o_barcode_location.max_pos_2)

    def set_values(self, read_number, position, umi, freq):
        if read_number == self.read_1 or self.read_1 is None:
            self.read_1 = read_number
            self.min_pos_1, self.max_pos_1 = self._set_min_max(self.min_pos_1, self.max_pos_1, position)
            self.umi_code1 = umi
            self.loc1_freq += freq
        elif read_number == self.read_2 or self.read_2 == None:
            self.read_2 = read_number
            self.min_pos_2, self.max_pos_2 = self._set_min_max(self.min_pos_2, self.max_pos_2, position)
            self.umi_code2 = umi
            self.loc2_freq += freq
        else:
            raise BarcodeLocationError('The Barcode %s (sample %s, lane %s) is enriched on more than two reads, we save only the 2 first locations. '
                                       'The original enriched locations are (read number, position): %s, %s, %s'
                                       %(self.barcode, self.sample_name, self.lane, self.read_1, self.read_2, read_number))
        self._set_small_read_in_first()

    def __str__(self):
        return repr("%s %s %s %s %s %s %s" %(self.barcode, self.read_1, self.read_2, self.min_pos_1, self.max_pos_1, self.min_pos_2, self.max_pos_2))



class EnrichedBarcodeLocation(object):
    #Find the locations of the barcodes on the reads
    def __init__(self, barcode, fragments, max_p_value_enrichment, sample_name=None, lane=None, o_sample_barcodes_trie=(), samples_num=1):
        self.barcode = barcode
        self.fragments = fragments
        self.locations_histogram = {}
        self.max_p_value_enrichment = max_p_value_enrichment
        self.bases_after_barcode = {}
        self.sample_name = sample_name
        self.lane = lane
        self.o_sample_barcodes_trie = o_sample_barcodes_trie
        self.samples_num = samples_num

    def _barcode_positions_on_read(self, barcode, read):
        return [match.start() for match in list(re.finditer(barcode, read))]

    def _other_barcodes_exists_on_other_reads(self, read_num, fragment):
        if not self.o_sample_barcodes_trie:
            return True
        ocuppied_reads = []
        for r_num, read in enumerate(fragment):
            if r_num != read_num:
                for _ in self.o_sample_barcodes_trie.iter(read):
                    ocuppied_reads.append(r_num)
                    break
        return len(self.o_sample_barcodes_trie) <= len(ocuppied_reads)

    def _barcode_positions_on_fragment(self, barcode, fragment):
        for read_num, read in enumerate(fragment):
            for pos in self._barcode_positions_on_read(barcode, read):
                if self._other_barcodes_exists_on_other_reads(read_num, fragment):
                    yield (read_num, pos, read[pos+len(barcode):])

    def _barcode_positions_on_all_fragments(self):
        for fragment in self.fragments:
            for read_num, pos, bases_after_bar in self._barcode_positions_on_fragment(self.barcode, fragment):
                try:
                    self.bases_after_barcode[read_num].append((pos, bases_after_bar))
                except KeyError:
                    self.bases_after_barcode[read_num] = [(pos, bases_after_bar)]
                try:
                    self.locations_histogram[(read_num, pos)] += 1
                except KeyError:
                    self.locations_histogram[(read_num, pos)] = 1

    def _random_chance_for_barcode_location(self):
        barcode_len = len(self.barcode)
        rand_chance = ((1.0/4)**barcode_len)
        return rand_chance

    def _find_enriched_locations(self):
        significantly_enriched = []
        rand_chance = self._random_chance_for_barcode_location()
        frags_num = len(self.fragments)
        for location, freq in self.locations_histogram.items():
            if freq >= rand_chance*len(self.fragments):#freq >= rand_chance because we don't want concern to significantly low frequency
                # if stats.chisquare([freq, frags_num-freq], [rand_chance*frags_num, frags_num-rand_chance*frags_num]).pvalue < self.max_p_value_enrichment:
                    significantly_enriched.append(location+(freq,))
        self._remove_low_freq_locs(significantly_enriched)
        return significantly_enriched

    def _remove_low_freq_locs(self, locations_list):
        if locations_list:
            #remove low freq compared to other location of this barcode
            max_freq_location = max(locations_list, key=lambda x: x[2])[2]
            for location in locations_list[:]:
                if location[2]*MAX_DIFF_BARCODES_FREQ_WITHIN_SAMPLE < max_freq_location:
                    locations_list.remove(location)
            #remove low freq compared to expected freq. We know the number of samples and suppose that the distribution is equal until order of 2*MAX_DIFF_BARCODES_FREQ
            freq_per_read = {}
            for location in locations_list:
                try:
                    freq_per_read[location[0]] += location[2]
                except KeyError:
                    freq_per_read[location[0]] = location[2]
            for read, freq in freq_per_read.items():
                if freq*MAX_DIFF_BARCODES_FREQ_BETWEEN_SAMPLES < len(self.fragments)/self.samples_num:
                    for location in locations_list[:]:
                        if location[0] == read:
                            locations_list.remove(location)

    def _get_enriched_locations(self):
        self._barcode_positions_on_all_fragments()
        return self._find_enriched_locations()


    def get_umi_cycles(self, read_number, locations_list):
        bases_after_barcode = self.bases_after_barcode[read_number]
        umi_code = []
        #Because the possibility of shift of the barcode, the length can be different
        locations_this_read = [(read, pos) for read, pos, freq in locations_list if read == read_number]
        max_len_read = max([len(read) for pos, read in bases_after_barcode if (read_number, pos) in locations_this_read])
        for cycle in range(max_len_read):
            letters = dict([('A',0), ('G',0), ('C',0), ('T',0), ('N',0)])
            reads_num = 0
            for pos, read in bases_after_barcode:
                if (read_number, pos) not in locations_this_read:
                    continue
                try:
                    letter = read[cycle]
                    reads_num += 1
                    letters[letter] += 1
                #because different length
                except IndexError:
                    continue
            if max(letters.values())> MIN_UMI_ENRICHMENT*reads_num:
                umi_code.append('n')#It is not regular base, so, it is adapter or last letter of the short read
            else:
                umi_code.append('y')#It is random base, so, it is UMI.
        umi_code = ''.join(umi_code)
        def replacementN(match):
            return match.group(0).replace('n', 'y')
        def replacementY(match):
            return match.group(0).replace('y', 'n')
        umi_code = re.sub(r"(yn+y)", replacementN, umi_code)
        umi_code = re.sub(r"(ny+n)", replacementY, umi_code)
        #For cases of denerative sequence after the barcode of regular reads (not index reads), and it is not miRNA (there is shift):
        num_pos = len([loc for loc in locations_list if loc[0] == read_number])
        if len(umi_code)+len(self.barcode) == len(self.fragments[0][read_number]) and num_pos == 1 and len(self.fragments[0][read_number]) > 40:
            umi_code = ''
        return umi_code

    @staticmethod
    def reversed_complement(barcode):
        revcompl = lambda x: ''.join([{'A':'T','C':'G','G':'C','T':'A'}[B] for B in x][::-1])
        return revcompl(barcode)

    def get_locations(self):
        rc = False
        orig_barcode = self.barcode
        locations_list = self._get_enriched_locations()
        if not locations_list:
            self.barcode = self.reversed_complement(self.barcode)
            locations_list = self._get_enriched_locations()
            if not locations_list:
                self.barcode = orig_barcode
            rc=True
        barcode_locations = BarcodeLocations(self.barcode)
        for read_number, position, freq in locations_list:
            try:
                umi_code = self.get_umi_cycles(read_number, locations_list)
                barcode_locations.set_values(read_number, position, umi_code, freq)
            except BarcodeLocationError as e:
                logger.error(str(e))
        return barcode_locations, rc


class SampleDetails(object):
    LOG_CRASH = 'The enriched location of barcode %s (sample %s, lane %s) on read %s is invalid because is located on the same read of barcode: %s'
    LOG_TWO_REPLACED = 'The enriched locations of the replaced barcode %s (sample %s, lane %s) are not on the same reads like the other replaced barcode: %s (%s %s)'

    def __init__(self, sample_name, barcodes_list, lane, user_name, user_email, pi_name):
        self.sample_name = sample_name
        self.barcodes_list = barcodes_list
        self.barcodes_locations = []
        self.replaced_barcodes = ['-','-']
        self.lane = lane
        self.user_name = user_name
        self.user_email = user_email
        self.pi_name = pi_name

    def __str__(self):
        return repr("%s %s %s %s %s %s" %(self.sample_name, ' '.join(self.barcodes_list), self.lane, self.user_name, self.user_email, self.pi_name))

    #Return list of sorted BarcodLocations
    def _sort_barcodes_by_reads(self):
        barcodes_order = {}
        temp_barcodes_locations = []
        for barcode_locations in self.barcodes_locations:
            if barcode_locations.read_1 is None:
                temp_barcodes_locations.append(barcode_locations)
                continue
            if barcode_locations.read_1 in barcodes_order.keys():
                    barcodes_order[barcode_locations.read_2] = barcode_locations
            else:
                barcodes_order[barcode_locations.read_1] = barcode_locations
        self.barcodes_locations = [barcodes_order[sorted_key] for sorted_key in sorted(barcodes_order.keys())]+temp_barcodes_locations

    def less_freq_loc(self, barcode_locations):
        return 1 if barcode_locations.loc1_freq < barcode_locations.loc2_freq else 2

    def fix_invalid_locations(self, barcode_locations):
        if barcode_locations.read_2:
            if not barcode_locations._is_intersect(barcode_locations.min_pos_1, barcode_locations.max_pos_1, barcode_locations.min_pos_2, barcode_locations.max_pos_2):#We enforce the position on the one read is equal to position on the other read
                self.remove_location(barcode_locations, self.less_freq_loc(barcode_locations))
            #if the position in the 2 reads are equal (intersected), and 2 reads are shift, compare the intervals of the 2 locations
            elif barcode_locations.is_shift_1() and barcode_locations.is_shift_2():
                barcode_locations.min_pos_1, barcode_locations.max_pos_1 = barcode_locations._set_min_max(barcode_locations.min_pos_1, barcode_locations.max_pos_1, barcode_locations.min_pos_2)
                barcode_locations.min_pos_1, barcode_locations.max_pos_1 = barcode_locations._set_min_max(barcode_locations.min_pos_1, barcode_locations.max_pos_1, barcode_locations.max_pos_2)
                barcode_locations.min_pos_2, barcode_locations.max_pos_2 = barcode_locations.min_pos_1, barcode_locations.max_pos_1
            #If there is fixing location on one read and shift location on other read, we suppose that it is don't replaced read.
            elif not (not barcode_locations.is_shift_1() and not barcode_locations.is_shift_2()):
                self.remove_location(barcode_locations, self.less_freq_loc(barcode_locations))

    def remove_location(self, barcode_locations, read_num, other_barcode=None, log=None):
        if log == self.LOG_CRASH:
            logger.error(log %(barcode_locations.barcode, self.sample_name, self.lane, barcode_locations.read_2 if read_num == 2 else barcode_locations.read_1, other_barcode))
        elif log == self.LOG_TWO_REPLACED:
            logger.error(log %(other_barcode, self.sample_name, self.lane, barcode_locations.barcode, barcode_locations.read_1, barcode_locations.read_2))
        if read_num == 2:
            barcode_locations.read_2 = None
            barcode_locations.min_pos_2 = float('inf')
            barcode_locations.max_pos_2 = -float('inf')
            barcode_locations.umi_code2 = ''
            barcode_locations.loc2_freq = 0
        else:
            barcode_locations.read_1 = barcode_locations.read_2
            barcode_locations.min_pos_1 = barcode_locations.min_pos_2
            barcode_locations.max_pos_1 = barcode_locations.max_pos_2
            barcode_locations.umi_code1 = barcode_locations.umi_code2
            barcode_locations.loc1_freq = barcode_locations.loc2_freq
            self.remove_location(barcode_locations, 2)

    def fix_crashes_locations_between_barcodes(self):
        i=0
        for barcode_locations in self.barcodes_locations[:-1]:#Stores the reads of non replaced barcodes
            for o_barcode_locations in self.barcodes_locations[i+1:]:
                if barcode_locations.read_1 is None or o_barcode_locations.read_1 is None:
                    continue
                if not barcode_locations.is_replaced() and not o_barcode_locations.is_replaced():
                    if barcode_locations.read_1 == o_barcode_locations.read_1:
                        self.remove_location(barcode_locations, 1, o_barcode_locations.barcode, self.LOG_CRASH)
                        break #No location in barcode_location
                elif not barcode_locations.is_replaced() and o_barcode_locations.is_replaced():
                    if barcode_locations.read_1 == o_barcode_locations.read_1:
                        self.remove_location(o_barcode_locations, 1, barcode_locations.barcode, self.LOG_CRASH)
                    elif barcode_locations.read_1 == o_barcode_locations.read_2:
                        self.remove_location(o_barcode_locations, 2, barcode_locations.barcode, self.LOG_CRASH)
                elif barcode_locations.is_replaced() and not o_barcode_locations.is_replaced():
                    if barcode_locations.read_1 == o_barcode_locations.read_1:
                        self.remove_location(barcode_locations, 1, o_barcode_locations.barcode, self.LOG_CRASH)
                    elif barcode_locations.read_2 == o_barcode_locations.read_1:
                        self.remove_location(barcode_locations, 2, o_barcode_locations.barcode, self.LOG_CRASH)
                elif barcode_locations.is_replaced() and o_barcode_locations.is_replaced():
                    if not (barcode_locations.read_1 == o_barcode_locations.read_1 and barcode_locations.read_2 == o_barcode_locations.read_2):
                        if barcode_locations.read_1 == o_barcode_locations.read_1 or barcode_locations.read_1 == o_barcode_locations.read_2:
                            self.remove_location(barcode_locations, 1, o_barcode_locations.barcode, self.LOG_TWO_REPLACED)
                        elif barcode_locations.read_2 == o_barcode_locations.read_1 or barcode_locations.read_2 == o_barcode_locations.read_2:
                            self.remove_location(barcode_locations, 2, o_barcode_locations.barcode, self.LOG_TWO_REPLACED)
                        elif barcode_locations.read_1 != o_barcode_locations.read_1 and barcode_locations.read_1 != o_barcode_locations.read_2 \
                            and barcode_locations.read_2 != o_barcode_locations.read_1 and barcode_locations.read_2 != o_barcode_locations.read_2:
                            self.remove_location(barcode_locations, 2, o_barcode_locations, self.LOG_TWO_REPLACED)
            i+=1

    def _find_replaced_reads(self):
        replaced_reads = ['-','-']
        for barcode_locations in self.barcodes_locations:
            if barcode_locations.is_replaced():
                return [barcode_locations.read_1, barcode_locations.read_2]
        return replaced_reads

    def _get_all_o_barcodes_in_trie(self, barcode_num):
        all_o_barcodes_trie = ahocorasick.Automaton()
        for o_bar_num, o_barcode in enumerate(self.barcodes_list):
            if o_bar_num != barcode_num:
                all_o_barcodes_trie.add_word(o_barcode, o_barcode)
        all_o_barcodes_trie.make_automaton()
        return all_o_barcodes_trie

    def find_barcodes_on_fragments(self, fragments, p_value, samples_num):
        not_found_barcodes=0
        for bar_num, barcode in enumerate(self.barcodes_list):
            enriched_bar_location = EnrichedBarcodeLocation(barcode, fragments, p_value, self.sample_name, self.lane, self._get_all_o_barcodes_in_trie(bar_num), samples_num)
            barcode_locations, rc =  enriched_bar_location.get_locations()
            if barcode_locations.read_1 is not None:
                if rc:
                    logging.error("No enrichment found for the barcode %s (sample %s, lane %s), But the software found enrichment "
                                  "of the reversed complement of this barcode" %(barcode, self.sample_name, self.lane))
                self.fix_invalid_locations(barcode_locations)
            else:
                not_found_barcodes += 1
                logging.error("No enriched location found for barcode: %s (sample %s, lane %s)" %(barcode, self.sample_name, self.lane))
            self.barcodes_locations.append(barcode_locations)#Save the barcodes also if location have not found
        if not_found_barcodes == len(self.barcodes_list):
            raise BarcodeLocationError('No locations found for any barcode of sample %s, lane %s' %(self.sample_name, self.lane))
        else:
            self.fix_crashes_locations_between_barcodes()
            self._sort_barcodes_by_reads()
            self.replaced_barcodes = self._find_replaced_reads()


class Mismatches(object):
    def __init__(self, samples_details, equal_mis_per_sample=False, min_remains_per_mis=MIN_REMAINS_PER_MIS):
        self.samples_details = samples_details
        self.min_remains_per_mis = min_remains_per_mis
        self.equal_mis_per_sample = equal_mis_per_sample
        self.min_mismatch_per_reads = dict(zip([None]+[read for read in range(10)], [sys.maxsize for _ in range(11)]))


    @staticmethod
    def _find_identical_reads(barcode_locs, o_barcode_locs):
        equal1 = False
        equal2 = False
        if barcode_locs.read_1 is None or o_barcode_locs.read_1 is None:
            yield False
        else:
            if barcode_locs.read_1 == o_barcode_locs.read_1:
                equal1 = True
                yield PotentialIntersection(barcode_locs.read_1, barcode_locs.min_pos_1, barcode_locs.max_pos_1,
                                            o_barcode_locs.min_pos_1, o_barcode_locs.max_pos_1)
            if barcode_locs.read_1 == o_barcode_locs.read_2:
                equal1 = True
                yield PotentialIntersection(barcode_locs.read_1, barcode_locs.min_pos_1, barcode_locs.max_pos_1,
                                            o_barcode_locs.min_pos_2, o_barcode_locs.max_pos_2)
            if barcode_locs.read_2 == o_barcode_locs.read_1:
                equal2 = True
                yield PotentialIntersection(barcode_locs.read_2, barcode_locs.min_pos_2, barcode_locs.max_pos_2,
                                            o_barcode_locs.min_pos_1, o_barcode_locs.max_pos_1)
            if barcode_locs.read_2 == o_barcode_locs.read_2 and o_barcode_locs.read_2 is not None:
                equal2 = True
                yield PotentialIntersection(barcode_locs.read_2, barcode_locs.min_pos_2, barcode_locs.max_pos_2,
                                            o_barcode_locs.min_pos_2, o_barcode_locs.max_pos_2)
            if not equal1:
                yield False
            if barcode_locs.read_2 and o_barcode_locs.read_2:
                yield False
                yield False
            if barcode_locs.read_2 and not equal2:
                yield False
            if not barcode_locs.read_2 and o_barcode_locs.read_2:
                yield False

    def _enlarge_long_barcode(self, trimmed_r_barcode, trimmed_l_barcode, r_start, r_end, l_start, l_end, orig_is_right):
        len_r_barcode = len(trimmed_r_barcode)
        len_l_barcode = len(trimmed_l_barcode)
        if len_r_barcode > len_l_barcode:
            max_add_to_right = l_end-r_start-len_r_barcode+1
            max_add_to_left = r_end-l_start-len_r_barcode+1
            return 'X'*min(len_l_barcode-1, max_add_to_left)+trimmed_r_barcode+'X'*min(len_l_barcode-1, max_add_to_right), trimmed_l_barcode, orig_is_right
        else:
            max_add_to_right = r_end-l_start-len_l_barcode+1
            max_add_to_left = l_end-r_start-len_l_barcode+1
            return 'X'*min(len_r_barcode-1, max_add_to_left)+trimmed_l_barcode+'X'*min(len_r_barcode-1, max_add_to_right), trimmed_r_barcode, not orig_is_right

    def _find_maximum_overlap(self, barcode, start, end, o_barcode, o_start, o_end):
        min_end = min(end, o_end)
        max_start = max(start, o_start)
        if max_start > min_end:#No overlapping
            return None, None, None, None, None, None, None, None
        possible_overlap = min_end-max_start+1
        len_bar = len(barcode)
        len_o_bar = len(o_barcode)
        sum_remains_bar = 0
        sum_remains_o_bar = 0
        if len_bar>possible_overlap:
            sum_remains_bar = len_bar-possible_overlap
        if len_o_bar>possible_overlap:
            sum_remains_o_bar = len_o_bar-possible_overlap
        if start == max_start:
            return barcode[:possible_overlap], o_barcode[-possible_overlap:], start, end-sum_remains_bar, o_start+sum_remains_o_bar, o_end, sum_remains_bar, True
        else:
            return o_barcode[:possible_overlap], barcode[-possible_overlap:], o_start, o_end-sum_remains_o_bar, start+sum_remains_bar, end, sum_remains_bar, False

    # The lengthes of first and second are equal.
    def _hamming_dist(self, orig, compare):
        if len(orig) != len(compare):
            raise IOError('The lengthes of the strings %s and %s to comparing are don\'t equal' %(orig, compare))
        remains=0
        diff=0
        for i, j in zip(orig, compare):
            if j == 'X' :
                remains+=1
            elif i != j:
                diff+=1
        return diff, remains

    def _allowed_mis_number(self, diff, remains):
        return int(max(0, floor((diff-1)/2))+floor(remains/self.min_remains_per_mis))

    def _max_mismatches_between_pair(self, barcode_locs, o_barcode_locs):
        max_mismatches = len(barcode_locs.barcode)
        #loop on all possible locations of the barcodes
        for potential_intersection in Mismatches._find_identical_reads(barcode_locs, o_barcode_locs):
            diff, remains = 0,0
            if not potential_intersection:
                max_mismatches = min(max_mismatches, self._allowed_mis_number(0, len(barcode_locs.barcode)))
                continue
            barcode = barcode_locs.barcode
            o_barcode = o_barcode_locs.barcode
            end_pos = potential_intersection.max_pos + len(barcode) - 1
            o_end_pos = potential_intersection.o_max_pos + len(o_barcode) - 1
            #sum_remains is the cutted bases (bases which are overlap with empty)
            trimmed_r_barcode, trimmed_l_barcode, r_start, r_end, l_start, l_end, cutted_bases, orig_is_right = \
                self._find_maximum_overlap(barcode, potential_intersection.min_pos, end_pos, o_barcode, potential_intersection.o_min_pos, o_end_pos)
            if not trimmed_r_barcode:
                max_mismatches = min(max_mismatches, self._allowed_mis_number(0, len(barcode_locs.barcode)))
                continue
            long_trimmed_barcode, short_trimmed_barcode, orig_is_long = \
                self._enlarge_long_barcode(trimmed_r_barcode, trimmed_l_barcode, r_start, r_end, l_start, l_end, orig_is_right)
            len_long = len(long_trimmed_barcode)
            len_short = len(short_trimmed_barcode)
            if not orig_is_long:
                for i in range(len_long-len_short+1):
                    diff, remains = self._hamming_dist(short_trimmed_barcode, long_trimmed_barcode[i:i+len_short])
            else:
                short_trimmed_barcode_withX = short_trimmed_barcode + 'X'*(len_long-len_short)
                notXstart, notXend = re.search('[AGCTN.]+', long_trimmed_barcode).span()
                for i in reversed(range(notXstart-len_long+len_short-1, notXstart+1)):
                    trimmed_long_barcode = trimmed_r_barcode if orig_is_right else trimmed_l_barcode
                    diff, remains = self._hamming_dist(trimmed_long_barcode, self._cyclic_string(short_trimmed_barcode_withX, i, len(trimmed_long_barcode)))
            sum_remains = cutted_bases+remains # Add to sum_remains the bases that overlap to 'X', and those that don't overlap at all.
            max_mismatches = min(max_mismatches, self._allowed_mis_number(diff, sum_remains))
        return max_mismatches

    def _cyclic_string(self, string, start, length):
        miss = 0
        if start < 0:
            miss = max(0, length+start)
            return string[start:]+string[:miss]
        return string[start:start+length]+string[:miss]

    def _set_equal_mismatches_values(self, barcode_locs):
        self.min_mismatch_per_reads[barcode_locs.read_1]=min(self.min_mismatch_per_reads[barcode_locs.read_1], barcode_locs.mismatch)
        self.min_mismatch_per_reads[barcode_locs.read_2]=min(self.min_mismatch_per_reads[barcode_locs.read_2], barcode_locs.mismatch)

    def set_number_of_mismatches(self):
        for sample_details in self.samples_details:
            for barcode_locs in sample_details.barcodes_locations:
                for o_sample_details in self.samples_details:
                    if sample_details != o_sample_details:
                        #The barcode is not unique, so we can ignore this barcode
                        if not CheckIdentityBarcodes.unique_barcode(barcode_locs, o_sample_details.barcodes_locations):
                            barcode_locs.mismatch = min(barcode_locs.mismatch, len(barcode_locs.barcode))
                            self._set_equal_mismatches_values(barcode_locs)
                            continue
                        for o_barcode_loc in o_sample_details.barcodes_locations:
                            barcode_locs.mismatch = min(barcode_locs.mismatch, self._max_mismatches_between_pair(barcode_locs, o_barcode_loc))
                            self._set_equal_mismatches_values(barcode_locs)
        if self.equal_mis_per_sample:
            for sample_details in self.samples_details:
                for barcode_locs in sample_details.barcodes_locations:
                    barcode_locs.mismatch = min(len(barcode_locs.barcode), self.min_mismatch_per_reads[barcode_locs.read_1], self.min_mismatch_per_reads[barcode_locs.read_2])



class CheckIdentityBarcodes(object):
    def __init__(self, samples_details):
        self.samples_details = samples_details

    @staticmethod
    def are_intersect(bar1_locs, bar2_locs):
        #There is possible of intersection between the barcodes, we suppose that the positions in two reads are identical (after the fixing)
        if bar1_locs.min_pos_1 <= bar2_locs.max_pos_1 and bar1_locs.max_pos_1 >= bar2_locs.min_pos_1:
            return True
        elif bar2_locs.min_pos_1 <= bar1_locs.max_pos_1 and bar2_locs.max_pos_1 >= bar1_locs.min_pos_1:
            return True
        else:
            return False

    @staticmethod
    def unique_barcode(bar1_locs, o_barcodes_locations):
        for bar2_locs in o_barcodes_locations:
            if bar1_locs.barcode == bar2_locs.barcode:
                if bar1_locs.read_1 in (bar2_locs.read_1, bar2_locs.read_2) or bar1_locs.read_2 in (bar2_locs.read_1, bar2_locs.read_2):#there is possible of identity
                    if CheckIdentityBarcodes.are_intersect(bar1_locs, bar2_locs):
                        return False
        return True

    def _barcodes_are_identity(self, barcodes_locations, o_barcodes_locations):
        if len(barcodes_locations) != len(o_barcodes_locations):
            return False
        for bar1_locs in barcodes_locations:
            if not CheckIdentityBarcodes.unique_barcode(bar1_locs, o_barcodes_locations):
                return False
        return True

    def find_identity_barcodes(self):
        for sample_details in self.samples_details:
            for o_sample_details in self.samples_details:
                if sample_details != o_sample_details:
                    if self._barcodes_are_identity(sample_details.barcodes_locations, o_sample_details.barcodes_locations):
                        logging.error('The barcodes of samples %s and %s and their locations are identity' %(sample_details.sample_name, o_sample_details.sample_name))



class CheckInvalidShift(object):
    MIN_SUFFIX_PREFIX = 3
    MAX_SHIFT_INVALID = 2
    def __init__(self, samples_details):
        self.samples_details = samples_details

    @staticmethod
    def _invalid_shift(barcode, o_barcode, potential_intersection):
        invalid=[]
        for start_pos in range(potential_intersection.min_pos, potential_intersection.max_pos+1):
            for o_start_pos in range(potential_intersection.o_min_pos, potential_intersection.o_max_pos+1):
                if barcode == o_barcode and start_pos == o_start_pos:
                    continue
                end_pos = start_pos+len(barcode)-1
                o_end_pos = o_start_pos+len(o_barcode)-1
                if (start_pos > o_end_pos or o_start_pos > end_pos):
                    continue #don't overlapping
                l_start = min(start_pos, o_start_pos)
                r_start = max(start_pos, o_start_pos)
                l_bar, r_bar = (barcode, o_barcode) if l_start == start_pos else (o_barcode, barcode)
                if max(0, o_start_pos-start_pos)+max(0, end_pos-o_end_pos) > CheckInvalidShift.MAX_SHIFT_INVALID:#2
                    continue
                try:
                    len_comp = min(len(r_bar), len(l_bar[r_start-l_start:]))
                    if len_comp >= CheckInvalidShift.MIN_SUFFIX_PREFIX and l_bar[r_start-l_start:r_start-l_start+len_comp] == r_bar[:len_comp]:#3
                        invalid.append([start_pos, o_start_pos])
                        continue
                    else:
                        continue
                except IndexError:
                    continue
        return invalid

    def _save_common_shifts(self, invalid_shifts, potential_intersection):
        shifts = range(potential_intersection.min_pos, potential_intersection.max_pos+1)
        o_shifts = range(potential_intersection.o_min_pos, potential_intersection.o_max_pos+1)
        invalid_bar = []
        invalid_o_bar = []
        for invalid_shift in invalid_shifts:
            invalid_bar.append(invalid_shift[0])
            invalid_o_bar.append(invalid_shift[1])
        for invalid_shift in invalid_shifts:
            #Find common shift that is valid in one of the barcodes
            if invalid_shift[0] in invalid_bar and invalid_shift[0] not in invalid_o_bar and invalid_bar[0] in o_shifts:
                invalid_bar.remove(invalid_shift[0])
            elif invalid_shift[1] in invalid_o_bar and invalid_shift[1] not in invalid_bar and invalid_bar[1] in shifts:
                invalid_o_bar.remove(invalid_shift[1])
            else:
                invalid_bar.remove(invalid_shift[0])
                invalid_o_bar.remove(invalid_shift[1])
        return sorted(invalid_bar), sorted(invalid_o_bar)

    def _remove_invalid_shift(self, invalid_shifts_bar, invalid_shifts_o_bar, barcode_locs, o_barcode_locs, invalid_read):
        if invalid_shifts_bar:
            if barcode_locs.read_1 == invalid_read:
                barcode_locs.max_pos_1 = invalid_shifts_bar[0]-1
            elif barcode_locs.read_2 == invalid_read:
                barcode_locs.max_pos_2 = invalid_shifts_bar[0]-1
        if invalid_shifts_o_bar:
            if o_barcode_locs.read_1 == invalid_read:
                o_barcode_locs.max_pos_1 = invalid_shifts_o_bar[0]-1
            elif o_barcode_locs.read_2 == invalid_read:
                o_barcode_locs.max_pos_2 = invalid_shifts_o_bar[0]-1

    def _compare_and_fix_barcodes_pair(self, barcodes_locations, o_barcodes_locations):
        if len(barcodes_locations) != len(o_barcodes_locations):
            return False
        for barcode_locs, o_barcode_locs in zip(barcodes_locations, o_barcodes_locations):
            for potential_intersection in Mismatches._find_identical_reads(barcode_locs, o_barcode_locs):
                if potential_intersection:
                    invalid_shifts = self._invalid_shift(barcode_locs.barcode, o_barcode_locs.barcode, potential_intersection)
                    invalid_shifts_bar, invalid_shifts_o_bar = self._save_common_shifts(invalid_shifts, potential_intersection)
                    self._remove_invalid_shift(invalid_shifts_bar, invalid_shifts_o_bar, barcode_locs, o_barcode_locs, potential_intersection.read)

    def fix_invalid_shifts(self):
        for sample_details in self.samples_details:
            for o_sample_details in self.samples_details:
                if sample_details != o_sample_details:
                    self._compare_and_fix_barcodes_pair(sample_details.barcodes_locations, o_sample_details.barcodes_locations)



class ReadBarcodeFile(object):
    def __init__(self, file):
        self.csv_file = open(file)
        self.reader = csv.reader(self.csv_file)

    def get_samples_details(self, lane):
        samples_details = {}
        for sample_details in self.get_sample_details(lane):
            if sample_details.sample_name in samples_details:
                random_name = '_'.join([sample_details.sample_name, ''.join(random.sample(string.digits, 3))])
                logger.warning('The sample name %s already exists in lane %s. The system will change its name to %s'
                             %(sample_details.sample_name, lane, random_name))
                sample_details.sample_name = random_name
            samples_details[sample_details.sample_name] = sample_details
        return samples_details.values()

    def get_sample_details(self, lane):
        self.count = 0
        for line in self.reader:
            if not line or line[0].startswith('#'): #Comment line
                continue
            try:
                _lane, sample_name, barcode1, barcode2, barcode3, user_name = line[:6]
            except ValueError:
                raise Exception('Missing or there exists extra field(s) in %s file in line %s' %(self.csv_file.name, line))
            user_email, pi_name = ['']*2
            if len(line)>6:
                user_email = line[6]#Optional
            if len(line)>7:
                pi_name = line[7]#Optional
            if int(_lane) == lane:
                self.count+=1
                self.validate_input(line, lane, sample_name, user_name)
                yield SampleDetails(sample_name, self.remove_empty_barcodes([barcode1, barcode2, barcode3], sample_name, lane), lane, user_name, user_email, pi_name)
        if self.count == 0:
            raise Exception('No entries for lane %s in file %s' %(lane, self.csv_file.name))

    def validate_input(self, line, lane, *args):
        for i in args:
            if i == '-' or '':
                raise Exception('Missing values in line %s of lane %s' %(line, lane))

    def remove_empty_barcodes(self, barcodes, sample, lane):
        while True:
            try:
                barcodes.remove('-')
            except ValueError:
                break
        if not barcodes:
            raise Exception('No barcodes in sample %s in lane %s' %(sample, lane))
        return barcodes


class WriteBarcodeSheet(object):
    def __init__(self, lanesBarcodesLocations, barcodesheet_file):
        self.lanesBarcodesLocations = lanesBarcodesLocations
        csv_writter = open(barcodesheet_file, 'w')
        self.writter = csv.writer(csv_writter)

    def print_header(self):
        self.writter.writerow(['#lane','project_name','sample_name','sub_sample_name','tag1_sequence','tag2_sequence',
                               'tag3_sequence','tag1_name','tag2_name','tag3_name','master_tag','cut_tag1','cut_tag2',
                               'cut_tag3','maximal_mismatches_tag1','maximal_mismatches_tag2','maximal_mismatches_tag3',
                               'maximal_offset_tagged_read1','maximal_offset_tagged_read2','maximal_offset_tagged_read3',
                               'reversed_first_tag','reversed_second_tag','tag_locations_R1','tag_locations_R2',
                               'tag_locations_R3','PI_name','user_email'])

    def write_file(self):
        self.print_header()
        for lane_barcodes in self.lanesBarcodesLocations:
            for sample_details in lane_barcodes.samples_details:
                fragment_descriptor = ['y'*read_len for read_len in lane_barcodes.read_lengths]
                properties = [['-' for _ in range(3)] for _ in range(4)]
                signed_reads = []
                for i, barcode_loc in enumerate(sample_details.barcodes_locations):
                    read = barcode_loc.read_1
                    len_umi = len(barcode_loc.umi_code1)
                    umi_code = barcode_loc.umi_code1
                    if read in signed_reads:
                        read = barcode_loc.read_2
                        len_umi = len(barcode_loc.umi_code2)
                        umi_code = barcode_loc.umi_code2
                    signed_reads.append(read)
                    properties[0][i] = barcode_loc.barcode
                    properties[1][i] = 'yes'
                    properties[2][i] = barcode_loc.mismatch
                    properties[3][i] = '0l0r' if read is None else '0l'+str(barcode_loc.max_pos_1-barcode_loc.min_pos_1)+'r'
                    if read is not None:
                        len_bar = len(barcode_loc.barcode)
                        tagged_read = fragment_descriptor[read]
                        fragment_descriptor[read] = tagged_read[:barcode_loc.min_pos_1]+'i'*len_bar+umi_code+tagged_read[barcode_loc.min_pos_1+len_bar+len_umi:]
                self.writter.writerow([sample_details.lane, sample_details.user_name, sample_details.sample_name, '-'] +
                                      properties[0] + properties[0] + ['-'] + properties[1] + properties[2] + properties[3] +
                                      self.printable_replaced(sample_details.replaced_barcodes) + fragment_descriptor +
                                      [sample_details.pi_name, sample_details.user_email])

    def printable_replaced(self, replaced_barcodes):
        if replaced_barcodes[0] != '-':
            return [replaced_barcodes[0]+1,replaced_barcodes[1]+1]
        return replaced_barcodes


class LaneBarcodesLocations(object):
    #samples_barcodes is dictionary.
    #keys are sample names, values are the SampleDetails objects.
    def __init__(self, lane, fragments, equal_mis_per_sample, samples_num, max_p_value_enrichment=MAX_P_VALUE_ENRICHMENT):
        self.lane = lane
        self.fragments = fragments
        self.equal_mis_per_sample = equal_mis_per_sample
        self.samples_num = samples_num
        self.read_lengths = [len(read) for read in self.fragments[0]]
        self.reads_num = len(self.read_lengths)
        self.samples_details = None
        self.max_p_value_enrichment = max_p_value_enrichment

    def read_sample_details(self, barcodes_list_file):
        self.samples_details = ReadBarcodeFile(barcodes_list_file).get_samples_details(self.lane)

    def update_samples(self):
        for sample_details in self.samples_details:
            try:
                sample_details.find_barcodes_on_fragments(self.fragments, self.max_p_value_enrichment, len(self.samples_details) if self.samples_num==0 else self.samples_num)
            except BarcodeLocationError as e:
                logging.error(e)
        CheckIdentityBarcodes(self.samples_details)
        CheckInvalidShift(self.samples_details).fix_invalid_shifts()
        Mismatches(self.samples_details, self.equal_mis_per_sample).set_number_of_mismatches()

