from collections import namedtuple
import copy
import gc
import glob
import logging
import multiprocessing
import os
import timeit

from spid.reader import SingleFastqFileSetReader, InputFilesReader, FilesReadingRanges
from spid.writer import OutputFilesManager, BulkWriter
from spid.build_data import SampleReader
from spid.io import OutputDirectoryTreeCreator
from spid.settings import MAX_OUTPUT_BUFFERS_QUEUE_SIZE, FIND_LAST_APPEARANCE
from spid.tagstree import TagsTreeBuilder
from spid.stats import create_statistics

logger = logging.getLogger(__name__)


DEMULTIPLEXING_PARAMETER_NAMES = ['barcode_sheet', 'output_dir', 'casava_folders_structure', 'read_lengths', 'reads_num',
                                  'output_buffer_size', 'casava_output_dir', 'fastq_cluster_count',
                                  'reader_chunk_size', 'max_read_processes', 'output_format', 'max_flush_processes',
                                  'force', 'find_last_appearance']

DemultiplexingParameters = namedtuple('DemultiplexingParameters', field_names=DEMULTIPLEXING_PARAMETER_NAMES)


class FragmentDemultiplexer(object):
    """
    Demultiplex a single fragment, meaning - determine to which sample the given fragment belongs according to its tags
    """
    def __init__(self, samples_details, tags_trees, reads_num, read_lengths, last_appear=FIND_LAST_APPEARANCE):
        self.samples_details = samples_details
        self.tags_trees = tags_trees
        self.reads_num = reads_num
        self.read_lengths = read_lengths
        self.last_appear = last_appear

    def demultiplex_fragment(self, fastq_fragment):
        sequence_fragment = []
        for i in range(self.reads_num):
            sequence_fragment.append(fastq_fragment[i][1])

        # raw_tags is list of hash tables structure, each of hash is belongs to one tagged reads -
        # keys are sample names and values are Tag objects which were found in the tree
        raw_tags = self.find_tags_in_sequences(sequence_fragment)
        # match_results is list, in first position contains the sample name that its tags found in all reads, 
        # and the other locations contain the Tag objects
        # If not found sample that their tags is found in all reads, the first location contain 'Undetermined' string 
        # (or Undetermined_subSampleName if only master tag found), and None in the second location
        match_result = self.check_match_results(raw_tags)
        tags = match_result[1]
        if match_result[0] != 'Undetermined' and not match_result[0].startswith('Undetermined_'):
            (reads_with_found_tag, sample_name, found_tags_numbers, tag_length) = ([], [], [], [])
            (init_tag_indices, real_init_tag_indices, real_end_tag_indices, cut_tags) = ([], [], [], [])
            sample = self._initialize_results(tags, reads_with_found_tag, sample_name,
                                              found_tags_numbers, tag_length, init_tag_indices, real_init_tag_indices,
                                              real_end_tag_indices, cut_tags)
            self._remove_tags_and_n_from_reads_with_found_tag(reads_with_found_tag, found_tags_numbers,
                                                              fastq_fragment, sample, cut_tags, init_tag_indices,
                                                              real_init_tag_indices, real_end_tag_indices)
            self._add_tag_to_header_line(fastq_fragment, sample.tags, found_tags_numbers)
            return fastq_fragment, sample.sample_name
        else:  # undetermined
            return fastq_fragment, match_result[0]

    def find_tags_in_sequences(self, sequences):
        """
        Search for an index matching the given fragment, which is actually a list of sequences, one per read
        The return code is a list of tags - one per indexed read
        """
        tags = []
        for i in range(len(self.tags_trees)):
            if self.tags_trees[i]:
                tag = self.tags_trees[i].lookup(sequences[i], self.last_appear)
                if tag:
                    tags.append(tag)
        return tags

    @staticmethod
    def _return_master_tag(master_sample_name, master_tag_num):
        if master_tag_num is None:
            return 'Undetermined', None
        else:
            return 'Undetermined_' + master_sample_name, None

    # you can call to this function only if you found the all tags. (else the reversed reads number are not
    # equal to the tag numbers, and cannot be used as indexes in tag)
    def append_if_two_found_tags_are_not_identical(self, sample_name, found_tags, new_tag,
                                                   num_samples_until_now):
        """
        Find if two tags found in two trees are identical (one reversed tag)
        """
        if self.samples_details[sample_name].reversed_tagged_reads[0] is None:
            # there are no reversed tags, the tags cannot be identical
            found_tags.append(new_tag)
            return True
        if num_samples_until_now == 1:
            if found_tags[0].tag_number != new_tag.tag_number:
                # first tag_number is identical to second tag_number (the same tag that is reversed)
                found_tags.append(new_tag)
                return True
        if num_samples_until_now == 2:
            if found_tags[0].tag_number != new_tag.tag_number \
                    and found_tags[1].tag_number != new_tag.tag_number:
                found_tags.append(new_tag)
                return True
        return False

    def _search_common_master_tag(self, sample_name, tag):
        """
        Functionality:
        Find in the results of one tree if it is contains master tag.
        Input:
        Details of the found tag
        Output:
        If found return the master sample name and the master tag number, else return None,None
        """        
        master_name = tag.master_sample_name
        tag_num = tag.tag_number
        master_tag_num = self.samples_details[sample_name].master_tag
        if master_tag_num == tag_num:
            return master_name, master_tag_num  # master tag is unique, therefore we can stop now
        return None, None

    def check_match_results(self, raw_tags):
        """
        Functionality:
        Check the results that found in the trees. Search intersection between the results of the trees.
        Input:
        The tags from all trees
        Output:
        If found triplet return: (True/False(tagged read 1), True/False(tagged read 2), True/False(tagged read 3), sample_name/Undetermined_sample/Undetermined, tag/None)
        Return true if in correspond tagged read was found tag. 
        """
        if not raw_tags:
            return 'Undetermined', None
        one = None
        pair = None
        found_tags_buffer = {}
        (master_sample_name, master_tag_num) = (None, None)
        for i in range(len(raw_tags)):  # loop over the trees
            for (sample_name, tag) in raw_tags[i].items():  # loop over the sample names
                if master_sample_name is None:  # master tag is still missing
                    (master_sample_name, master_tag_num) = self._search_common_master_tag(sample_name, tag)
                sample_details = self.samples_details[sample_name]
                if sample_details.tags_num == 1:
                    if not one:  # save the first result. also in case of 'R' if found the tag twice, we save the first
                        one = (sample_name, [tag])
                elif sample_details.tags_num == 2:
                    if found_tags_buffer.has_key(sample_name):
                        if self.append_if_two_found_tags_are_not_identical(sample_name, found_tags_buffer[sample_name], tag, 1):
                            pair = (sample_name, found_tags_buffer[sample_name])
                    else:
                        found_tags_buffer[sample_name] = [tag]
                elif sample_details.tags_num == 3:#to found sample expected be 3 tags
                    if found_tags_buffer.has_key(sample_name):
                        num_tags = len(found_tags_buffer[sample_name])
                        if num_tags == 1:
                            self.append_if_two_found_tags_are_not_identical(sample_name, found_tags_buffer[sample_name], tag, 1)
                        elif num_tags == 2 and self.append_if_two_found_tags_are_not_identical(sample_name, found_tags_buffer[sample_name],tag, 2):
                            return (sample_name, found_tags_buffer[sample_name])
                    else:
                        found_tags_buffer[sample_name] = [tag]
        #first search if we have pair, only if not we return the sample with the one tag
        if pair is not None:
            return pair
        elif one is not None:
            return one
        else:
            return self._return_master_tag(master_sample_name, master_tag_num)

    def _initialize_results(self, tags, reads_with_found_tag, sample_name, found_tags_numbers, tag_length,
                            init_tag_indices, real_init_tag_indices, real_end_tag_indices, cut_tags):
        """
        Functionality:
        Initialize the details of the found tags, the not found tags is not initialize to variables. 
        Input:
        The found parameters
        Output:
        The organized found parameters 
        """
        sample = []
        found_tags_num = len(tags)
        for i in range(found_tags_num):  # i is index on all potential tags
            sample_name.append(tags[i].sample_name)
            tag_number = tags[i].tag_number
            found_tags_numbers.append(tag_number)
            tag_length.append(tags[i].tag_length)
            real_init_tag_indices.append(tags[i].tag_start)
            reads_with_found_tag.append(tags[i].tree_num)
            real_end_tag_indices.append(real_init_tag_indices[i]+tag_length[i]-1)
            sample = self.samples_details[sample_name[i]]
            init_tag_indices.append(sample.init_tag_indices[tag_number])
            cut_tags.append(sample.cut_tags[tag_number])
        return sample

    @staticmethod
    def _cut_sequence(sequence, n_and_tag_indexes, cut_tag):
        """
        Functionality:
        Remove the tags and 'n' from the sequence.
        Input:
        One sequence from fragment, indexes of the tag and 'n'. 
        Output:
        The sequence without tag and 'n'.
        """
        removed = 0 
        for (start, end, char_type) in n_and_tag_indexes:
            start_m_removed = start-removed
            end_m_removed_p_1 = end-removed+1
            if char_type != 'i' or cut_tag: 
                sequence[1] = sequence[1][:start_m_removed] + sequence[1][end_m_removed_p_1:]
                sequence[3] = sequence[3][:start_m_removed] + sequence[3][end_m_removed_p_1:]
                removed += (end-start+1)

    @staticmethod
    def _repair_n_tag_indexes(n_and_tag_indexes, new_intervals):
        """
        Functionality:
        Repairs the intervals of i,y,n in the read according the real location of the tag in read (because of the offset)
        Input:
        The original intervals of one read
        Output:
        The correct intervals.
        """
        for n_start, n_end, n_char_type in new_intervals:
            i = 0
            inserted = False
            n_start_m_1 = n_start-1 #for optimization
            n_end_p_1 = n_end+1 #for optimization
            for start, end, char_type in n_and_tag_indexes[:]:
                if n_start <= end and n_end >= end:#new interval exceed from right side of old interval - we cut the end of old interval 
                    if n_start_m_1 < start:
                        del n_and_tag_indexes[i]
                        i -= 1
                    else:
                        n_and_tag_indexes[i][1] = n_start_m_1
                    if (n_char_type == 'i' or n_char_type == 'n') and not inserted:
                        n_and_tag_indexes.insert(i+1, [n_start, n_end, n_char_type])
                        i += 1
                        inserted = True                
                elif n_start <= start and n_end >= start:#new interval exceed from left side of old interval - we cut the initial of old interval 
                    if (n_char_type == 'i' or n_char_type == 'n') and not inserted:
                        n_and_tag_indexes.insert(i, [n_start, n_end, n_char_type])
                        i += 1
                        inserted = True
                    if n_end_p_1 > end:
                        del n_and_tag_indexes[i]
                        i -= 1
                    else:
                        n_and_tag_indexes[i][0] = n_end_p_1           
                elif n_start > start and n_end < end:#new interval is contained into the old interval - we cut the old to 2 pieces
                    n_and_tag_indexes[i][1] = n_start_m_1
                    if (n_char_type == 'i' or n_char_type == 'n') and not inserted:
                        n_and_tag_indexes.insert(i+1, [n_start, n_end, n_char_type])
                        i += 1
                    n_and_tag_indexes.insert(i+1, [n_end_p_1, end, char_type])
                    break
                i += 1

    def remove_tag_and_n(self, read_index, sequence, n_and_tag_indexes, cut_tag,
                         init_tag_index, real_init_tag_index, real_end_tag_index):
        len_seq = len(sequence[1])
        if real_init_tag_index == init_tag_index or not cut_tag:  # there was not offset
            self._cut_sequence(sequence, n_and_tag_indexes, cut_tag)
        else:  # cut index with offset
            cpy_n_and_tag_indexes = copy.deepcopy(n_and_tag_indexes)
            tag_len = real_end_tag_index-real_init_tag_index+1
            offset = real_init_tag_index-init_tag_index
            if offset > 0:  # the offset was forward 3' edge
                exceed_num = max(real_end_tag_index-len_seq+1, 0)
                y_interval = [init_tag_index, init_tag_index+offset-1, 'y']
                i_interval = [y_interval[1]+1, y_interval[1]+(tag_len-exceed_num), 'i']
                n_interval = [0, real_init_tag_index-1, 'n']
                if init_tag_index != 0:  # the tag is in middle of sequence
                    self._repair_n_tag_indexes(cpy_n_and_tag_indexes, [y_interval, i_interval])
                else:  # if (init_tag_index==0) we remove all bases in left of real_init_tag_index
                    self._repair_n_tag_indexes(cpy_n_and_tag_indexes, [n_interval, i_interval])
            else:  # the offset was backward 5' edge
                offset = -offset
                seq_start = max(real_init_tag_index, 0)  # if the tag exceeds behind the 5', we begin from 0.
                exceed_num = min(real_init_tag_index, 0)  # number of bases that exceed behind the 5'
                i_end = tag_len + exceed_num - 1
                if init_tag_index + tag_len == self.read_lengths[read_index]:  # optimization of: init_tag_index+tag_len-1 == read_length-1:
                    i_end = self.read_lengths[read_index] - 1
                i_interval = [seq_start, i_end, 'i']
                y_interval = [i_interval[1] + 1, i_interval[1] + offset, 'y']
                self._repair_n_tag_indexes(cpy_n_and_tag_indexes, [i_interval, y_interval])
            self._cut_sequence(sequence, cpy_n_and_tag_indexes, cut_tag)
    
    def _remove_tags_and_n_from_reads_with_found_tag(self, reads_with_found_tag, found_tags_numbers, lines,
                                                     sample, cut_tags, init_tag_indices, real_init_tag_indices,
                                                     real_end_tag_indices):
        """
        Functionality:
        Send the tagged reads to trimming of the n and tags, and the not tagged reads to trimming of the n only. 
        Input:
        All reads and the tag that found in them.
        Output:
        no output
        """
        tagged_index = -1 
        for read_index in range(self.reads_num):
            if read_index in reads_with_found_tag: # tagged read that found it tag - remove tag and 'n'
                tagged_index += 1
                tag_number = found_tags_numbers[tagged_index]
                self.remove_tag_and_n(read_index, lines[read_index], sample.n_and_tag_indexes[read_index], cut_tags[tag_number], 
                                      init_tag_indices[tag_number], real_init_tag_indices[tag_number], 
                                      real_end_tag_indices[tag_number])
            else: # not tagged read - remove 'n' only
                self.remove_tag_and_n(read_index, lines[read_index], sample.n_and_tag_indexes[read_index], [], [], [], [])

    def _add_tag_to_header_line(self, sequences, tags, found_tags_numbers):
        """
        Functionality:
        Add the found tag to header line of the sequences. If the tag is reversed, the order of the tags will be as them found. 
        Input:
        Sequences and found tags
        Output:
        Sequences with tags in header lines
        """
        for seq in sequences:
            seq_split = seq[0].split(":")
            if seq_split[-1]:
                seq[0] = ":".join(seq_split[:-1])+':'
            comma = ''
            for tag_number in found_tags_numbers:
                seq[0] += (comma + tags[tag_number])
                comma = ','


class InputBatchDemultiplexer(object):
    """De-multiplexes one set of FASTQ files"""

    def __init__(self, input_fastq_set_with_ranges, demultiplexing_params, lane, output_buffers_queue):
        self.demultiplexing_params = demultiplexing_params
        self.reader = SingleFastqFileSetReader(input_fastq_set_with_ranges, demultiplexing_params.reader_chunk_size)
        self.lane = lane
        self.output_buffers_queue = output_buffers_queue

        self.samples_details = create_sample_details(self.demultiplexing_params.barcode_sheet, self.lane,
                                                     self.demultiplexing_params.reads_num)
        self.sample_names = self.samples_details.keys()
        self.tags_trees = TagsTreeBuilder(self.samples_details, self.demultiplexing_params.reads_num, self.demultiplexing_params.read_lengths).build()
        self.fragment_demultiplexer = FragmentDemultiplexer(self.samples_details, self.tags_trees, self.demultiplexing_params.reads_num,
                                                            self.demultiplexing_params.read_lengths, self.demultiplexing_params.find_last_appearance)
        self.output_buffers = dict([(sample_name, []) for sample_name in self.sample_names])

    def flush_output_buffer_for_sample(self, sample_name):
        # buffer is full - flush and make room for new fragments
        output_buffer = self.output_buffers[sample_name]
        self.output_buffers[sample_name] = []
        logger.debug('Pushing output buffer with %s sequences belonging to %s to queue'
                     % (len(output_buffer), sample_name))
        self.output_buffers_queue.put((sample_name, output_buffer))

    def demultiplex_batch(self):
        while True:
            try:
                fastq_fragment = self.reader.next()
            except StopIteration:
                break
            fragment, sample_name = self.fragment_demultiplexer.demultiplex_fragment(fastq_fragment)
            output_buffer = self.output_buffers[sample_name]
            output_buffer.append(fragment)
            if len(output_buffer) == self.demultiplexing_params.output_buffer_size:
                self.flush_output_buffer_for_sample(sample_name)
        logger.debug('Finished reading all fragments from this batch, writing the remaining fragments to output files')
        for (sample_name, output_buffer) in self.output_buffers.items():
            if output_buffer:  # skip empty buffers
                self.flush_output_buffer_for_sample(sample_name)


# TODO try to find a better place for this method
def create_sample_details(barcode_sheet, lane, reads_num):
    samples_reader = SampleReader(barcode_sheet, [lane], reads_num)
    samples_details = samples_reader.samples_details
    return samples_details[lane]


class FlushProcess(multiprocessing.Process):
    def __init__(self, demultiplexing_params, lane, output_buffers_queue, output_locks,
                 num_sequences_per_sample_dict, **kwargs):
        super(FlushProcess, self).__init__(**kwargs)
        self.demultiplexing_params = demultiplexing_params
        self.lane = lane
        self.output_buffers_queue = output_buffers_queue
        self.output_locks = output_locks
        self.num_sequences_per_sample_dict = num_sequences_per_sample_dict
        self.samples_details = create_sample_details(self.demultiplexing_params.barcode_sheet, self.lane,
                                                     self.demultiplexing_params.reads_num)
        self.sample_names = self.samples_details.keys()
        self.not_empty_reads_num = dict([(sample_name, sample_details.non_empty_reads)
                                         for (sample_name, sample_details) in self.samples_details.items()])
        self.output_files_manager = OutputFilesManager(self.demultiplexing_params.output_dir, self.lane,
                                                       self.samples_details, self.demultiplexing_params.output_format,
                                                       self.demultiplexing_params.fastq_cluster_count)

    def run(self):
        logger.info('Flush process started and will end when output buffers queue is empty')
        while True:
            item = self.output_buffers_queue.get()
            if item is None:  # None means that we processed all input files
                logger.info('No more output buffers, exiting')
                break
            sample_name, output_buffer = item
            assert sample_name in self.output_locks
            lock = self.output_locks[sample_name]
            logger.debug('acquiring lock for sample %s ...' % sample_name)
            lock.acquire()
            logger.debug('lock for sample %s acquired' % sample_name)
            try:
                self.flush_output_buffer_for_sample(sample_name, output_buffer)
            except Exception as e:
                logger.error('Error during flush: %s' % e)
                logger.exception(e)
                logger.error('Output buffers approximate queue size: %s' % (self.output_buffers_queue.qsize(),))
                raise e
            finally:
                lock.release()
                logger.debug('lock for sample %s released' % sample_name)

    def get_output_filenames_for_sample(self, sample_name):
        return self.output_files_manager.get_output_filenames(sample_name,
                                                              self.num_sequences_per_sample_dict[sample_name])

    def flush_output_buffer_for_sample(self, sample_name, output_buffer):
        output_filenames, num_sequences_in_output_files = self.get_output_filenames_for_sample(sample_name)
        max_buffer_size = min(self.demultiplexing_params.output_buffer_size,
                              self.demultiplexing_params.fastq_cluster_count - num_sequences_in_output_files)
        if max_buffer_size < len(output_buffer):
            self.flush_output_buffer_for_sample(sample_name, output_buffer[:max_buffer_size])
            self.flush_output_buffer_for_sample(sample_name, output_buffer[max_buffer_size:])
        else:
            self._flush_output_buffer(output_filenames, output_buffer, sample_name,
                                      self.demultiplexing_params.output_format)

    def _flush_output_buffer(self, output_filenames, output_buffer, sample_name, output_format):
        not_empty_reads_num = self.not_empty_reads_num[sample_name]
        logger.debug('Sample %s : flushing %s sequences' % (sample_name, len(output_buffer)))
        logger.debug('Sample %s : started flushing %s sequences to output files %s'
                     % (sample_name, len(output_buffer), output_filenames))
        writer = BulkWriter(output_filenames, sample_name, not_empty_reads_num, output_format)
        num_flushed_sequences = writer.flush(output_buffer)
        logger.debug('Sample %s : finished flushing %s sequences to output files %s'
                     % (sample_name, num_flushed_sequences, output_filenames))
        self.num_sequences_per_sample_dict[sample_name] += num_flushed_sequences


class BatchDemultiplexerProcess(multiprocessing.Process):
    def __init__(self, demultiplexing_params, lane, input_files_queue, output_buffers_queue, **kwargs):
        super(BatchDemultiplexerProcess, self).__init__(**kwargs)
        self.demultiplexing_params = demultiplexing_params
        self.lane = lane
        self.input_files_queue = input_files_queue
        self.output_buffers_queue = output_buffers_queue

    def run(self):
        logger.info('Batch demultiplexing started and will end when input files queue is empty')
        while True:
            input_fastq_set_with_ranges = self.input_files_queue.get()
            if input_fastq_set_with_ranges == [None, None, None]:  # None means that we processed all input files
                logger.info('No more input files, exiting')
                break
            start_time = timeit.default_timer()
            logger.debug('Starting batch demultiplexing')
            batch_demultiplexer = InputBatchDemultiplexer(input_fastq_set_with_ranges, self.demultiplexing_params,
                                                          self.lane, self.output_buffers_queue)
            try:
                batch_demultiplexer.demultiplex_batch()
            finally:
                del batch_demultiplexer
            gc.collect()
            elapsed_time_in_milliseconds = (timeit.default_timer() - start_time) * 1000
            logger.debug('Batch demultiplexing ended (%.2f ms.)' % (elapsed_time_in_milliseconds,))


class LaneDemultiplexer(object):
    """
    Demultiplex FASTQ files of originating from a single lane
    """
    def __init__(self, demultiplexing_params, lane):
        self.demultiplexing_params = demultiplexing_params
        self.lane = lane
        self.input_files_reader = InputFilesReader(self.lane, self.demultiplexing_params.casava_output_dir,
                                                   self.demultiplexing_params.casava_folders_structure)
        self.manager = multiprocessing.Manager()
        self.input_files_queue = self.manager.Queue()
        self.samples_details = create_sample_details(self.demultiplexing_params.barcode_sheet, self.lane,
                                                     self.demultiplexing_params.reads_num)
        self.sample_names = self.samples_details.keys()
        self.output_locks = dict([(sample_name, self.manager.Lock()) for sample_name in self.sample_names])
        self.output_buffers_queue = self.manager.Queue(maxsize=MAX_OUTPUT_BUFFERS_QUEUE_SIZE)
        self.batch_demultiplexer_processes = []
        self.flush_processes = []
        self.num_sequences_per_sample_dict = self.manager.dict([(sample_name, 0) for sample_name in self.sample_names])
        OutputDirectoryTreeCreator(self.samples_details, self.demultiplexing_params.output_dir, self.lane,
                                   self.demultiplexing_params.force).create_directory_tree()

    def _init_input_fastq_files_path(self):
        project_dirs = glob.glob(os.path.join(self.demultiplexing_params.casava_output_dir, 'Project_*'))
        if not project_dirs:
            raise IOError("Project_* directories do not exist in %s, please check if CASAVA ran successfully"
                          % self.demultiplexing_params.casava_output_dir)
        if len(project_dirs) > 1:
            raise IOError("Multiple project directories found in %s, SPID can handle only a single project directory "
                          "in each lane. Project directories: %s"
                          % (self.demultiplexing_params.casava_output_dir, project_dirs))
        return os.path.join(project_dirs[0], 'Sample_lane' + str(self.lane))

    def demultiplex_one_lane(self):
        file_names = self.input_files_reader.get_fastq_filenames()
        input_files_and_ranges = FilesReadingRanges(file_names, self.input_files_reader.gzipped_input_files).get_reading_range_within_file(self.demultiplexing_params.max_read_processes)
        for input_fastq_set_with_ranges in input_files_and_ranges:
            self.input_files_queue.put(input_fastq_set_with_ranges)
        for i in range(self.demultiplexing_params.max_read_processes):
            # signal demultiplexing sub-processes when they reach the end (a.k.a "poison pill")
            self.input_files_queue.put([None, None, None])
        # start sub-processes in charge of demultiplexing
        for i in range(min(self.demultiplexing_params.max_read_processes, input_files_and_ranges)):
            batch_demultiplexer_process = BatchDemultiplexerProcess(self.demultiplexing_params, self.lane,
                                                                    self.input_files_queue, self.output_buffers_queue,
                                                                    name='batch-demultiplexer-%s-%s' % (self.lane, i))
            self.batch_demultiplexer_processes.append(batch_demultiplexer_process)
            batch_demultiplexer_process.daemon = False
        [p.start() for p in self.batch_demultiplexer_processes]
        # start sub-processes in charge of flushing output
        for i in range(self.demultiplexing_params.max_flush_processes):
            flush_process = FlushProcess(self.demultiplexing_params, self.lane,
                                         self.output_buffers_queue, self.output_locks,
                                         self.num_sequences_per_sample_dict,
                                         name='flush-%s-%s' % (self.lane, i))
            self.flush_processes.append(flush_process)
            flush_process.daemon = True
        [p.start() for p in self.flush_processes]
        # wait for all processes to end
        [p.join() for p in self.batch_demultiplexer_processes]
        logger.info('All batch demultiplexing processes ended')
        for i in range(self.demultiplexing_params.max_flush_processes):
            # signal sub-processes when they reach the end (a.k.a "poison pill")
            self.output_buffers_queue.put(None)
        [p.join() for p in self.flush_processes]
        logger.info('All flush processes ended')
        self.create_statistics_files()
        # TODO: check return code, fail on error

    def create_statistics_files(self):
        total_num_sequences_per_sample = dict(self.num_sequences_per_sample_dict)
        logger.debug('Total number of sequences per sample: %s' % (total_num_sequences_per_sample,))
        create_statistics(self.samples_details, total_num_sequences_per_sample,
                          self.demultiplexing_params.output_dir, self.lane)

    def get_total_num_demultiplexed_sequences(self):
        return sum(self.num_sequences_per_sample_dict.values())


class LaneDemultiplexerProcess(multiprocessing.Process):
    def __init__(self, demultiplexing_params, lane, **kwargs):
        super(LaneDemultiplexerProcess, self).__init__(**kwargs)
        self.demultiplexing_params = demultiplexing_params
        self.lane = lane

    def run(self):
        start_time = timeit.default_timer()
        logger.info('Lane demultiplexing started for lane %d' % self.lane)
        LaneDemultiplexer(self.demultiplexing_params, self.lane).demultiplex_one_lane()
        elapsed_time_in_milliseconds = (timeit.default_timer() - start_time) * 1000
        logger.debug('Demultiplexing lane %d ended (%.2f ms.)' % (self.lane, elapsed_time_in_milliseconds))


class FlowcellDemultiplexer(object):
    def __init__(self, demultiplexing_params, lanes):
        self.demultiplexing_params = demultiplexing_params
        self.lanes = lanes

    def demultiplex_flowcell(self):
        logger.info('Flow cell demultiplexing. Lanes: %s' % (self.lanes,))
        # demultiplex all lanes in parallel
        lane_demultiplexing_processes = []
        for lane in self.lanes:
            lane_demultiplexer_process = LaneDemultiplexerProcess(self.demultiplexing_params, lane,
                                                                  name='lane-demultiplexer-%s' % (lane,))
            lane_demultiplexing_processes.append(lane_demultiplexer_process)
            lane_demultiplexer_process.daemon = False
        # start all processes and wait for them to end
        [p.start() for p in lane_demultiplexing_processes]
        [p.join() for p in lane_demultiplexing_processes]
