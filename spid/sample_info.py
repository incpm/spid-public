SAMPLE_TYPE_TAGGED = 'tagged'
SAMPLE_TYPE_WITHOUT_TAGS = 'untagged'
SAMPLE_TYPE_UNDETERMINED_SAMPLE = 'undetermined-sample'
SAMPLE_TYPE_UNDETERMINED_SUBSAMPLE = 'undetermined-subsample'


class BarcodesDescription(object):
    @staticmethod
    def get_not_empty_reads_indexes(reads_description, cut_tags):
        """
        Find the reads that contain information other than indexes
        """
        not_empty_reads_num = []
        tagged_index = 0  # index of cut_tags list
        for i in range(len(reads_description)):  # i is index on all reads
            if reads_description[i] is None:
                continue
            if 'y' in reads_description[i]:
                not_empty_reads_num.append(i)
                if 'i' in reads_description[i]:
                    tagged_index += 1
            elif 'i' in reads_description[i] and not cut_tags[tagged_index]:
                not_empty_reads_num.append(i)
                tagged_index += 1
        return not_empty_reads_num

    @staticmethod
    def get_reads_with_tag(reads_description):
        """
        Find the number of tags, and the locations of the tags in the reads.
        Input:
        reads_description - a list in CASAVA format describing the base type in all the reads
        (y - regular, n - ignore, i - tag/index)
        Output:
        A tuple containing:
        The number of tags found (0, 1 or 2),
        1st tuple containing the read number containing the tag(s)
        2nd tuple containing the start location of the tag inside the read 
        """
        first_read_num = None
        second_read_num = None
        third_read_num = None
        first_init_tag_indices = None
        second_init_tag_indices = None
        third_init_tag_indices = None
        num_tags = 0
        for i in range(len(reads_description)):
            if reads_description[i] is None:
                continue
            if 'i' in reads_description[i]:
                num_tags += 1
                if first_read_num is None:
                    first_read_num = i
                    first_init_tag_indices = reads_description[i].find('i')
                elif second_read_num is None:
                    second_read_num = i
                    second_init_tag_indices = reads_description[i].find('i')
                else:
                    third_read_num = i
                    third_init_tag_indices = reads_description[i].find('i')
        return num_tags, \
            (first_read_num, second_read_num, third_read_num), \
            (first_init_tag_indices, second_init_tag_indices, third_init_tag_indices)

    @staticmethod
    def find_n_and_tag_indexes(reads_description):
        """
        Find the locations of the tags (i), sequence (y) and ignore (n) bases in the reads
        Input:
        Description of the reads, for example: iiiyyyynn
        Output:
        List of lists: first dimension is read number, second dimension is lists for each of 
        the n,i,y coordinates - initial, end, and identifier (i,n or y)
        """
        n_and_tag_indexes = []
        for read_description in reads_description:
            indexes = []
            if read_description:
                len_seq = len(read_description)
                init_block_n = init_block_i = None
                for idx in range(len_seq):
                    if read_description[idx] == 'n':
                        end_block_n = idx
                        if init_block_n is None:
                            init_block_n = idx
                        if idx == len_seq-1 or read_description[idx+1] != 'n':
                            indexes.append([init_block_n, end_block_n, 'n'])
                            init_block_n = None
                    if read_description[idx] == 'i':
                        end_block_i = idx
                        if init_block_i is None:
                            init_block_i = idx
                        if idx == len_seq-1 or read_description[idx+1] != 'i':
                            indexes.append([init_block_i, end_block_i, 'i'])
                            init_block_i = None
            n_and_tag_indexes.append(indexes)
        return n_and_tag_indexes


class SampleInfo(object):
    def __init__(self, lane, reads_num, project_name, master_sample_name, sample_name,
                 tags, tags_names, master_tag, cut_tags, max_mismatch,
                 max_offset, reversed_tagged_reads, reads_description, sample_type=SAMPLE_TYPE_TAGGED):
        self.lane = lane
        self.reads_num = reads_num
        self.project_name = project_name
        self.master_sample_name = master_sample_name
        self.sample_name = sample_name
        self.tags = tags
        self.tags_names = tags_names
        self.master_tag = master_tag
        self.cut_tags = cut_tags
        self.max_mismatch = max_mismatch
        self.max_offset = max_offset
        self.reversed_tagged_reads = reversed_tagged_reads
        self.reads_description = reads_description
        (self.tags_num, self.tagged_reads, self.init_tag_indices) = BarcodesDescription.get_reads_with_tag(self.reads_description)
        self.not_empty_reads_num = BarcodesDescription.get_not_empty_reads_indexes(self.reads_description, self.cut_tags)
        self.n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(self.reads_description)
        self.sample_type = sample_type

    @classmethod
    def _undetermined(cls, lane, reads_num, **kwargs):
        return cls(lane=lane,
                   reads_num=reads_num,
                   # project_name=project_name,
                   # master_sample_name=master_sample_name,
                   # sample_name=sample_name,
                   tags=[None] * 3,
                   tags_names=[None] * 3,
                   master_tag=None,
                   cut_tags=[False] * 3,
                   max_mismatch=[None] * 3,
                   max_offset=[None] * 3,
                   reversed_tagged_reads=[None] * 2,
                   reads_description=[None] * reads_num,
                   **kwargs)

    @classmethod
    def undetermined_sample(cls, lane, reads_num):
        return cls._undetermined(lane, reads_num, project_name=None,
                                 master_sample_name='Undetermined',
                                 sample_name='Undetermined', sample_type=SAMPLE_TYPE_UNDETERMINED_SAMPLE)

    @classmethod
    def undetermined_subsample(cls, lane, reads_num, sample_details, internal_undetermined):
        return cls._undetermined(lane, reads_num,
                                 project_name=sample_details.project_name,
                                 master_sample_name=sample_details.master_sample_name,
                                 sample_name=internal_undetermined,
                                 sample_type=SAMPLE_TYPE_UNDETERMINED_SUBSAMPLE)

    def get_other_rev_tagged_read_number(self, current_rev_tagged_read_number):
        if current_rev_tagged_read_number == self.reversed_tagged_reads[0]:
            return self.reversed_tagged_reads[1]
        else:
            return self.reversed_tagged_reads[0]

    def is_undetermined_sample(self):
        return self.sample_type == SAMPLE_TYPE_UNDETERMINED_SAMPLE

    def is_undetermined_subsample(self):
        return self.sample_type == SAMPLE_TYPE_UNDETERMINED_SUBSAMPLE

    def is_undetermined(self):
        return self.is_undetermined_sample() or self.is_undetermined_subsample()

    @property
    def non_empty_reads(self):
        if self.is_undetermined():
            return range(self.reads_num)
        else:
            return self.not_empty_reads_num
