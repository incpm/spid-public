import datetime
import logging
import os

from spid.settings import PROJECT_NAME


FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(processName)s - %(process)d - %(levelname)s - %(message)s')

root_logger = logging.getLogger(PROJECT_NAME)
root_logger.setLevel(logging.DEBUG)

def add_run_log_handlers(logs_dir, suffix=''):
    # create handler for outputting to console
    stderr_handler = logging.StreamHandler()
    stderr_handler.setLevel(logging.INFO)
    stderr_handler.setFormatter(FORMATTER)
    root_logger.addHandler(stderr_handler)

    # create application log file
    now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    log_filename = os.path.join(logs_dir, '%s.%s%s.log' %(PROJECT_NAME, now, suffix))
    file_handler = logging.FileHandler(log_filename)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(FORMATTER)
    root_logger.addHandler(file_handler)

    # create error log file
    error_log_filename = os.path.join(logs_dir, 'error.%s%s.log' %(now, suffix))
    error_file_handler = logging.FileHandler(error_log_filename)
    error_file_handler.setLevel(logging.WARNING)
    error_file_handler.setFormatter(FORMATTER)
    root_logger.addHandler(error_file_handler)
