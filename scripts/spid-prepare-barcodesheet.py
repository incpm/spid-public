#!/usr/bin/env python

import argparse
import logging
import logging.config
import os

from spid.barcodesheet import LaneBarcodesLocations, WriteBarcodeSheet, ReadBarcodeFile
from spid.reader import InputFilesReader
from spid.settings import DEFAULT_LOG_DIR, SAMPLE_SIZE, PROJECT_NAME

logger = logging.getLogger(PROJECT_NAME + '.' + __name__)

def get_options():
    parser = argparse.ArgumentParser(description='Prepare input file for SPID')
    parser.add_argument('--barcode-list', required=True, help='Path to input BarcodesList.csv file')
    parser.add_argument('--barcode-sheet-output', required=True, help='Path to output BarcodeSheet.csv file')
    parser.add_argument('--casava-folders-structure', action='store_true', default=False,
                        help='Search the fastq input files in sub-folders that created by CASAVA verstion 1 under '
                             'Unaligned directory.'
                             'Default: fastq files need to be under --casava-output-dir')
    parser.add_argument('--lanes', type=int, nargs='+', choices=range(1, 9), required=True,
                        help='Lanes numbers to demultiplex (space separateor list of the lanes number)')
    parser.add_argument('--samples-number-per-lane', type=int, nargs='+', required=False, default=0,
                        help='Number of samples per lane or 0 if the --barcode-list file contains all samples '
                             'which are sequenced in the lane (space separateor list of the samples number)')
    parser.add_argument('--casava-output-dir', required=True,
                       help='CASAVA output path (Unaligned directory)')
    parser.add_argument('--equal-mis-per-sample', action='store_true', default=False,
                        help='Don\'t enable different number of mismatches between the samples')
    parser.add_argument('--logging-config', type=str, required=False,
                        help='Load logging configuration from this file. Overrides default settings')
    return parser.parse_args()

def main():
    args = get_options()
    if args.logging_config:
        logger.info("Loading logging configuration from file: %s" % args.logging_config)
        assert os.path.isfile(args.logging_config), 'Could not find logging configuration file %s' % args.logging_config
        logging.config.fileConfig(args.logging_config)
    else:
        from spid.logging_conf import add_run_log_handlers
        add_run_log_handlers(logs_dir=DEFAULT_LOG_DIR)
    if args.samples_number_per_lane and len(args.lanes) != len(args.samples_number_per_lane):
        raise IOError('The lengths of the lists in \"--samples-number-per-lane\" and \"--lanes\" parameters are don\'t equals')
    elif not args.samples_number_per_lane:
        args.samples_number_per_lane = [0]*len(args.lanes)
    lanes_barcodes_locations = []
    for lane, samples_num in zip(args.lanes, args.samples_number_per_lane):
        fragments = InputFilesReader(lane, args.casava_output_dir, args.casava_folders_structure)._get_reads_sample(SAMPLE_SIZE)
        lane_barcodes_locations = LaneBarcodesLocations(lane, fragments, args.equal_mis_per_sample, samples_num)
        lane_barcodes_locations.read_sample_details(args.barcode_list)
        lane_barcodes_locations.update_samples()
        lanes_barcodes_locations.append(lane_barcodes_locations)
    WriteBarcodeSheet(lanes_barcodes_locations, args.barcode_sheet_output).write_file()


if __name__ == '__main__':
    main()
