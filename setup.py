from setuptools import setup, find_packages

with open('VERSION.txt', 'r') as version_file:
    version = version_file.read().strip()

requires = ['BeautifulSoup4', 'six', 'pyahocorasick==1.1.4']

setup(
    name='spid',
    version=version,
    author='Refael Kohen, Ophir Tal',
    author_email='refael.kohen@gmail.com, ophir.tal@weizmann.ac.il',
    packages=find_packages(),
    scripts=['scripts/spid-demultiplex.py', 'scripts/spid-prepare-barcodesheet.py'],
    url='https://bitbucket.org/incpm/spid-public',
    license='GPL',
    description='FASTQ Demultiplexing Application',
    long_description=open('README.txt').read(),
    tests_require=requires + ['nose'],
    install_requires=requires,
    test_suite='nose.collector',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX',
        'Programming Language :: Python'
    ],
)
