Installation
============

Install using pip
-----------------

The easiest way to install SPID is using `pip <https://pip.pypa.io/>`_::

    $ pip install spid

Get the code
------------

    $ git clone git@bitbucket.org:incpm/spid.git

Digging deeper
--------------
To read some more on SPID's implementation, you are welcome to read `this page <classes.html>`_
