=============
Why Use SPID?
=============

Features
========

SPID supports the following options:

Up to 3 tags for each sample
----------------------------

Each sequence can be composed of up to 3 reads, each read can contain a tag.

Tag position
------------

Tags can appear anywhere in the read - specifically, in the middle or at the 3' edge of the read.

It is possible to define for each tagged read the starting position of the tag in the read.

For example, the following sequence is composed of three reads and contains two tags:

**R1:** **AGT** ATATCGA, **R2:** ATCATCTGA, **R3:** TTAATA **TGC**

The first tag AGT starts at first base of R1, while the second tag TGC starts at 7th base of R3

Mismatches
----------

Allow any number of mismatches in the tags **for each tag separately**.

The user should make sure that the tags are sufficiently far apart.

Offsets
-------

Allow tag offset in tagged read at 5' and/or 3' edges for each tagged read separately.

Wildcards
---------

Allow wildcards in tags. For example, these tags are possible:
::

    5'- ATA...AAA - 3'

Meaning, a 9 bases sequence where the first 3 bases are ATA, the following 3 bases are wildcards, and the last 3 are AAA
::

    5' - ...AAA - 3'

Meaning, 3 first bases are wildcards and the last 3 bases are AAA.
::

    5' - .GCC..TAC - 3'

Translation: first base is a wildcard, then CGG, then two wildcards ,then TAC

Switch reads
------------

For each set of tags, the user can define two tagged reads as switch reads. These tagged reads can exchange the tags,
so that the tag that was supposed to be located in one read will be located in the other read and vice versa.

Example:

Given the two tags of sample X, which are searched in the starting point of R1 and R2 respectively:
**tag 1:** CTA,	**tag 2:** TAAC

If we allow switched reads, then the two following sets of sequences belong to sample X:

======================= ======================= ======================
Set                     R1                      R2
======================= ======================= ======================
Regular orientation     **CTA** TCTATAC         **TAAC** CCGGG
Switched orientation    **TAAC** CTATAC         **CTA** TTCGGG
======================= ======================= ======================

*Note:* Two switching tagged reads with one tag means that this tag can appear in one of the tagged reads.

Per-sample parameters
---------------------

All the parameters such as tags number, mismatches, offsets, tagged reads etc. are specific to each of the samples independently.
SPID supports the option to mix samples with different types and number of tags, different tagged reads,
different tag locations and other parameters.

Restrictions
============

There are some restrictions, though, so make sure you read this section too:

* Each read can include only one tag

* High memory usage, so make sure you read `this page <memory-usage.html>`_
