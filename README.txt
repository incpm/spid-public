SPID (Sequences with Polymorphic Indices Demultiplexer) is a software for automatic detection and demultiplexing a wide range of barcode types efficiently.

SPID also verifies the legality of the barcodes combination, recommends how many mismatches can be allowed, and looks for missing barcodes.

It allows mixing samples with different barcode types in the same lane or flowcell. The software is mainly designed for bioinformatics units which deal with a large number of samples as well as for researchers who want to know where their barcodes are located within the reads.

This project is currently using Python 2.7 or 3.5

All documentation is in the "docs" directory and online at **https://readthedocs.org/projects/spid/**