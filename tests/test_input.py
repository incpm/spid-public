from spid.sample_info import SampleInfo
from spid.tagstree import TagsTreeBuilder


class BuilderForTests(object):
    def __init__(self, set_num, mis1, mis2, mis3, init1, init2, init3, off1, off2, off3, cut1, cut2, cut3,
                 tagged1, tagged2, tagged3, rev1, rev2, master_tag_num, reads_num, read_lengths):
        (self.set_num, self.init1, self.init2, self.init3, self.tagged1, self.tagged2, self.tagged3,
         self.reads_num, self.read_lengths) = \
            (set_num, init1, init2, init3, tagged1, tagged2, tagged3,
             reads_num, read_lengths)
        self.default_params = dict(
            lane=None, project_name=None, reads_num=self.reads_num,
            tags_names=(None, None, None),
            master_sample_name=None,
            cut_tags=(cut1, cut2, cut3),
            master_tag=master_tag_num,
            max_mismatch=(mis1, mis2, mis3),
            max_offset=(off1, off2, off3),
            reversed_tagged_reads=(rev1, rev2)
        ).items()
        self.samples_details = self.get_sets()
        tags_tree_builder = TagsTreeBuilder(self.samples_details, self.reads_num, self.read_lengths)
        self.tags_trees = tags_tree_builder.build()

    sample_params = {
        0: {
            'sam1': (None, 'sam1', ('ATG.TT', None, None)),
            'sam2': (None, 'sam2', ('A.GCGT', None, None))
        },
        1: {
            # allowing 1 mismatch - at least 3 differences between every two tags
            # sam1 is reversed, tags_num to sam3 - you cannot use with reversed tag
            'sam1': (None, 'sam1', ('CCGGCT', 'TACGTC', None)),
            'sam2': (None, 'sam2', ('CCGGCT', 'CGTACT', None)),
            'sam3': (None, 'sam3', ('TACGTC', 'CCGGCT', None)),
            'sam4': (None, 'sam4', ('ACACAG', None, None))
        },
        14: {
            'sam1': (None, 'sam1', ('...TAT', None, None)),
            'sam2': (None, 'sam2', ('..ATCT', None, None)),
            'sam3': (None, 'sam3', ('GCA', None, None))
        },
        13: {
            'sam1': (None, 'sam1', ('CC.TG', None, None)),
            'sam2': (None, 'sam2', ('CC..T', None, None)),
            'sam3': (None, 'sam3', ('C.GTC', None, None))
        },
        2: {
            # allowing 1 mismatch - at least 3 differences between every two tags
            # sam1 tag1 is equal to sam2 tag1, and sam2 tag2 is equal to sam3 tag2
            'sam1': (None, 'sam1', ('CCGGCT', 'TACGTC', None)),
            'sam2': (None, 'sam2', ('CCGGCT', 'CGTACT', None)),
            'sam3': (None, 'sam3', ('TGACTG', 'CGTACT', None)),
            'sam4': (None, 'sam4', ('GCTAGC', 'GACTGA', None)),
            'sam5': (None, 'sam5', ('CAGTCA', 'TCGATG', None)),
            'sam7': (None, 'sam7', ('ACACAG', None, None))
        },
        3: {
            # three tags: first and third are reversed
            # allowing 1 mismatch - at least 3 differences between every two tags
            # in all the tagged reads - you can use with reversed tag
            'sam1': (None, 'sam1', ('CCGGCT', 'TTCGG', 'TACGTC')),
            'sam2': (None, 'sam2', ('CCGGCT', 'TTCGG', 'CGTACT')),
            'sam3': (None, 'sam3', ('TGACTG', 'TTCGG', 'CGTACT')),
            'sam4': (None, 'sam4', ('GCTAGC', 'CCAAG', 'GACTGA')),
            'sam5': (None, 'sam5', ('CAGTCA', 'CCAAG', 'TCGATG')),
            'sam7': (None, 'sam7', ('GCACAG', 'CCAAG', None))
        },
        4: {
            # as previous set, this time with master tag
            'master1_sam1': ('master1', 'master1_sam1', ('CCGGCT', 'TTCGG', 'TACGTC')),
            'master1_sam2': ('master1', 'master1_sam2', ('CCGGCT', 'TTCGG', 'CGTACT')),
            'master1_sam3': ('master1', 'master1_sam3', ('TGACTG', 'TTCGG', 'CGTACT')),
            'master2_sam4': ('master2', 'master2_sam4', ('GCTAGC', 'CCAAG', 'GACTGA')),
            'master2_sam5': ('master2', 'master2_sam5', ('CAGTCA', 'CCAAG', 'TCGATG')),
            'master2_sam7': ('master2', 'master2_sam7', ('GCACAG', 'CCAAG', None))
        },
        5: {
            # three UMI tags: first and third are reversed. allowing 1 mismatch in first and third tags
            'master1_good': ('master1', 'master1_good', ('CTAGA......ATTGATT', 'CATGCT', 'AATTCAATT')),
            'master1_uncut_both': ('master1', 'master1_uncut_both', ('GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATT', 'CATGCT', 'ATCAACAATACCAATAAACTTAACATCGAATTCAATT')),
            'master1_uncut_first': ('master1', 'master1_uncut_first', ('GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATT', 'CATGCT', 'ATTCAATT')),
            'master1_uncut_second': ('master1', 'master1_uncut_second', ('CTAGA......ATTGATT', 'CATGCT', 'ATCAACAATACCAATAAACTTAACATCGAATTCAATT')),
            'master1_self_closed': ('master1', 'master1_self_closed', ('ATCAACAATACCAATAAACTTAACATCGAATTCAATATC', 'CATGCT', None)),
            'master1_closed_plasmid': ('master1', 'master1_closed_plasmid', ('CTAGA......ATTGATATT', 'CATGCT', 'AATTCAATATCAAT')),
            'master2_good': ('master2', 'master2_good', ('CTAGA......ATTGATT', 'TACTTA', 'AATTCAATT')),
            'master2_uncut_both': ('master2', 'master2_uncut_both', ('GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATT', 'TACTTA', 'ATCAACAATACCAATAAACTTAACATCGAATTCAATT')),
            'master2_uncut_first': ('master2', 'master2_uncut_first', ('GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATT', 'TACTTA', 'ATTCAATT')),
            'master2_uncut_second': ('master2', 'master2_uncut_second', ('CTAGA......ATTGATT', 'TACTTA', 'ATCAACAATACCAATAAACTTAACATCGAATTCAATT')),
            'master2_self_closed': ('master2', 'master2_self_closed', ('ATCAACAATACCAATAAACTTAACATCGAATTCAATATC', 'TACTTA', None)),
            'master2_closed_plasmid': ('master2', 'master2_closed_plasmid', ('CTAGA......ATTGATATT', 'TACTTA', 'AATTCAATATCAAT'))
        },
        6: {
            # no index
            'sam1': (None, 'sam1', (None, None, None))
        },
        # sets with collisions between tags
        7: {
            'sam1': (None, 'sam1', ('CCGGCT', None, None)),
            'sam2': (None, 'sam2', ('CCCGGCT', None, None))
        },
        8: {
            'sam1': (None, 'sam1', ('CCGGCT', 'TACGTC', None)),
            'sam2': (None, 'sam2', ('CCCGGCT', 'GGGGGG', None)),
            'sam3': (None, 'sam3', ('CCGGCT', 'GGGGGG', None))
        },
        9: {
            'sam1': (None, 'sam1', ('AGCTAG', None, None)),
            'sam2': (None, 'sam2', ('AGCTAG', None, None))
        },
    }

    def get_sets(self):
        assert self.set_num in self.sample_params
        return dict([(sample_name, self.create_sample(*args))
                     for (sample_name, args) in self.sample_params[self.set_num].items()])

    def create_sample(self, master_sample_name, sample_name, tags):
        sample_params = dict(self.default_params)
        sample_params.update(dict([('master_sample_name', master_sample_name), ('sample_name', sample_name)] +
                                  self.reads_description(tags)))
        return SampleInfo(**sample_params)

    def reads_description(self, tags):
        reads_description = []
        for read_num in range(self.reads_num):
            reads_description.append(list('y'*self.read_lengths[read_num]))
        for tagged_read_num, init, tag in zip([self.tagged1, self.tagged2, self.tagged3],
                                              [self.init1, self.init2, self.init3], tags):
            if tag is not None:
                for index in range(init, init+len(tag)):
                    reads_description[tagged_read_num][index] = 'i'
        for read_num in range(self.reads_num):
            reads_description[read_num] = ''.join(reads_description[read_num])
        return [('tags', tags), ('reads_description', reads_description)]
