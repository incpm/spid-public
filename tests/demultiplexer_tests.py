import unittest

# noinspection PyPackageRequirements
from nose.plugins.attrib import attr

from spid.demultiplexer import FragmentDemultiplexer
from spid.sample_info import BarcodesDescription
# noinspection PyPep8Naming
from spid.tagstree import Tag as T

from tests.test_utils import barcode_sheet_filename
from tests.test_input import BuilderForTests


@attr(test_type='integration', duration='fast')
class FragmentDemultiplexerTest(unittest.TestCase):
    def setUp(self):
        self.barcode_sheet = barcode_sheet_filename('BarcodeSheet_singleRead_51_7.txt')
        self.lane = 1
        self.reads_num = 2
        self.read_lengths = (51, 7)

    def test_repair_n_tag_indexes(self):
        demultiplexer = FragmentDemultiplexer(None, None, None, read_lengths=self.read_lengths)
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i']])  # insert 'i' sequence
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'n']])  # insert 'n' sequence
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'n'], [4, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'y']])  # insert 'y' sequence
        self.assertEquals(n_and_tag_indexes, [[0, 0, 'n'], [4, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [6, 7, 'i']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 7, 'i'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [8, 10, 'i']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'n'], [8, 10, 'i'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [8, 13, 'i']])
        self.assertEquals(n_and_tag_indexes, [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'n'], [8, 13, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [6, 6, 'i']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'i'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [6, 6, 'y']])
        self.assertEquals(n_and_tag_indexes, [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [9, 10, 'n'], [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 15, 'n']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [10, 12, 'i']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'n'], [9, 9, 'n'], [10, 12, 'i'],
                           [13, 15, 'n']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 15, 'n']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[1, 3, 'i'], [10, 12, 'y']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 0, 'n'], [1, 3, 'i'], [4, 5, 'i'], [6, 6, 'n'], [9, 9, 'n'], [13, 15, 'n']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[3, 4, 'i']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 1, 'n'], [2, 2, 'i'], [3, 4, 'i'], [5, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'],
                           [11, 12, 'i']])
        n_and_tag_indexes = [[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']]
        demultiplexer._repair_n_tag_indexes(n_and_tag_indexes, [[3, 4, 'y']])
        self.assertEquals(n_and_tag_indexes,
                          [[0, 1, 'n'], [2, 2, 'i'], [5, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']])

    def test_remove_tag_and_n(self):
        demultiplexer = FragmentDemultiplexer(None, None, None, read_lengths=self.read_lengths)

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiiiyyyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 0, 0, 6)  # cut the tag and 'n'
        self.assertEquals(lines[1], 'NNNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiyyyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 0, 0, 0, 6)  # not cut the tag (only the 'n')
        self.assertEquals(lines[1], 'TTTNNNTNNNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['yyiiiiiiiyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 2, 2, 8)  # the tag in middle of sequence
        self.assertEquals(lines[1], 'TTNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiiiyyyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 0, 2,
                                       8)  # the expected tag in 0 index, with offset forward 3' edge
        self.assertEquals(lines[1], 'NNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiiiyyyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 0, 0, 2,
                                       8)  # the expected tag in 0 index, with offset forward 3' edge - not cut
        self.assertEquals(lines[1], 'TTTNNNTNNNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['yyiiiiiiiyyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 2, 4,
                                       10)  # the expected tag in middle, with offset forward 3' edge
        self.assertEquals(lines[1], 'TTTNN')

        # Note:  the second 'n' will not removed although the offset
        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['yniiiiiiinyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 0, 2, 4,
                                       10)  # the expected tag in middle, with offset forward 3' edge - not cut
        self.assertEquals(lines[1], 'TTNNNTNNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['yyyiiiiiiiyyn', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 3, 2,
                                       8)  # the expected tag in middle, with offset forward 5' edge
        self.assertEquals(lines[1], 'TTNNN')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiiiiiiiin', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], 1, 0, 0, 11)
        self.assertEquals(lines[1], '')

        lines = ['@fastq read:', 'TTTNNNTNNNNNN', '+', '1Q@#^&**)_(&']
        n_and_tag_indexes = BarcodesDescription.find_n_and_tag_indexes(['iiiiiiiiiiiin', 'yy', 'yy'])
        demultiplexer.remove_tag_and_n(0, lines, n_and_tag_indexes[0], None, None, None, None)  # remove only 'n'
        self.assertEquals(lines[1], 'TTTNNNTNNNNN')

    def test_check_match_results(self):
        read_lengths = (51, 6, 51)
        reads_num = 3
        data_builder = BuilderForTests(set_num=0,
                                       mis1=0, mis2=None, mis3=None,
                                       init1=0, init2=None, init3=None,
                                       off1='0l1r', off2=None, off3=None,
                                       cut1=True, cut2=None, cut3=None,
                                       tagged1=0, tagged2=None, tagged3=None,
                                       rev1=None, rev2=None, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=True)

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ATGCTTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 0, 0, None, 'ATG.TT')]))
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ATGCGTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 6, 0, 0, None, 'A.GCGT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACGCGTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 6, 0, 0, None, 'A.GCGT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACGCTTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ATGGGTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        data_builder = BuilderForTests(set_num=13,
                                       mis1=0, mis2=None, mis3=None,
                                       init1=0, init2=None, init3=None,
                                       off1='0l1r', off2=None, off3=None,
                                       cut1=True, cut2=None, cut3=None,
                                       tagged1=0, tagged2=None, tagged3=None,
                                       rev1=None, rev2=None, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=True)

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGTGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 5, 0, 0, None, 'CC.TG')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 5, 0, 0, None, 'CC..T')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGTTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 5, 0, 0, None, 'CC..T')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam3', [
            T('sam3', 0, 5, 0, 0, None, 'C.GTC')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CTGTTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CTGTGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCTCCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        data_builder = BuilderForTests(set_num=14,
                                       mis1=0, mis2=None, mis3=None,
                                       init1=0, init2=None, init3=None,
                                       off1='0l1r', off2=None, off3=None,
                                       cut1=True, cut2=None, cut3=None,
                                       tagged1=0, tagged2=None, tagged3=None,
                                       rev1=None, rev2=None, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=True)

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('GCATCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 6, 0, 0, None, '..ATCT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('GCATATNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 0, 0, None, '...TAT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CAATATNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 0, 0, None, '...TAT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CAATCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 6, 0, 0, None, '..ATCT')]))

        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCCTCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        # not reversed tags:
        data_builder = BuilderForTests(set_num=1,
                                       mis1=0, mis2=0, mis3=None,
                                       init1=0, init2=0, init3=None,
                                       off1='0l1r', off2='0l0r', off3=None,
                                       cut1=True, cut2=True, cut3=None,
                                       tagged1=0, tagged2=2, tagged3=None,
                                       rev1=None, rev2=None, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)
        # two tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'TACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 0, 0, None, 'CCGGCT'),
            T('sam1', 1, 6, 0, 2, None, 'TACGTC')]))
        # two tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam3', [
            T('sam3', 0, 6, 0, 0, None, 'TACGTC'),
            T('sam3', 1, 6, 0, 2, None, 'CCGGCT')]))
        # no tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # no pair of tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CGTACTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # one not uniq tag found - expected two (expected one is illegal)
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # one tag found - expected one
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam4', [
            T('sam4', 0, 6, 0, 0, None, 'ACACAG')]))
        # first uniq tag found - expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # second uniq tag found - expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'TACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        # two tags - reversed first with third:
        data_builder = BuilderForTests(set_num=2,
                                       mis1=1, mis2=1, mis3=None,
                                       init1=0, init2=0, init3=None,
                                       off1='0l1r', off2='0l0r', off3=None,
                                       cut1=True, cut2=True, cut3=None,
                                       tagged1=0, tagged2=2, tagged3=None,
                                       rev1=0, rev2=2, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)

        # two tags found with one mismatch in each of tags
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 0, 0, None, 'TCGGCT'),
            T('sam1', 1, 6, 0, 2, None, 'CACGTC')]))
        # two tags found - reversed
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 1, 6, 0, 0, None, 'TACGTC'),
            T('sam1', 0, 6, 0, 2, None, 'CCGGCT')]))
        # two tags of sam2 found - one common to sam1 and the second to sam3
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CGTACTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam2', [
            T('sam2', 0, 6, 0, 0, None, 'CCGGCT'),
            T('sam2', 1, 6, 0, 2, None, 'CGTACT')]))
        # two tags of sam2 found - in read 1 and read 2
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'CGTACT', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # two tags of sam2 found - the second in read 2 and the first in read 1
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CGTACTNNNNNNNNNNNNNNNNNNNNNN', 'CCGGCT', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # two tags found - the same sample but the same tag
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'TACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # no tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # no pair of tags found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'GACTGNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # one not uniq tag found - expected two (expected one is illegal)
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # one tag found - expected one
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 0, None, 'ACACAG')]))
        # one tag found - expected one
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'ACACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 2, None, 'ACACAG')]))
        # one tag  of sam7 apear twice
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'ACACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 0, None, 'ACACAG')]))
        # one tag  of sam1 apear twice
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'CCGGCTNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # second uniq tag found in first sequence- expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TACGTCNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # second uniq tag found in second sequence- expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'TACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # first uniq tag found in first sequence - expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('TGACTGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # first uniq tag found in second sequence - expected two
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'TGACTGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        # two tags - reversed first with second:
        data_builder = BuilderForTests(set_num=2,
                                       mis1=1, mis2=1, mis3=None,
                                       init1=0, init2=0, init3=None,
                                       off1='0l1r', off2='0l0r', off3=None,
                                       cut1=True, cut2=True, cut3=None,
                                       tagged1=0, tagged2=2, tagged3=None,
                                       rev1=0, rev2=1, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)

        # sam7 tag (reversed tag) found in read 3
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'ACACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # sam7 tag (reversed tag) found in read 3
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'ACACAG', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 1, None, 'ACACAG')]))
        # sam7 tag (reversed tag) found in read 1 AND read 2
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'ACACAG', 'NNNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 0, None, 'ACACAG')]))
        # sam7 tag (reversed tag) found in read 1 AND read 3
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'ACACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 0, None, 'ACACAG')]))
        # sam7 tag (reversed tag) found in read 1 AND read2 AND read 3
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('ACACAGNNNNNNNNNNNNNNNNNNNNNN', 'ACACAG', 'ACACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 0, 0, None, 'ACACAG')]))

        # three tags - first and third are reversed:
        data_builder = BuilderForTests(set_num=3,
                                       mis1=1, mis2=1, mis3=1,
                                       init1=1, init2=0, init3=2,
                                       off1='0l1r', off2='1l0r', off3='0l2r',
                                       cut1=True, cut2=True, cut3=True,
                                       tagged1=0, tagged2=1, tagged3=2,
                                       rev1=0, rev2=2, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)

        # three tags found with one mismatch in each of tags
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NNCACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 1, 0, None, '.TCGGCT'),
            T('sam1', 1, 5, 0, 1, None, 'TTCGG'),
            T('sam1', 2, 6, 2, 2, None, '..CACGTC')]))
        # three tags found with one mismatch in each of tags - reversed
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNCACGTCNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NTCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 2, 6, 2, 0, None, '..CACGTC'),
            T('sam1', 1, 5, 0, 1, None, 'TTCGG'),
            T('sam1', 0, 6, 1, 2, None, '.TCGGCT')]))
        # two tags found: reversed tag (on first read) and regular tag 
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NGCACAGNNNNNNNNNNNNNNNNNNNNNN', 'CCAAG', 'NNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 1, 0, None, '.GCACAG'),
            T('sam7', 1, 5, 0, 1, None, 'CCAAG')]))
        # two tags found: reversed tag (on third read) and regular tag 
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNN', 'CCAAG', 'NGCACAGNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 1, 5, 0, 1, None, 'CCAAG'),
            T('sam7', 0, 6, 1, 2, None, '.GCACAG')]))
        # The first of the reversed tag of sam1 found twice: in 1 and 3 reads.
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NTCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # The second of the reversed tag of sam1 found twice: in 1 and 3 reads.  
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NCACGTCNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NNCACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # The reversed tag of sam7 found twice - tag 2 not found  
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NGCACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNNN', 'NGCACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # The reversed tag of sam7 found twice - tag 2 is found  
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NGCACAGNNNNNNNNNNNNNNNNNNNNNN', 'CCAAG', 'NGCACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 1, 0, None, '.GCACAG'),
            T('sam7', 1, 5, 0, 1, None, 'CCAAG')]))
        # re-run previous tests with offset
        # three tags found with one mismatch in each of tags
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNNNGACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 0, 6, 2, 0, None, '..TCGGCT'),
            T('sam1', 1, 5, -1, 1, None, 'TCGG'),
            T('sam1', 2, 6, 4, 2, None, '....GACGTC')]))
        # three tags found with one  mismatch in each of tags - reversed
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNCACGTCNNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNTCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam1', [
            T('sam1', 2, 6, 4, 0, None, '....CACGTC'),
            T('sam1', 1, 5, -1, 1, None, 'TCGG'),
            T('sam1', 0, 6, 2, 2, None, '..TCGGCT')]))
        # two tags found: reversed tag (on first read) and regular tag
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNGCACAGNNNNNNNNNNNNNNNNNNNNNN', 'CAAG', 'NNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 0, 6, 2, 0, None, '..GCACAG'),
            T('sam7', 1, 5, -1, 1, None, 'CAAG')]))
        # two tags found: reversed tag (on third read) and regular tag
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNN', 'CAAG', 'NNGCACAGNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('sam7', [
            T('sam7', 1, 5, -1, 1, None, 'CAAG'),
            T('sam7', 0, 6, 2, 2, None, '..GCACAG')]))
        # three tags - first and third are reversed with master tag:
        data_builder = BuilderForTests(set_num=4,
                                       mis1=1, mis2=1, mis3=1,
                                       init1=1, init2=0, init3=2,
                                       off1='0l1r', off2='1l0r', off3='0l2r',
                                       cut1=True, cut2=True, cut3=True,
                                       tagged1=0, tagged2=1, tagged3=2,
                                       rev1=0, rev2=2, master_tag_num=1,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNNNGACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_sam1', [
            T('master1_sam1', 0, 6, 2, 0, 'master1', '..TCGGCT'),
            T('master1_sam1', 1, 5, -1, 1, 'master1', 'TCGG'),
            T('master1_sam1', 2, 6, 4, 2, 'master1', '....GACGTC')]))
        # two tags found: reversed tag (on third read) and regular tag 
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNN', 'CAAG', 'NNGCACAGNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master2_sam7', [
            T('master2_sam7', 1, 5, -1, 1, 'master2', 'CAAG'),
            T('master2_sam7', 0, 6, 2, 2, 'master2', '..GCACAG')]))
        # first tag not found, but master tag (1) and third tag found .
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNNNGACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined_master1', None))
        # third tag not found, but master tag (1) and first tag found .
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined_master1', None))
        # master tag (1) not found, first and third found .
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNN', 'NNNNGACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
        # only master tag (1) found .
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNN', 'TCGGA', 'NNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined_master1', None))
        # the first of the reversed tag of sam1 found twice: in 1 and 3 reads.
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NTCGGCTNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NTCGGCTNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined_master1', None))
        # the second of the reversed tag of sam1 found twice: in 1 and 3 reads.
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNCACGTCNNNNNNNNNNNNNNNNNNNNNN', 'TTCGGA', 'NNCACGTCNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined_master1', None))
        # the reversed tag of sam7 found twice - tag 2 not found
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NGCACAGNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNNN', 'NGCACAGNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))

        # three UMI tags - first and third are reversed with master tag:
        # testing the long and short tags, when the short is part of the long
        data_builder = BuilderForTests(set_num=5,
                                       mis1=1, mis2=0, mis3=1,
                                       init1=0, init2=0, init3=0,
                                       off1='0l3r', off2='0l0r', off3='0l3r',
                                       cut1=True, cut2=True, cut3=True,
                                       tagged1=0, tagged2=1, tagged3=2,
                                       rev1=0, rev2=2, master_tag_num=1,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=True)

        # good sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNCTAGA......ATTCATTNNNNNNNNNNNNNNN', 'CATGCTNNN', 'NNGATTCAATTNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_good', [
            T('master1_good', 0, 18, 3, 0, 'master1', '...CTAGA......ATTCATT'),
            T('master1_good', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_good', 2, 9, 2, 2, 'master1', '..GATTCAATT')]))
        # uncut_both sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences((
            'NGGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATTNNNN', 'CATGCTNNN',
            'NNNGTCAACAATACCAATAAACTTAACATCGAATTCAATTNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_uncut_both', [
            T('master1_uncut_both', 0, 44, 1, 0, 'master1', '.GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......ATTGATT'),
            T('master1_uncut_both', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_uncut_both', 2, 37, 3, 2, 'master1', '...GTCAACAATACCAATAAACTTAACATCGAATTCAATT')]))
        # uncut_first sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNGGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......AGTGATTNNNNNN', 'CATGCTNNN', 'NNATTCAATTNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_uncut_first', [
            T('master1_uncut_first', 0, 44, 2, 0, 'master1', '..GGAGTTTTATTGTGGTGGTGGTTGTTCTAGA......AGTGATT'),
            T('master1_uncut_first', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_uncut_first', 2, 8, 2, 2, 'master1', '..ATTCAATT')]))
        # uncut_second sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNCTAGA......ATTGATTNNNNNNNN', 'CATGCTNNNN', 'NATCAACAATACCAATAAACTTAACATCGAATTCAATTNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_uncut_second', [
            T('master1_uncut_second', 0, 18, 3, 0, 'master1', '...CTAGA......ATTGATT'),
            T('master1_uncut_second', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_uncut_second', 2, 37, 1, 2, 'master1', '.ATCAACAATACCAATAAACTTAACATCGAATTCAATT')]))
        # closed_plasmid sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('CTAGA......ATTGATATTNNNNNN', 'CATGCTNNNNN', 'NNNAATTCAATATCAATNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_closed_plasmid', [
            T('master1_closed_plasmid', 0, 20, 0, 0, 'master1', 'CTAGA......ATTGATATT'),
            T('master1_closed_plasmid', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_closed_plasmid', 2, 14, 3, 2, 'master1', '...AATTCAATATCAAT')]))
        # seld_closed sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNATCAACAATACCAATAAACTTAACATCGAATTCAATATCNNNNNNN', 'CATGCTNNNNNN', 'NNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_self_closed', [
            T('master1_self_closed', 0, 39, 2, 0, 'master1', '..ATCAACAATACCAATAAACTTAACATCGAATTCAATATC'),
            T('master1_self_closed', 1, 6, 0, 1, 'master1', 'CATGCT')]))
        # seld_closed sample, with mismatch and offset, reversed
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNN', 'CATGCTNNNNNN', 'NNATCAACAATACCAATAAACTTAACATCGAATTCAATATCNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_self_closed', [
            T('master1_self_closed', 1, 6, 0, 1, 'master1', 'CATGCT'),
            T('master1_self_closed', 0, 39, 2, 2, 'master1', '..ATCAACAATACCAATAAACTTAACATCGAATTCAATATC')]))
        # seld_closed sample, with mismatch and offset, R reversed appears in twice
        raw_tags_details = demultiplexer.find_tags_in_sequences((
            'NNATCAACAATACCAATAAACTTAACATCGAATTCAATATCNNNNNNN', 'CATGCTNNNNNN',
            'NNATCAACAATACCAATAAACTTAACATCGAATTCAATATCNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('master1_self_closed', [
            T('master1_self_closed', 0, 39, 2, 0, 'master1', '..ATCAACAATACCAATAAACTTAACATCGAATTCAATATC'),
            T('master1_self_closed', 1, 6, 0, 1, 'master1', 'CATGCT')]))

        # no index in lane
        data_builder = BuilderForTests(set_num=6,
                                       mis1=None, mis2=None, mis3=None,
                                       init1=None, init2=None, init3=None,
                                       off1=None, off2=None, off3=None,
                                       cut1=None, cut2=None, cut3=None,
                                       tagged1=None, tagged2=None, tagged3=None,
                                       rev1=None, rev2=None, master_tag_num=None,
                                       reads_num=reads_num, read_lengths=read_lengths)
        demultiplexer = FragmentDemultiplexer(data_builder.samples_details, data_builder.tags_trees,
                                              reads_num=reads_num, read_lengths=read_lengths, last_appear=False)
        # good sample, with mismatch and offset
        raw_tags_details = demultiplexer.find_tags_in_sequences(
            ('NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'NNNNNNNN', 'NNNNNNNNNNNNNNNNNNNNNNNNNNN'))
        self.assertEquals(demultiplexer.check_match_results(raw_tags_details), ('Undetermined', None))
