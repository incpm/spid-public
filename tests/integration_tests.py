import logging
import os
import subprocess
import tempfile
import timeit
import unittest 

from nose.plugins.attrib import attr
from nose.tools.nontrivial import nottest, istest

from spid.demultiplexer import DemultiplexingParameters, FlowcellDemultiplexer
from spid.settings import PROJECT_NAME, OUTPUT_FILE_FORMAT_UNCOMPRESSED

from tests.test_settings import RUN_BASE_DIR

BASE_OUTPUT_DIRECTORY = '/users/biodev/test-output/'

logger = logging.getLogger(PROJECT_NAME + '.' + __name__)


class Paths(object):
    def __init__(self, runs_base_dir, flw_dir, lanes):
        self.runs_base_dir = runs_base_dir
        self.flw_dir = flw_dir
        self.lanes = lanes
        self.input_dir = os.path.join(self.runs_base_dir, self.flw_dir, 'Data/Intensities/BaseCalls')
        lanes = '_'.join([str(l) for l in self.lanes])
        self.casava_demultiplexed_expected = os.path.join(self.runs_base_dir, 'casava_demultiplexed_expected',
                                                          self.flw_dir)
        tmp_output_dir = os.path.join(tempfile.mkdtemp(prefix='spid_output.', dir=BASE_OUTPUT_DIRECTORY), self.flw_dir)
        self.spid_output_dir = self.prepare_output_dir(tmp_output_dir, lanes)
        bcl2fastq_dir = os.path.join(self.runs_base_dir, 'casava_bcl2fastq_results', self.flw_dir)
        self.casava_output_dir = os.path.join(bcl2fastq_dir, 'Unaligned_auto_casava_L_' + lanes)
        self.barcode_sheet_filename = os.path.join(self.casava_demultiplexed_expected, 'BarcodeSheet.csv')

    @staticmethod
    def prepare_output_dir(base_output_dir, lanes):
        if os.path.exists(base_output_dir):
            if not os.path.isdir(base_output_dir):
                raise IOError('Output directory exists but is not a directory: %s' % base_output_dir)
        else:
            os.makedirs(base_output_dir)
        spid_output_dir = os.path.join(base_output_dir, 'Unaligned_auto_spid_L_' + lanes)
        return spid_output_dir


@nottest
class SpidTest(unittest.TestCase):
    def setUp(self):
        self.paths = Paths(RUN_BASE_DIR, self.flw_dir, self.lanes)

    def wait_and_check_status_process(self, process, software):
        status = process.wait()
        self.assertEquals(status, 0, msg='%s run failed - return code is %s' % (software, status))

    def compare_casava_spid(self):
        sort_cmd = 'for f in `find %s -name \"*.fastq\"`; do grep \'%s\' $f | sed -e \'s/ 1:N:0:.*//\' ' \
                   '| uniq | sort > $f.sorted; done'\
                   % (self.paths.spid_output_dir, self.machine)
        sort_process = subprocess.Popen(sort_cmd, shell=True)
        status = sort_process.wait()
        self.assertEquals(status, 0, msg='sorting results failed - return code is %s' % (status,))
        casava_files_cmd = 'find %s -name \"*.fastq.sorted\" -not -path \"*R2*\" -not -path \"*R3*\"'\
                           % self.paths.casava_demultiplexed_expected
        spid_files_cmd = 'find %s -name \"*.fastq.sorted\" -not -path \"*R2*\" -not -path \"*R3*\"'\
                         % self.paths.spid_output_dir
        spid_files = subprocess.Popen(spid_files_cmd, stdout=subprocess.PIPE,
                                      stderr=None, shell=True).communicate()[0].split()
        casava_files = subprocess.Popen(casava_files_cmd, stdout=subprocess.PIPE,
                                        stderr=None, shell=True).communicate()[0].split()
        spid_files_sorted = sorted(spid_files)
        casava_files_sorted = sorted(casava_files)
        self.assertEquals(len(casava_files_sorted), len(spid_files_sorted),
                          msg='Count of FASTQ files in SPID and CASAVA output is not equal: %s vs. %s.\n'
                              'sorted CASAVA files: %s\n'
                              'sorted SPID files: %s'
                              % (len(casava_files_sorted), len(spid_files_sorted),
                                 casava_files_sorted, spid_files_sorted))
        for cf, sf in zip(casava_files_sorted, spid_files_sorted):
            diff_cmd = 'diff %s %s' % (cf, sf)
            diff = subprocess.Popen(diff_cmd, stdout=subprocess.PIPE, stderr=None, shell=True).communicate()[0]
            if diff:
                self.fail('Files %s and %s differ' % (cf, sf))
            else:
                logger.info('Files %s and %s are equal' % (cf, sf))

    def run_spid(self):
        start_time_spid = timeit.default_timer()
        demultiplexing_params = DemultiplexingParameters(barcode_sheet=self.paths.barcode_sheet_filename,
                                                         output_dir=self.paths.spid_output_dir,
                                                         reads_num=len(self.read_lengths),
                                                         read_lengths=self.read_lengths,
                                                         casava_output_dir=self.paths.casava_output_dir,
                                                         output_buffer_size=1000,
                                                         fastq_cluster_count=1000000000,
                                                         max_read_processes=4,
                                                         output_format=OUTPUT_FILE_FORMAT_UNCOMPRESSED,
                                                         max_flush_processes=4,
                                                         force=False,
                                                         find_last_appearance=self.find_last_appearance)
        logger.info('Running SPID on lanes %s with the following parameters: %s'
                    % (self.lanes, demultiplexing_params))
        FlowcellDemultiplexer(demultiplexing_params, self.lanes).demultiplex_flowcell()
        elapsed_spid = str(timeit.default_timer() - start_time_spid)
        logger.info('Total SPID run time was: %s' % elapsed_spid)

    def test_spid_run(self):
        start_time_total = timeit.default_timer()
        logger.info('CASAVA output directory: %s' % self.paths.casava_output_dir)
        logger.info('SPID output directory: %s' % self.paths.spid_output_dir)
        self.run_spid()
        self.compare_casava_spid()
        elapsed_total = str(timeit.default_timer() - start_time_total)
        logger.info('Total test run was: %s' % elapsed_total)


@istest
@attr(test_type='integration', duration='fast')
class SpeedySpidTest1(SpidTest):
    machine = 'HISEQ'
    flw_dir = '131027_D00257_0111_SMRNA3PRIM'
    read_lengths = [101, 7]
    lanes = [1]
    find_last_appearance = False


@istest
@attr(test_type='integration', duration='fast')
class SpeedySpidTest2(SpidTest):
    machine = 'HWI-ST808'
    flw_dir = '302020_SN808_0155_UMIBARCODE'
    read_lengths = [101, 7, 101]
    lanes = [3]
    find_last_appearance = True


@istest
@attr(test_type='integration', duration='fast')
class SpeedySpidTest3(SpidTest):
    machine = 'HWI-ST808'
    flw_dir = '130421_SN808_0128_INDEX5PRIM'
    read_lengths = [101, 7, 101]
    lanes = [2, 3]
    find_last_appearance = True


@istest
@attr(test_type='integration', duration='slow')
class LongSpidTest1(SpidTest):
    machine = 'HWI-ST808'
    flw_dir = '201126_SN808_0155_CD27N9ACXX'
    read_lengths = [101, 7, 101]
    lanes = [1, 2]
    find_last_appearance = True


@istest
@attr(test_type='integration', duration='slow')
class LongSpidTest2(SpidTest):
    machine = 'HISEQ'
    flw_dir = '130814_D00257_0107_AH0YCUADXX'
    read_lengths = [51, 7, 51]
    lanes = [1]
    find_last_appearance = True


@istest
@attr(test_type='integration', duration='slow')
class LongSpidTest3(SpidTest):
    machine = 'HWI-ST808'
    flw_dir = '201126_SN808_0155_ONEMISMACH'
    read_lengths = [101, 7, 101]
    lanes = [1]
    find_last_appearance = True
