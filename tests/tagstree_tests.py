import os
import tempfile
import unittest

# noinspection PyPackageRequirements
from nose.plugins.attrib import attr

from spid.build_data import SampleReader
from spid.tagstree import TagTreeWithWildcardsInMiddle, TagsTreeBuilder, Tag, WILDCARD, TagTreeStartWithWildcards, \
    TREE_ALGORITHM_MIDDLE_WILDCARDS, TagVariationsGenerator

from tests.test_utils import barcode_sheet_filename, TEST_TAG_TREES_DIR


class TagTreeTestMixin(unittest.TestCase):
    def assert_tree_equals(self, tag_tree, expected_output_filename):
        tree_output = str(tag_tree)
        expected_output_filename = os.path.join(TEST_TAG_TREES_DIR, expected_output_filename)
        expected_tree_output = open(expected_output_filename, 'r').read()
        if tree_output != expected_tree_output:
            _, tree_output_filename = tempfile.mkstemp(prefix='tree.', suffix='.txt')
            open(tree_output_filename, 'w').write(tree_output)
            self.fail('tree differs from expected tree. '
                      'Use "diff %s %s" for more details' % (tree_output_filename, expected_output_filename))


@attr(test_type='unit')
class TagTreeWithWildcardsInMiddleTest(TagTreeTestMixin):
    def setUp(self):
        # noinspection PyAttributeOutsideInit
        self.tree = TagTreeWithWildcardsInMiddle()

    def test_add_empty_sequence(self):
        self.assertRaises(AssertionError, self.tree.add, '', None)

    def test_add_one_base_sequence(self):
        td = Tag('one_base', 0, 1, 0, 0, '', 'A')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_one_base_sequence_tree.txt')

    def test_add_one_wildcard_sequence(self):
        td = Tag('one_wildcard', 0, 1, 0, 0, '', WILDCARD)
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_one_wildcard_tree.txt')

    def test_add_two_bases_sequence(self):
        td = Tag('two_bases', 0, 2, 0, 0, '', 'AG')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_two_bases_sequence_tree.txt')

    def test_add_two_bases_sequence_with_wildcard(self):
        td = Tag('two_bases_with_wildcard', 0, 2, 0, 0, '', 'T.')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_two_bases_sequence_with_wildcard_tree.txt')

    def test_zip_tree1(self):
        td1 = Tag('td1', 0, 3, 0, 0, '', 'T.C')
        self.tree.add(td1.tag, td1)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_zip_tree1.txt')
        self.tree.zip_tree()
        # nothing to zip here
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_zip_tree1.txt')

    def test_zip_tree2(self):
        td1 = Tag('td1', 0, 4, 0, 0, '', 'G..G')
        self.tree.add(td1.tag, td1)
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_zip_tree2_before_zip.txt')
        self.tree.zip_tree()
        self.assert_tree_equals(self.tree, 'middle-wildcards/expected_zip_tree2_after_zip.txt')


@attr(test_type='unit')
class TagTreeStartWithWildcardsTest(TagTreeTestMixin):
    def setUp(self):
        # noinspection PyAttributeOutsideInit
        self.tree = TagTreeStartWithWildcards()

    def test_add_empty_sequence(self):
        self.assertRaises(AssertionError, self.tree.add, '', None)

    def test_add_one_base_sequence(self):
        td = Tag('one_base', 0, 1, 0, 0, '', 'A')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_one_base_sequence_tree.txt')

    def test_add_one_wildcard_sequence(self):
        td = Tag('wildcard', 0, 1, 0, 0, '', WILDCARD)
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_one_wildcard_tree.txt')

    def test_add_two_bases_sequence(self):
        td = Tag('two_bases', 0, 2, 0, 0, '', 'AG')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_two_bases_sequence_tree.txt')

    def test_add_two_bases_sequence_with_wildcard(self):
        td = Tag('td', 0, 2, 0, 0, '', '.T')
        self.tree.add(td.tag, td)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_two_bases_sequence_with_wildcard_tree.txt')

    def test_add_several_tags_with_wildcards_without_merge(self):
        tags = [Tag('tag1', 0, 4, 0, 0, '', '.AGT'),
                Tag('tag2', 0, 3, 0, 0, '', '..G')]
        for tag in tags:
            self.tree.add(tag.tag, tag)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_several_tags_with_wildcards_tree_before_merge.txt')

    def test_add_several_tags_with_wildcards_and_merge(self):
        tags = [Tag('tag1', 0, 4, 0, 0, '', '.AGT'),
                Tag('tag2', 0, 3, 0, 0, '', '..G')]
        self.tree.build(tags)
        self.assert_tree_equals(self.tree, 'start-wildcards/expected_several_tags_with_wildcards_tree_after_merge.txt')


@attr(test_type='unit')
class TagsTreeBuilderTest(TagTreeTestMixin):
    maxDiff = None

    def test_build_with_empty_sample_details(self):
        tag_trees = TagsTreeBuilder(samples_details={}, reads_num=2, read_lengths=(51, 7)).build()
        self.assertEquals(len(tag_trees), 2)
        self.assertEquals(tag_trees[0], [])
        self.assertEquals(tag_trees[1], [])

    def test_build_single_read(self):
        barcode_sheet = barcode_sheet_filename('BarcodeSheet_singleRead_51_7.txt')
        lanes = [1, 3]
        self.reads_num = 2
        self.read_lengths = (51, 7)
        samples_reader = SampleReader(barcode_sheet=barcode_sheet, lanes=lanes, reads_num=self.reads_num)
        samples_details = samples_reader.samples_details[lanes[1]]
        self.assertEquals(set(samples_details.keys()), set(['Undetermined', 'no-index']))
        samples_details = samples_reader.samples_details[lanes[0]]
        self.assertEquals(set(samples_details.keys()),
                          set(['Undetermined', 'sample10', 'spike_control', 'sample5', 'sample4', 'sample7', 'sample6',
                               'sample1', 'sample3', 'sample2', 'sample9', 'sample8']))
        self.assertEquals(samples_details['sample10'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['CCGTCC', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['CCGTCC', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample10', 'init_tag_indices': (0, None, None),
                           'reads_num': 2, 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'sample_type': 'tagged'})
        self.assertEquals(samples_details['spike_control'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['GTGAAA', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['GTGAAA', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'spike_control', 'init_tag_indices': (0, None, None),
                           'reads_num': 2, 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample5'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['CAGATC', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['CAGATC', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample5', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample4'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['GCCAAT', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['GCCAAT', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample4', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample7'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['AGTCAA', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['AGTCAA', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample7', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample6'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['CTTGTA', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['CTTGTA', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample6', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample1'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['CGATGT', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['CGATGT', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample1', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample3'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['ACAGTG', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['ACAGTG', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample3', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample2'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['TGACCA', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['TGACCA', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample2', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample9'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['ATGTCA', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['ATGTCA', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample9', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample8'].__dict__,
                          {'cut_tags': [True, False, False],
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'lane': 1, 'project_name': 'tanya', 'tagged_reads': (1, None, None),
                           'reversed_tagged_reads': [None, None], 'tags': ['AGTTCC', None, None],
                           'master_sample_name': None, 'max_offset': ['0l0r', None, None], 'not_empty_reads_num': [0],
                           'tags_names': ['AGTTCC', None, None], 'max_mismatch': [0, None, None], 'master_tag': None,
                           'tags_num': 1, 'sample_name': 'sample8', 'init_tag_indices': (0, None, None), 'reads_num': 2,
                           'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]], 'sample_type': 'tagged'})
        tag_trees = TagsTreeBuilder(samples_details=samples_details, reads_num=self.reads_num,
                                    read_lengths=self.read_lengths).build(TREE_ALGORITHM_MIDDLE_WILDCARDS)
        self.assertEquals(len(tag_trees), 2)
        self.assertEquals(tag_trees[0], [])
        tags_tree = tag_trees[1]
        self.assert_tree_equals(tags_tree, 'middle-wildcards/expected_single_read_tree1.txt')
        tag_details = tags_tree.lookup(sequence='DUMMY', last_appear=True)
        self.assertEquals(tag_details, {})
        tag_details = tags_tree.lookup(sequence='ATGTCA', last_appear=True)
        self.assertEquals(tag_details, {'sample9': Tag('sample9', 0, 6, 0, 1, None, 'ATGTCA')})
        tag_details = tags_tree.lookup(sequence='CCGTCC', last_appear=True)
        self.assertEquals(tag_details, {'sample10': Tag('sample10', 0, 6, 0, 1, None, 'CCGTCC')})

    def test_create_tag_offsets_and_inits(self):
        tgTrBuilder = TagsTreeBuilder({}, 1, None)
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 0, '0l0r', 10), [('AAAAAA', 0)])
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 3, '0l0r', 10), [('...AAAAAA', 3)])
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 3, '1l1r', 10),
                          [('...AAAAAA', 3), ('..AAAAAA', 2), ('....AAAAAA', 4)])  # offset
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAAA', 3, '1l1r', 10),
                          [('...AAAAAAA', 3), ('..AAAAAAA', 2), ('....AAAAAA', 4)])  # offset out from right border
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 0, '1l1r', 10),
                          [('AAAAAA', 0), ('AAAAA', -1), ('.AAAAAA', 1)])  # offset out from left border
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 3, '3l3r', 10),
                          [('...AAAAAA', 3), ('..AAAAAA', 2), ('.AAAAAA', 1), ('AAAAAA', 0), ('....AAAAAA', 4),
                           ('.....AAAAA', 5), ('......AAAA', 6)])  # offset
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 0, '1l0r', 10),
                          [('AAAAAA', 0), ('AAAAA', -1)])
        self.assertEquals(tgTrBuilder._create_tag_offsets_and_inits('AAAAAA', 0, '0l1r', 10),
                          [('AAAAAA', 0), ('.AAAAAA', 1)])

    def test_build_small_rna(self):
        barcode_sheet = barcode_sheet_filename('BarcodeSheet_small_RNA.txt')
        lanes = [2]
        self.reads_num = 2
        self.read_lengths = (101, 7)
        samples_reader = SampleReader(barcode_sheet=barcode_sheet, lanes=lanes, reads_num=self.reads_num)
        samples_details = samples_reader.samples_details[lanes[0]]
        self.assertEquals(sorted(samples_details.keys()),
                          ['BC1', 'BC10', 'BC2', 'BC3', 'BC4', 'BC5', 'BC6', 'BC7', 'BC8', 'BC9', 'Undetermined'])
        self.assertEquals(samples_details['BC1'].__dict__, {
            'cut_tags': [True, False, False],
            'reads_description': [
                'nyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiinnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',
                'nnnnnnn'
            ], 'lane': 2, 'project_name': 'frog', 'tagged_reads': (0, None, None),
            'reversed_tagged_reads': [None, None], 'tags': ['ATCACG', None, None], 'master_sample_name': None,
            'max_offset': ['0l1r', None, None], 'not_empty_reads_num': [0],
            'tags_names': ['ATCACG', None, None], 'max_mismatch': [1, None, None], 'master_tag': None,
            'tags_num': 1, 'sample_name': 'BC1', 'init_tag_indices': (34, None, None), 'reads_num': 2,
            'sample_type': 'tagged', 'n_and_tag_indexes': [[[0, 0, 'n'], [34, 39, 'i'], [40, 100, 'n']], [[0, 6, 'n']]]
        })
        tag_trees = TagsTreeBuilder(samples_details=samples_details, reads_num=self.reads_num,
                                    read_lengths=self.read_lengths).build()
        self.assertEquals(len(tag_trees), 2)
        self.assertEquals(type(tag_trees[0]), TagTreeStartWithWildcards)
        self.assertEquals(tag_trees[1], [])
        tags_tree = tag_trees[0]
        self.assert_tree_equals(tags_tree, 'start-wildcards/expected_small_RNA_tree.txt')


@attr(test_type='unit')
class TagVariationsGeneratorTest(unittest.TestCase):
    maxDiff = None

    def setUp(self):
        self.generator = TagVariationsGenerator()

    def test_create_tag_variations_no_mismatch(self):
        self.assertEquals(self.generator.create_tag_variations('ACGT', 0), ['ACGT'])

    def test_create_tag_variations_one_mismatch(self):
        self.assertEquals(sorted(self.generator.create_tag_variations('ACGT', 1)),
                          ['AAGT', 'ACAT', 'ACCT', 'ACGA', 'ACGC', 'ACGG', 'ACGN', 'ACGT', 'ACNT',
                           'ACTT', 'AGGT', 'ANGT', 'ATGT', 'CCGT', 'GCGT', 'NCGT', 'TCGT'])

    def test_create_tag_variations_two_mismatches(self):
        self.assertEquals(sorted(self.generator.create_tag_variations('AAA', 2)),
                          ['AAA', 'AAC', 'AAG', 'AAN', 'AAT', 'ACA', 'ACC', 'ACG', 'ACN', 'ACT',
                           'AGA', 'AGC', 'AGG', 'AGN', 'AGT', 'ANA', 'ANC', 'ANG', 'ANN', 'ANT',
                           'ATA', 'ATC', 'ATG', 'ATN', 'ATT', 'CAA', 'CAC', 'CAG', 'CAN', 'CAT',
                           'CCA', 'CGA', 'CNA', 'CTA', 'GAA', 'GAC', 'GAG', 'GAN', 'GAT', 'GCA',
                           'GGA', 'GNA', 'GTA', 'NAA', 'NAC', 'NAG', 'NAN', 'NAT', 'NCA', 'NGA',
                           'NNA', 'NTA', 'TAA', 'TAC', 'TAG', 'TAN', 'TAT', 'TCA', 'TGA', 'TNA',
                           'TTA'])
