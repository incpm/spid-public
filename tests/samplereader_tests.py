from nose.plugins.attrib import attr

from spid.build_data import SampleReader
from spid.sample_info import BarcodesDescription
from spid.tagstree import TagsTreeBuilder, Tag

from tests.tagstree_tests import TagTreeTestMixin
from tests.test_utils import barcode_sheet_filename


@attr(test_type='unit')
class SampleReaderTest(TagTreeTestMixin):
    maxDiff = None

    def setUp(self):
        self.barcode_sheet = barcode_sheet_filename('BarcodeSheet_singleRead_51_7.txt')
        self.lanes = [1, 3]
        self.reads_num = 2
        self.read_lengths = (51, 7)
        self.samples_reader = SampleReader(barcode_sheet=self.barcode_sheet, lanes=self.lanes, reads_num=3)

    def test_read_single_read_barcode_sheet(self):
        samples_details = self.samples_reader.samples_details[3]
        self.assertEquals(set(samples_details.keys()), set(['Undetermined', 'no-index']),
                          msg='samples_details: %s' % self.samples_reader.samples_details)
        samples_details = self.samples_reader.samples_details[1]
        self.assertEquals(set(samples_details.keys()), set(['Undetermined', 'sample10', 'spike_control', 'sample5',
                                                            'sample4', 'sample7', 'sample6', 'sample1', 'sample3',
                                                            'sample2', 'sample9', 'sample8']))
        self.assertEquals(samples_details['sample10'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample10',
                           'tagged_reads': (1, None, None), 'tags': ['CCGTCC', None, None],
                           'tags_names': ['CCGTCC', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['spike_control'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'spike_control',
                           'tagged_reads': (1, None, None), 'tags': ['GTGAAA', None, None],
                           'tags_names': ['GTGAAA', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample5'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample5',
                           'tagged_reads': (1, None, None), 'tags': ['CAGATC', None, None],
                           'tags_names': ['CAGATC', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample4'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample4',
                           'tagged_reads': (1, None, None), 'tags': ['GCCAAT', None, None],
                           'tags_names': ['GCCAAT', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample7'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample7',
                           'tagged_reads': (1, None, None), 'tags': ['AGTCAA', None, None],
                           'tags_names': ['AGTCAA', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample6'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample6',
                           'tagged_reads': (1, None, None), 'tags': ['CTTGTA', None, None],
                           'tags_names': ['CTTGTA', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample1'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample1',
                           'tagged_reads': (1, None, None), 'tags': ['CGATGT', None, None],
                           'tags_names': ['CGATGT', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample3'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample3',
                           'tagged_reads': (1, None, None), 'tags': ['ACAGTG', None, None],
                           'tags_names': ['ACAGTG', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample2'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample2',
                           'tagged_reads': (1, None, None), 'tags': ['TGACCA', None, None],
                           'tags_names': ['TGACCA', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample9'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample9',
                           'tagged_reads': (1, None, None), 'tags': ['ATGTCA', None, None],
                           'tags_names': ['ATGTCA', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['sample8'].__dict__,
                          {'cut_tags': [True, False, False], 'init_tag_indices': (0, None, None), 'lane': 1,
                           'master_sample_name': None, 'master_tag': None, 'max_mismatch': [0, None, None],
                           'max_offset': ['0l0r', None, None], 'n_and_tag_indexes': [[], [[0, 5, 'i'], [6, 6, 'n']]],
                           'not_empty_reads_num': [0], 'project_name': 'tanya',
                           'reads_description': ['yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'iiiiiin'],
                           'reads_num': 3, 'reversed_tagged_reads': [None, None], 'sample_name': 'sample8',
                           'tagged_reads': (1, None, None), 'tags': ['AGTTCC', None, None],
                           'tags_names': ['AGTTCC', None, None], 'tags_num': 1, 'sample_type': 'tagged'})
        self.assertEquals(samples_details['Undetermined'].__dict__,
                          {'cut_tags': [False, False, False], 'reads_description': [None, None, None], 'lane': 1,
                           'project_name': None,
                           'tagged_reads': (None, None, None), 'reversed_tagged_reads': [None, None],
                           'tags': [None, None, None], 'master_sample_name': 'Undetermined',
                           'max_offset': [None, None, None], 'not_empty_reads_num': [], 'tags_names': [None, None, None],
                           'max_mismatch': [None, None, None], 'sample_type': 'undetermined-sample',
                           'master_tag': None, 'tags_num': 0, 'sample_name': 'Undetermined',
                           'init_tag_indices': (None, None, None), 'reads_num': 3, 'n_and_tag_indexes': [[], [], []]})
        tags_trees = TagsTreeBuilder(samples_details=samples_details, reads_num=self.reads_num,
                                     read_lengths=self.read_lengths).build()
        self.assertEquals(len(tags_trees), 2)
        self.assertEquals(tags_trees[0], [])
        tags_tree = tags_trees[1]
        self.assert_tree_equals(tags_tree, 'start-wildcards/expected_single_read_tree1.txt')
        tag_details = tags_tree.lookup(sequence='DUMMY', last_appear=True)
        self.assertEquals(tag_details, {})
        tag_details = tags_tree.lookup(sequence='ATGTCA', last_appear=True)
        self.assertEquals(tag_details, {'sample9': Tag('sample9', 0, 6, 0, 1, None, 'ATGTCA')})
        tag_details = tags_tree.lookup(sequence='CCGTCC', last_appear=True)
        self.assertEquals(tag_details, {'sample10': Tag('sample10', 0, 6, 0, 1, None, 'CCGTCC')})

    def test_find_n_and_tag_indexes(self):
        self.assertEquals(BarcodesDescription.find_n_and_tag_indexes(['iiiiyyyyyynyyynn', 'yy', 'yy']),
                          [[[0, 3, 'i'], [10, 10, 'n'], [14, 15, 'n']], [], []])
        self.assertEquals(BarcodesDescription.find_n_and_tag_indexes(['iiiiyyyyyynyyynny', 'yy', 'yy']),
                          [[[0, 3, 'i'], [10, 10, 'n'], [14, 15, 'n']], [], []])
        self.assertEquals(BarcodesDescription.find_n_and_tag_indexes(['yiiiiyyyyyynyyynny', 'yy', 'yy']),
                          [[[1, 4, 'i'], [11, 11, 'n'], [15, 16, 'n']], [], []])
        self.assertEquals(BarcodesDescription.find_n_and_tag_indexes(['nniiiinyynn', 'yy', 'yy']),
                          [[[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n']], [], []])
        self.assertEquals(BarcodesDescription.find_n_and_tag_indexes(['nniiiinyynnii', 'yy', 'yy']),
                          [[[0, 1, 'n'], [2, 5, 'i'], [6, 6, 'n'], [9, 10, 'n'], [11, 12, 'i']], [], []])

    def test_validate_sample_name(self):
        SampleReader._validate_sample_name('ABCDEFG')
        self.assertRaises(ValueError, SampleReader._validate_sample_name, 'ABC DEF')
