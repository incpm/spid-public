import os

from spid.settings import PROJECT_ROOT

TEST_DATA_DIR = os.path.join(PROJECT_ROOT, 'test-data')
TEST_BARCODE_SHEETS_DIR = os.path.join(TEST_DATA_DIR, 'barcode-sheets')
TEST_TAG_TREES_DIR = os.path.join(TEST_DATA_DIR, 'tag-trees')


def barcode_sheet_filename(filename):
    return os.path.join(TEST_BARCODE_SHEETS_DIR, filename)
