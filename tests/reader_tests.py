from nose.plugins.attrib import attr
import os
import tempfile
import unittest

from spid.reader import SingleFastqFileSetReader, InputFilesReader, FilesReadingRanges, FileReadingRanges
from spid.settings import PROJECT_ROOT
READER_CHUNCK_SIZE=1024
READER_LINES_BUFFER=9000

@attr(test_type='unit')
class InputFilesReaderTest(unittest.TestCase):
    def setUp(self):
        self.fastq_dir1 = os.path.join(PROJECT_ROOT, 'test-data/input-files/Unaligned_casava1_v1')
        self.reader_v1 = InputFilesReader(lane=2, casava_output_dir=self.fastq_dir1, casava_folders_structure=True)
        self.fastq_dir2 = os.path.join(PROJECT_ROOT, 'test-data/input-files/Unaligned_casava3_v2')
        self.reader_v2 = InputFilesReader(lane=2, casava_output_dir=self.fastq_dir2, casava_folders_structure=False)

    def test_read_casava_v1(self):
        self.assertEqual(self.reader_v1.read_lengths, [102, 8, 102])
        filenames = self.reader_v1.get_fastq_filenames()
        self.assertEquals(len(filenames), 48)
        self.assertEquals([os.path.basename(f) for f in filenames[0]],
                          ['lane2_NoIndex_L002_R1_001.fastq', 'lane2_NoIndex_L002_R2_001.fastq',
                           'lane2_NoIndex_L002_R3_001.fastq'])
        self.assertEquals([os.path.basename(f) for f in filenames[-1]],
                          ['lane2_NoIndex_L002_R1_048.fastq', 'lane2_NoIndex_L002_R2_048.fastq',
                           'lane2_NoIndex_L002_R3_048.fastq'])

    def test_read_casava_v2(self):
        filenames = self.reader_v2.get_fastq_filenames()
        self.assertEquals(len(filenames), 1)
        self.assertEquals([os.path.basename(f) for f in filenames[0]],
                          ['Undetermined_S0_L002_R1_001.fastq', 'Undetermined_S0_L002_I1_001.fastq',
                           'Undetermined_S0_L002_R2_001.fastq'])

    def get_reads_sample_test(self):
        fragments = [['NTATAAAGCGAGAAAGACACGGGCTNATATTTATCGGTGGAGCACAGTGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAAAAAACAAAAAAAAA',
                     'CACCCCC',
                      'NTATAAAGCGAGAAAGACACGGGCTNATATTTATCGGTGGAGCACAGTGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAAAAAACAAAAAAAAA']]

        self.assertEqual(self.reader_v1._get_reads_sample(1), fragments)


@attr(test_type='unit')
class SingleInputFileReaderTest(unittest.TestCase):
    def setUp(self):
        self.fastq_dir = os.path.join(PROJECT_ROOT, 'test-data/input-files/Unaligned_casava2_v1/Project_ABCDEFGXX/Sample_lane1')
        self.fastq_file_basenames = ['lane1_NoIndex_L001_R1_001.fastq', 'lane1_NoIndex_L001_R2_001.fastq']
        self.fastq_filenames = [os.path.join(self.fastq_dir, file_basename)
                                for file_basename in self.fastq_file_basenames]


    def test_read_v1(self):
        fastq_reading_ranges = [FileReadingRanges(file_name, 0, os.path.getsize(file_name)-1) for file_name in self.fastq_filenames]
        reader_v1 = SingleFastqFileSetReader(fastq_reading_ranges, READER_CHUNCK_SIZE, READER_LINES_BUFFER)
        self.assertEquals(len(reader_v1.fastq_lines[0]), 0)
        self.assertEquals(reader_v1.sequence_index, -1)
        self.assertEquals(reader_v1.num_lines, None)
        first_sequence = reader_v1.next()
        self.assertEquals(len(reader_v1.fastq_lines), 2)#Two reads
        self.assertEquals(len(reader_v1.fastq_lines[0]), READER_LINES_BUFFER)
        self.assertEquals(len(reader_v1.fastq_lines[1]), READER_LINES_BUFFER)
        self.assertEquals(reader_v1.sequence_index, 0)
        self.assertEquals(reader_v1.num_lines, READER_LINES_BUFFER)
        self.assertEquals(first_sequence,
                          [['@HISEQ:111:H0723ADXX:2:1101:1143:1977 1:N:0:',
                            'NTATAAAGCGAGAAAGACACGGGCTNATATTTATCGGTGGAGCACAGTGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAAAAAACAAAAAAAAA',
                            '+',
                            '#4=DFFFFHHHHHJJJJJJJJJJJJ#2?FHJJJJJJJBGIIIGCFHEEHIJJHHHHFFFFFDDDDDDDCDDDDDDDDDDDDDD##################'],
                           ['@HISEQ:111:H0723ADXX:2:1101:1143:1977 2:N:0:', 'CACCCCC', '+', '#######']])
        for i in range(1, 50):  #Track after sequence_index value
            reader_v1.next()
            self.assertEquals(reader_v1.sequence_index, i)

        def infinite_next():
            while True:
                reader_v1.next()

        # read the rest of the sequences, and get StopIteration in the end
        self.assertRaises(StopIteration, eval("infinite_next"))

    def test_read_v2(self):
        fastq_reading_ranges  = FilesReadingRanges([self.fastq_filenames], False).get_reading_range_within_file(3)
        reads_count = 0
        headers ={}
        for fastq_reading_range in fastq_reading_ranges:
            reader_v2 = SingleFastqFileSetReader(fastq_reading_range, READER_CHUNCK_SIZE, READER_LINES_BUFFER)
            while True:
                try:
                    fragment = reader_v2.next()
                    reads_count += 1
                    header = fragment[0][0].split(' ')[0]
                    self.assertRaises(KeyError, lambda: headers[header])#Unique header line
                    headers[header] = True
                    self.assertEqual(len(fragment), 2)
                    self.assertEqual(fragment[0][0].split(' ')[0], fragment[1][0].split(' ')[0])#header lines
                    self.assertEqual(fragment[0][2], fragment[1][2])# "+" line
                    self.assertEqual(len(fragment[0][1]),101)
                    self.assertEqual(len(fragment[1][1]),7)
                    self.assertEqual(len(fragment[0][3]),101)
                    self.assertEqual(len(fragment[1][3]),7)
                except StopIteration:
                    break
        self.assertEqual(reads_count, 12500)



@attr(test_type='unit')
class FilesReadingRangesTest(unittest.TestCase):
    def setUp(self):
        self.fastq_dir = os.path.join(PROJECT_ROOT, 'test-data/input-files/Unaligned_casava2_v1/Project_ABCDEFGXX/Sample_lane1')
        self.fastq_file_basenames_v1 = [['lane1_NoIndex_L001_R1_001.fastq', 'lane1_NoIndex_L001_R2_001.fastq'],
                                        ['lane1_NoIndex_L001_R1_002.fastq', 'lane1_NoIndex_L001_R2_002.fastq']]
        self.fastq_file_basenames_v2 = [['lane1_NoIndex_L001_R1_001.fastq', 'lane1_NoIndex_L001_R2_001.fastq']]
        self.fastq_filenames_v1 = [[os.path.join(self.fastq_dir, file_basename)
                                   for file_basename in fastq_file_basenames_set]
                                   for fastq_file_basenames_set in self.fastq_file_basenames_v1]
        self.fastq_filenames_v2 = [[os.path.join(self.fastq_dir, file_basename)
                                   for file_basename in fastq_file_basenames_set]
                                   for fastq_file_basenames_set in self.fastq_file_basenames_v2]
        self.files_ranges_v1 = FilesReadingRanges(self.fastq_filenames_v1, False)
        self.files_ranges_v2 = FilesReadingRanges(self.fastq_filenames_v2, False)

    def _next_line(self, file, start_read):
        fh = open(file, 'r')
        fh.seek(start_read)
        return fh.readline().split(' ')[0]

    def test_correct_first_file_ranges(self):
        first_file = self.fastq_filenames_v2[0][0]
        first_range = FileReadingRanges(first_file, 0, 999)
        self.assertEqual(self.files_ranges_v2._correct_start_first_file_and_header_line(first_range), '@HISEQ:111:H0723ADXX:2:1101:1143:1977')
        self.assertEqual(first_range.start, 0)
        self.files_ranges_v2._correct_end_first_file(first_range)
        self.assertEqual(first_range.end, 1254)
        second_range = FileReadingRanges(first_file, 1000, 2000)
        self.assertEqual(self.files_ranges_v2._correct_start_first_file_and_header_line(second_range), '@HISEQ:111:H0723ADXX:2:1101:1233:1993')
        self.assertEqual(second_range.start, 1255)
        self.assertEqual(self._next_line(first_file, 1255), '@HISEQ:111:H0723ADXX:2:1101:1233:1993')

    def test_correct_second_file_ranges(self):
        second_file = self.fastq_filenames_v2[0][1]
        first_range = FileReadingRanges(second_file, 0, 299)
        self.files_ranges_v2._correcte_start_other_files([first_range], '@HISEQ:111:H0723ADXX:2:1101:1143:1977')
        self.assertEqual(first_range.start, 0)
        second_range = FileReadingRanges(second_file, 300, 599)
        self.files_ranges_v2._correcte_start_other_files([second_range], '@HISEQ:111:H0723ADXX:2:1101:1233:1993')
        self.assertEqual(second_range.start, 315)
        self.assertEqual(self._next_line(second_file, 315), '@HISEQ:111:H0723ADXX:2:1101:1233:1993')


    def test_all_file_ranges(self):
        self.files_ranges_v2._set_all_file_ranges()
        self.assertEqual(len(self.files_ranges_v2.files_and_ranges), 1)
        first_file = self.files_ranges_v2.files_and_ranges[0][0]
        second_file = self.files_ranges_v2.files_and_ranges[0][1]
        self.assertEqual(first_file.file_name, self.fastq_filenames_v2[0][0])
        self.assertEqual(first_file.start, 0)
        self.assertEqual(first_file.end, os.path.getsize(self.fastq_filenames_v2[0][0])-1)
        self.assertEqual(second_file.file_name, self.fastq_filenames_v2[0][1])
        self.assertEqual(second_file.start, 0)
        self.assertEqual(second_file.end, os.path.getsize(self.fastq_filenames_v2[0][1])-1)

    def test_raw_ranges(self):
        self.files_ranges_v2._set_raw_ranges_within_files_set(3)
        self.assertEqual(len(self.files_ranges_v2.files_and_ranges), 3)
        self.files_ranges_v2.files_and_ranges = []
        self.files_ranges_v2._set_raw_ranges_within_files_set(2)
        self.assertEqual(len(self.files_ranges_v2.files_and_ranges), 2)

    def test_get_reading_range_within_file(self):
        self.files_ranges_v2.get_reading_range_within_file(3)
        self.assertEqual(len(self.files_ranges_v2.files_and_ranges), 3)
        self.files_ranges_v1.get_reading_range_within_file(3)
        self.assertEqual(len(self.files_ranges_v1.files_and_ranges), 2)

