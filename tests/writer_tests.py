from nose.plugins.attrib import attr
import os
import tempfile
import unittest

from spid.settings import OUTPUT_FILE_FORMAT_UNCOMPRESSED
from spid.writer import BulkWriter


@attr(test_type='unit')
class BulkWriterTest(unittest.TestCase):
    def setUp(self):
        self.fastq_output_dir = tempfile.mkdtemp(prefix='tests.', suffix='.writer')
        self.output_file_basenames = ['lane1_dummy_L001_R1_001.fastq', 'lane1_dummy_L001_R2_001.fastq']
        self.output_filenames = [os.path.join(self.fastq_output_dir, file_basename)
                                 for file_basename in self.output_file_basenames]
        self.sequences = [
            [['@HISEQ:111:H0723ADXX:2:1101:1143:1977 1:N:0:',
              'NTATAAAGCGAGAAAGACACGGGCTNATATTTATCGGTGGAGCACAGTGATCTCGTATGCCGTCTTCTGCTTGAAAAAAAAAAAAAAAAAACAAAAAAAAA',
              '+',
              '#4=DFFFFHHHHHJJJJJJJJJJJJ#2?FHJJJJJJJBGIIIGCFHEEHIJJHHHHFFFFFDDDDDDDCDDDDDDDDDDDDDD##################'],
             ['@HISEQ:111:H0723ADXX:2:1101:1143:1977 2:N:0:',
              'CACCCCC',
              '+',
              '#######']]]
        self.writer = BulkWriter(self.output_filenames, sample_name='dummy',
                                 not_empty_reads_num=[0, 1], output_format=OUTPUT_FILE_FORMAT_UNCOMPRESSED)

    def test_flush(self):
        self.assertEquals(self.writer.flush(self.sequences), 1)
