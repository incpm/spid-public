import tempfile
import six
import unittest

# noinspection PyPackageRequirements
from nose.plugins.attrib import attr

from spid.barcodesheet import BarcodeLocations, EnrichedBarcodeLocation, SampleDetails, \
    Mismatches, ReadBarcodeFile, CheckInvalidShift, PotentialIntersection

MAX_P_VALUE_ENRICHMENT_TEST=0.05

class TestData(unittest.TestCase):
    def setUp(self):
        self.barLocTwo = BarcodeLocations('AGT', 0, 2, 3, 3, 3, 3)
        self.barLocTwo2 = BarcodeLocations('TAT', 1, 2, 1, 1, 1, 1)
        self.barLocShift = BarcodeLocations('AGT', 0, 2, 3, 4, 4, 8)
        self.barLocOne = BarcodeLocations('AGT', 2, None, 3, 3, float('inf'), -float('inf'))


        self.fragmets_two_loc = [['NNNAGTNN','NNNN','NNNAGTNNAGT'],
                                 ['NNNNNNNN','NNNN','NNNAGTNNNNN'],
                                 ['NNNAGTNN','NNNN','NNNNNNNNNNN'],
                                 ['NNNAGTNN','NNNN','NNNNNNNNNNN'],
                                 ['NNNAGTNN','NNNN','NNNNNNNNNNN'],
                                 ['NNNAGTNN','NNNN','NNNNNNNNNNN'],
                                 ['NNNNNNNN','NAGT','NNNAGTNNNNN']]
        self.fragmets_shift_loc = [['NNNAGTNNN','NNNN','NNNNNNNNAGT'],
                                   ['NNNNAGTNN','NNNN','NNNNAGTNNNN'],
                                   ['NNNNAGTNN','NNNN','NNNNAGTNNNN'],
                                   ['NNNAGTNNN','NNNN','NNNNNNNNAGT']]
        self.fragmets_one_loc = [['NNNNNNNN','NNNN','NNNAGTNNAGT'],
                                 ['NNNNNNNN','NNNN','NNNAGTNNNNN'],
                                 ['NNNNNNNN','NNNN','NNNAGTNNNNN'],
                                 ['NNNAGTNN','NNNN','NNNNNNNNNNN'],
                                 ['NNNNNNNN','NAGT','NNNAGTNNNNN']]
        self.fragmets_replaced_loc = [['NNAAANNNNNN','NTAT','NTATNNNAAAN'],
                                      ['NNAAANNNNNN','NNNN','NTATNNNAAAN'],
                                      ['NNNNNNAAANN','NTAT','NNNNNNNAAAN'],
                                      ['NNNNNNAAANN','NNNN','NNNNNNNAAAN'],
                                      ['NNAAANNNNNN','NNNN','NNNNGGGNNNN'],
                                      ['NNAAANNNNNN','NNNN','NNNNGGGNNNN'],
                                      ['NNNNNNAAANN','NNNN','NNNNNNNNGGG'],
                                      ['NNNNNNAAANN','NTAT','NNNNNNNNGGG'],
                                      ['NCCCNNNNNNN','NNNN','NNNNNNNNGGG'],
                                      ['NCCCNNNNNNN','NNNN','NNNNNNNNGGG'],
                                      ['CCCNNNNNNNN','NNNN','NNNNGGGNNNN'],
                                      ['CCCNNNNNNNN','NTAT','NNNNGGGNNNN'],
                                      ['NNNNGGGNNNN','NNNN','NNCCCNNNNNN'],
                                      ['NNNNGGGNNNN','NNNN','NNCCCNNNNNN'],
                                      ['NNNNNNNNGGG','NNNN','CCCNNNNNNNN'],
                                      ['NNNNNNNNGGG','NTAT','CCCNNNNNNNN']]

        self.enriched_barcode_two_locations = EnrichedBarcodeLocation(self.barLocTwo.barcode, self.fragmets_two_loc, MAX_P_VALUE_ENRICHMENT_TEST)
        self.enriched_barcode_shift_location = EnrichedBarcodeLocation(self.barLocShift.barcode, self.fragmets_shift_loc, MAX_P_VALUE_ENRICHMENT_TEST)
        self.enriched_barcode_one_location = EnrichedBarcodeLocation(self.barLocOne.barcode, self.fragmets_one_loc, MAX_P_VALUE_ENRICHMENT_TEST)

        #Examples of replaced barcoedes
        self.barLocRep1 = BarcodeLocations('TTT', 1, None, 1, 1)
        self.barLocRep2 = BarcodeLocations('AAA', 0, 2, 2, 6, 7, 8)
        self.barLocRep3 = BarcodeLocations('GGG', 0, 2, 4, 8, 7, 8)
        self.barLocRep4 = BarcodeLocations('CCC', 0, 2, 4, 8, 12, 15)
        self.barLocRep5 = BarcodeLocations('TTA', 1, 2, 4, 8, 12, 15)
        self.barLocRep6 = BarcodeLocations('TTG', 0, None, 4, 8)
        self.barLocRep7 = BarcodeLocations('CCC', 0, 2, 0, 1, 0, 2)
        self.barLocRep8 = BarcodeLocations('CCC', 0, 2, 0, 0, 0, 2)
        self.barLocRep2fixed = BarcodeLocations('AAA', 0, None, 2, 6, float('inf'), -float('inf'))
        self.barLocRep3fixed = BarcodeLocations('GGG', 0, 2, 4, 8, 4, 8)
        self.barLocRep5fixed = BarcodeLocations('TTA', 1, None, 4, 8, float('inf'), -float('inf'))
        self.barLocRep7fixed = BarcodeLocations('CCC', 0, 2, 0, 2, 0, 2)
        self.barLocRep8fixed = BarcodeLocations('CCC', 0, None, 0, 0, float('inf'), -float('inf'))

        self.sample_details1 = SampleDetails('sample1', None, '1', 'refael.kohen', 'pmrefael@weiz.com', 'pi.refael')
        self.sample_details2 = SampleDetails('sample2', None, '1', 'refael.kohen', 'pmrefael@weiz.com', 'pi.refael')
        self.sample_details3 = SampleDetails('sample3', None, '1', 'refael.kohen', 'pmrefael@weiz.com', 'pi.refael')

@attr(test_type='unit')
class BarcodeLocationTest(TestData):
    def setUp(self):
        super(BarcodeLocationTest, self).setUp()

    def find_barcode_positions_on_read_test(self):
        self.assertEqual(self.enriched_barcode_two_locations._barcode_positions_on_read('AGT', 'NNNAGTNNAGT'), [3,8])
        self.assertEqual(self.enriched_barcode_two_locations._barcode_positions_on_read('CCC', 'NNNAGTNNAGT'), [])

    def find_barcode_on_fragment_test(self):
        self.assertEqual([loc for loc in self.enriched_barcode_two_locations._barcode_positions_on_fragment('AGT', ['NNNAGTNN','NNNN','NNNAGTNNAGT'])],
                         [(0,3,'NN'),(2,3,'NNAGT'),(2,8,'')])

    def barcode_positions_on_all_fragments_test(self):
        self.enriched_barcode_two_locations._barcode_positions_on_all_fragments()
        self.assertEqual(self.enriched_barcode_two_locations.locations_histogram,
                         dict([((0,3),5),((2,3),3),((2,8),1),((1,1),1)]))

    def get_enriched_location_test(self):
        self.assertEqual(set(self.enriched_barcode_two_locations._get_enriched_locations()),set([(0,3,5),(2,3,3)]))
        self.assertEqual(set(self.enriched_barcode_shift_location._get_enriched_locations()), set([(0,3,2),(0,4,2),(2,4,2),(2,8,2)]))

    def get_locations_test(self):
        bar_locations, rc = self.enriched_barcode_two_locations.get_locations()
        self.assertEqual((bar_locations.__str__(), rc), (self.barLocTwo.__str__(), False))
        bar_locations, rc = self.enriched_barcode_shift_location.get_locations()
        self.assertEqual((bar_locations.__str__(), rc), (self.barLocShift.__str__(), False))
        bar_locations, rc = self.enriched_barcode_one_location.get_locations()
        self.assertEqual((bar_locations.__str__(),rc), (self.barLocOne.__str__(), False))



@attr(test_type='unit')
class SampleDetailsTest(TestData):
    def setUp(self):
        super(SampleDetailsTest, self).setUp()
        self.sample_details = SampleDetails('sample1', None, '1', 'refael.kohen', 'pmrefael@weiz.com', 'pi.refael')

    def fix_crashes_locations_between_barcodes_test1(self):
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep2, self.barLocRep3]
        self.sample_details.fix_crashes_locations_between_barcodes()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep1, self.barLocRep2, self.barLocRep3]))
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep2]
        self.sample_details.fix_crashes_locations_between_barcodes()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep1, self.barLocRep2]))
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep5]
        self.sample_details.fix_crashes_locations_between_barcodes()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep1, self.barLocRep5]))

    def fix_crashes_locations_between_barcodes_test2(self):
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep5]
        self.sample_details.fix_crashes_locations_between_barcodes()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep1, BarcodeLocations('TTA', 2, None, 12, 15, float('inf'), -float('inf'))]))

    def fix_crashes_locations_between_barcodes_test3(self):
        self.sample_details.barcodes_locations = [self.barLocRep4, self.barLocRep5]
        self.sample_details.fix_crashes_locations_between_barcodes()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([BarcodeLocations('CCC', 0, None, 4, 8, float('inf'), -float('inf')),self.barLocRep5]))

    def fix_invalid_location_test(self):
        self.sample_details.fix_invalid_locations(self.barLocRep2)
        self.assertEqual(self.barLocRep2.__str__(), self.barLocRep2fixed.__str__())
        self.sample_details.fix_invalid_locations(self.barLocRep3)
        self.assertEqual(self.barLocRep3.__str__(), self.barLocRep3fixed.__str__())
        self.sample_details.fix_invalid_locations(self.barLocRep5)
        self.assertEqual(self.barLocRep5.__str__(), self.barLocRep5fixed.__str__())
        self.sample_details.fix_invalid_locations(self.barLocRep7)
        self.assertEqual(self.barLocRep7.__str__(), self.barLocRep7fixed.__str__())
        self.sample_details.fix_invalid_locations(self.barLocRep8)
        self.assertEqual(self.barLocRep8.__str__(), self.barLocRep8fixed.__str__())

    def _string_value(self, list):
        str = ''
        for item in list:
            str += "_" + item.__str__()
        return str

    def sort_barcodes_by_reads_test(self):
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep2, self.barLocRep3]
        self.sample_details._sort_barcodes_by_reads()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep2, self.barLocRep1, self.barLocRep3]))
        self.sample_details.barcodes_locations = [self.barLocRep1, self.barLocRep5, self.barLocRep6]
        self.sample_details._sort_barcodes_by_reads()
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep6, self.barLocRep1, self.barLocRep5]))

    def find_barcodes_on_fragments_test1(self):
        self.sample_details.barcodes_list = ['CCC','GGG']
        self.sample_details.find_barcodes_on_fragments(self.fragmets_replaced_loc, MAX_P_VALUE_ENRICHMENT_TEST, 2)
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep7fixed, self.barLocRep3fixed]))
        self.assertEqual(self.sample_details.replaced_barcodes, [0,2])

    def find_barcodes_on_fragments_test2(self):
        self.sample_details.barcodes_list = ['GGG']
        self.sample_details.find_barcodes_on_fragments(self.fragmets_replaced_loc, MAX_P_VALUE_ENRICHMENT_TEST, 2)
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep3fixed]))
        self.assertEqual(self.sample_details.replaced_barcodes, [0,2])

    def find_barcodes_on_fragments_test3(self):
        self.sample_details.barcodes_list = ['AAA']
        self.sample_details.find_barcodes_on_fragments(self.fragmets_replaced_loc, MAX_P_VALUE_ENRICHMENT_TEST, 2)
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep2fixed]))
        self.assertEqual(self.sample_details.replaced_barcodes, ['-','-'])

    def find_barcodes_on_fragments_test4(self):
        self.sample_details.barcodes_list = ['AAA','GGG']
        self.sample_details.find_barcodes_on_fragments(self.fragmets_replaced_loc, MAX_P_VALUE_ENRICHMENT_TEST, 2)
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([self.barLocRep2fixed, BarcodeLocations('GGG', 2, None, 4, 8, float('inf'), -float('inf'))]))
        self.assertEqual(self.sample_details.replaced_barcodes, ['-','-'])

    def find_barcodes_on_fragments_test5(self):
        self.sample_details.barcodes_list = ['TAT','AAA']
        self.sample_details.find_barcodes_on_fragments(self.fragmets_replaced_loc, MAX_P_VALUE_ENRICHMENT_TEST, 2)
        self.assertEqual(self._string_value(self.sample_details.barcodes_locations), self._string_value([BarcodeLocations('AAA', 0, None, 2, 6, float('inf'), -float('inf')), self.barLocTwo2]))
        self.assertEqual(self.sample_details.replaced_barcodes, [1,2])


@attr(test_type='unit')
class MismatchesTest(TestData):
    def setUp(self):
        super(MismatchesTest, self).setUp()
        self.mismatches = Mismatches(None, 6)

    def find_maximum_overlap_test(self):
        # _find_maximum_overlap(self, barcode, start, end, o_barcode, o_start, o_end):
        self.assertEqual(self.mismatches._find_maximum_overlap('AAA', 4, 6, 'GGGGGG', 7, 12), (None,None,None,None,None,None,None,None))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 7, 12, 'AAA', 4, 6), (None,None,None,None,None,None,None,None))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 0, 5, 'AAA', 6, 8), (None,None,None,None,None,None,None,None))
        self.assertEqual(self.mismatches._find_maximum_overlap('AAA', 4, 6, 'GGGGGG', 5, 10), ('GG', 'AA', 5, 6, 5, 6, 1, False))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAA', 4, 6), ('GG', 'AA', 5, 6, 5, 6, 4, True))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAA', 10, 12), ('A', 'G', 10, 10, 10, 10, 5, False))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAA', 8, 10), ('AAA', 'GGG', 8, 10, 8, 10, 3, False))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAA', 5, 7), ('GGG', 'AAA', 5, 7, 5, 7, 3, True))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAAA', 8, 15), ('AAA', 'GGG', 8, 14, 8, 10, 3, False))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAAA', 0, 10), ('GGGGGG', 'AAAA', 5, 10, 0, 10, 0, True))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 10, 'AAAA', 0, 20), ('GGGGGG', 'AAAA', 5, 10, 0, 20, 0, True))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 20, 'AAAA', 0, 10), ('GGGGGG', 'AAAA', 5, 20, 0, 10, 0, True))
        self.assertEqual(self.mismatches._find_maximum_overlap('GGGGGG', 5, 20, 'AAAA', 0, 6), ('GG', 'AA', 5, 16, 2, 6, 4, True))

    def enlarge_long_barcode_test(self):
        #_enlarge_long_barcode(self, trimmed_r_barcode, trimmed_l_barcode, r_start, r_end, l_start, l_end):

        #Example of length of one
        self.assertEqual(self.mismatches._enlarge_long_barcode('A', 'G', 10, 12, 5, 10, True), ('G', 'A', False))

        #Example of longer barcode starts in righter than the shorter barcode (or start in the same location like the shorter barcode)

        #Left side of longer barcode is enlarged in maximum length of shorter barcode, right side is enlarged according the right limit of shorter barcode.
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 30, 0, 13, True), ('XXAAAA', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 30, 0, 14, True), ('XXAAAAX', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 30, 0, 15, True), ('XXAAAAXX', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 30, 0, 20, True), ('XXAAAAXX', 'GGG', True))
        #Right side of longer barcode is enlarged in maximum length of shorter barcode, left side is enlarged according the right limit of shorter barcode.
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 12, 15, 10, 20, True), ('XXAAAAXX', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 12, 15, 11, 20, True), ('XAAAAXX', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 12, 15, 12, 20, True), ('AAAAXX', 'GGG', True))
        #Right and Left sides of longer barcode is enlarged according the limits of shorter barcode in two sides.
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 13, 10, 12, True), ('AAAA', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 13, 9, 13, True), ('XAAAA', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 13, 9, 12, True), ('XAAAA', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 13, 10, 14, True), ('AAAAX', 'GGG', True))
        self.assertEqual(self.mismatches._enlarge_long_barcode('AAAA', 'GGG', 10, 13, 9, 14, True), ('XAAAAX', 'GGG', True))

        #Right side of longer barcode is enlarged in maximum length of shorter barcode, left side is enlarged according the right limit of longer barcode.
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 9, 30, 0, 15, True), ('XXAAAAXX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 30, 0, 15, True), ('XXAAAAXX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 11, 30, 0, 15, True), ('XAAAAXX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 12, 30, 0, 15, True), ('AAAAXX', 'GGG', False))
        #Left side of longer barcode is enlarged in maximum length of shorter barcode, right side is enlarged according the left limit of longer barcode.
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 12, 9, 15, True), ('XXAAAA', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 13, 9, 15, True), ('XXAAAAX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 14, 9, 15, True), ('XXAAAAXX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 15, 9, 15, True), ('XXAAAAXX', 'GGG', False))
        #Right and Left sides of longer barcode is enlarged according the limits of shorter barcode in two sides.
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 11, 13, 10, 13, True), ('AAAA', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 13, 10, 13, True), ('AAAA', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 12, 10, 14, True), ('XAAAA', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 13, 10, 14, True), ('XAAAA', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 11, 14, 10, 13, True), ('AAAAX', 'GGG', False))
        self.assertEqual(self.mismatches._enlarge_long_barcode('GGG', 'AAAA', 10, 14, 10, 13, True), ('AAAAX', 'GGG', False))

    def hamming_dist_test(self):
        with self.assertRaises(IOError):
            self.mismatches._hamming_dist('AAG','AAAA')
            self.mismatches._hamming_dist('AAAG','AAA')
        self.assertEqual(self.mismatches._hamming_dist('AAA', 'AAG'), (1,0))
        self.assertEqual(self.mismatches._hamming_dist('AAAAA', 'AAGXX'), (1,2))
        self.assertEqual(self.mismatches._hamming_dist('AAAAA', 'XXGAA'), (1,2))

    def allowed_mis_number_test(self):
        #_allowed_mis_number(self, diff, remains, min_remains_per_mis=MIN_REMAINS_PER_MIS)
        self.assertEqual(self.mismatches._allowed_mis_number(3, 13), 3)
        self.assertEqual(self.mismatches._allowed_mis_number(3, 10), 2)
        self.assertEqual(self.mismatches._allowed_mis_number(2, 10), 1)
        self.assertEqual(self.mismatches._allowed_mis_number(3, 5), 1)
        self.assertEqual(self.mismatches._allowed_mis_number(2, 5), 0)

    def max_mismatches_between_pair_test(self):
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, None, 2, 6), BarcodeLocations('GGG', 0, None, 2, 6)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, None, 2, 6), BarcodeLocations('GGG', 0, 2, 4, 8, 4, 8)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, 2, 4, 8, 4, 8), BarcodeLocations('GGG', 0, 2, 4, 8, 4, 8)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, 2, 3, 3, 3, 3), BarcodeLocations('GGG', 0, 2, 3, 3, 3, 3)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAA', 0, 2, 3, 3, 3, 3), BarcodeLocations('GGG', 0, 2, 3, 3, 3, 3)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAAAAA', 0, 2, 3, 3, 3, 3), BarcodeLocations('AAA', 0, 2, 3, 3, 3, 3)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, None, 2, 2), BarcodeLocations('AAA', 0, None, 10, 10)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAA', 0, None, 2, 2), BarcodeLocations('AAA', 0, None, 10, 10)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAA', 0, None, 2, 2), BarcodeLocations('AAA', 1, None, 10, 10)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAAAAA', 0, 2, 4, 4, 4, 4), BarcodeLocations('GGG', 0, 2, 4, 4, 4, 4)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('GGG', 0, 2, 4, 4, 4, 4), BarcodeLocations('AAAAAAAAA', 0, 2, 4, 4, 4, 4)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAAAAA', 0, 2, 4, 4, 4, 4), BarcodeLocations('GGG', 0, 1, 4, 4, 4, 4)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('GGG', 0, 1, 4, 4, 4, 4), BarcodeLocations('AAAAAAAAA', 0, 2, 4, 4, 4, 4)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAAAAAAAA', 0, 2, 3, 3, 3, 3), BarcodeLocations('AAA', 0, 2, 3, 11, 3, 3)),1)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('GGG', 0, 2, 4, 4, 4, 4), BarcodeLocations('AAA', 0, 2, 4, 4, 4, 4)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('AAA', 0, None, 2, 2), BarcodeLocations('GGG', 0, 1, 2, 2, 2, 2)),0)
        self.assertEqual(self.mismatches._max_mismatches_between_pair(BarcodeLocations('GGG', 0, 1, 2, 2, 2, 2),BarcodeLocations('AAA', 0, None, 2, 2)),0)



@attr(test_type='unit')
class CheckInvalidShiftTest(TestData):
    def setUp(self):
        super(CheckInvalidShiftTest, self).setUp()
        self.check_invalid_shift = CheckInvalidShift(None)

    def invalid_shift_test(self):
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'GGG', PotentialIntersection(1, 0, 0, 0, 0)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'GGG', PotentialIntersection(1, 3, 3, 3, 3)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'GGG', PotentialIntersection(1, 3, 3, 5, 6)), [[3,5]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'GGG', PotentialIntersection(1, 3, 3, 5, 10)), [[3,5]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'GGG', PotentialIntersection(1, 3, 3, 4, 4)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AGG', PotentialIntersection(1, 3, 3, 4, 4)), [[3,4]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AG', PotentialIntersection(1, 3, 3, 4, 4)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AG', PotentialIntersection(1, 3, 5, 4, 4)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AGG', PotentialIntersection(1, 1, 5, 4, 4)), [[3,4]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAAGGG', 'AGG', PotentialIntersection(1, 1, 5, 4, 4)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGGG', 'AGG', PotentialIntersection(1, 1, 5, 4, 4)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGGG', 'AGGGGGGG', PotentialIntersection(1, 1, 5, 4, 4)), [[3,4]])

        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AAG', PotentialIntersection(1, 5, 5, 5, 5)), [[5,5]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AAAG', PotentialIntersection(1, 5, 5, 4, 4)), [[5,4]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AAAG', PotentialIntersection(1, 5, 5, 3, 4)), [[5,4]])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGG', 'AAAG', PotentialIntersection(1, 5, 5, 0, 0)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAGGGG', 'AAAG', PotentialIntersection(1, 5, 5, 3, 3)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAAGGG', 'AAG', PotentialIntersection(1, 5, 5, 3, 3)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAAGGG', 'AAG', PotentialIntersection(1, 5, 5, 6, 6)), [])
        self.assertEqual(self.check_invalid_shift._invalid_shift('AAAG', 'AAG', PotentialIntersection(1, 5, 5, 6, 6)), [[5,6]])


@attr(test_type='unit')
class ReadBarcodeFileTest(unittest.TestCase):
    def setUp(self):
        self.dirpath = tempfile.mkdtemp()
        _, self.barcodes_list_file = tempfile.mkstemp(prefix='barcodes_list_test', suffix='.csv', dir=self.dirpath)
        self.fh = open(self.barcodes_list_file, 'w')
        self.header_line ="#lane,sampleName,bar1,bar2,bar3,userName,email,piName"
        self.lines = ["1,sample1,CGATGT,-,CCCCCC,username1,us1@gmail.com,pi1",
                      "1,sample2,-,CGATGT,CCCCCC,username1,us1@gmail.com,pi1",
                      "1,sample3,CGATGT,CCCCCC,-,username1,us1@gmail.com,pi1",
                      "1,sample4,CGATGT,-,CCCCCC,username1,us1@gmail.com",
                      "1,sample5,-,-,-,username1,us1@gmail.com,pil",
                      "1,,CGATGT,CCCCCC,-,username1,us1@gmail.com,pi1",
                      "2,sample6,TGACCA,GGGGGG,-,username2,us2@gmail.com,pi2"]

    def tearDown(self):
        self.fh.close()

    def read_empty_test(self):
        #Empty file
        samples = ReadBarcodeFile(self.barcodes_list_file).get_sample_details(1)
        with self.assertRaises(Exception):
            six.next(samples)#empty file

    def read_header_line_only_test(self):
        #file with header line only
        self.fh.write(self.header_line)
        self.fh.seek(0)#must to do seek(0)
        samples = ReadBarcodeFile(self.barcodes_list_file).get_sample_details(1)
        with self.assertRaises(Exception):
            six.next(samples)#Empty file

    def read_valid_file_test(self):
        #valid file
        self.fh.write('\n'.join([self.header_line]+self.lines))
        self.fh.seek(0)#must to close or do seek(0)
        samples = ReadBarcodeFile(self.barcodes_list_file).get_sample_details(1)

        self.assertEqual(six.next(samples).__str__(), SampleDetails('sample1', ['CGATGT', 'CCCCCC'], 1, 'username1', 'us1@gmail.com', 'pi1').__str__())
        self.assertEqual(six.next(samples).__str__(), SampleDetails('sample2', ['CGATGT', 'CCCCCC'], 1, 'username1', 'us1@gmail.com', 'pi1').__str__())
        self.assertEqual(six.next(samples).__str__(), SampleDetails('sample3', ['CGATGT', 'CCCCCC'], 1, 'username1', 'us1@gmail.com', 'pi1').__str__())
        with self.assertRaises(Exception):
            six.next(samples)#misssing fields
            six.next(samples)#no index
            six.next(samples)#empty field
        with self.assertRaises(StopIteration):
            six.next(samples)#stop iteration



